﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Vibrotech;
using System.IO.Ports;
using Vibrotech_DPS2016;
namespace Vibrotech_DPS2016
{
    /// <summary>
    /// Active.xaml 的交互逻辑
    /// </summary>
    public partial class Active : Window
    {
        //private DpsDevice device;
        //private DpsDevice.DeviceInfo Information = new DpsDevice.DeviceInfo();
        public Active()
        {
            InitializeComponent();
            //device = new DpsDevice();
            //device.OnConnected += DeviceOnConnectedThread;
            //device.OnDisConnected += DeviceOnDisConnectedThread;
            //Connect_Information();
        }
        //private void Connect_Information()
        //{
        //    if (device.IsDeviceConnected)
        //    {
        //        DeviceOnConnected();
        //    }
        //    else
        //    {
        //        DeviceOnDisConnected();
        //    }
        //}
        //private void DeviceOnConnectedThread()
        //{
        //    Dispatcher.Invoke(new Action(DeviceOnConnected));
        //}
        //private void DeviceOnConnected()
        //{
        //    Activation_Information();

        //}
        //private void DeviceOnDisConnectedThread()
        //{
        //    Dispatcher.Invoke(new Action(DeviceOnDisConnected));

        //}
        //private void DeviceOnDisConnected()
        //{
        //    this.Close();
        //}

        private void Activation_Information()
        {
            //if (!serialport.IsOpen)
            //{
            //    serialport.Open();
            //}
            //Activation_ActiveCode.Text = serialport.ReadLine();

        }
        private void Activation_OK_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Close();
                //await device.WriteActivationCode(Activation_ActiveCode.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "ERROR");
            }
            //this.Close();

        }

        private void Activation_Cencel_Click(object sender, RoutedEventArgs e)
        {
            Activation_ActiveCode.Text = "";
            this.Close();
        }
        //delegate void HandleInterfaceUpdateDelagate(string text);
        //HandleInterfaceUpdateDelagate interfaceUpdateHandle;
        //private void Window_Loaded(object sender, RoutedEventArgs e)
        //{
        //    SerialPort serialport = new SerialPort();

        //serialport.PortName = "COM8";
        //    serialport.BaudRate = 115200;
        //    serialport.Parity = Parity.None;
        //    serialport.StopBits = StopBits.One;
        //        serialport.Open();
        //    if (!serialport.IsOpen)
        //    {
        //        MessageBox.Show("请输入激活码");
        //        //serialport.Open();
        //    }
        //    else
        //    {
        //        SendBytesData1(serialport);
        //        SendBytesData2(serialport);
        //        ReceiveData(serialport);
        //        //serialport.DataBits = 8;
        //        //serialport.Handshake = Handshake.None;
        //        //serialport.RtsEnable = true;
        //        //serialport.DataReceived += new SerialDataReceivedEventHandler(DataReceivedHandler);
        //        //serialport.Open();
        //        //serialport.WriteLine(Activation_HardwareSn.Text);

        //        //serialport.Close();
        //        //Activation_ActiveCode.Text = serialport.ReadLine();
        //    }
        //}
        //private void SendBytesData1(SerialPort serialport1)
        //{
        //    byte[] bytesSend1 = System.Text.Encoding.Default.GetBytes("Robin680305");
        //    serialport1.Write(bytesSend1, 0, bytesSend1.Length);
        //}

        //private void SendBytesData2(SerialPort serialport2)
        //{
        //    byte[] bytesSend2 = System.Text.Encoding.Default.GetBytes(Activation_HardwareSn.Text);
        //    serialport2.Write(bytesSend2, 0, bytesSend2.Length);
        //}
        //private void ReceiveData(SerialPort serialport1)
        //{
        //    Thread threadReceivesub = new Thread(new ParameterizedThreadStart(AsyReceiveData));
        //    threadReceivesub.Start(serialport1);

        //}
        //private void AsyReceiveData(object serialportObj)
        //{
        //    SerialPort serialPort = (SerialPort)serialportObj;
        //    System.Threading.Thread.Sleep(500);
        //    try
        //    {
        //        int n = serialPort.BytesToRead;
        //        byte[] buf = new byte[n];
        //        serialPort.Read(buf, 0, n);
        //        interfaceUpdateHandle = new HandleInterfaceUpdateDelagate(UpdateTextBox);
        //        Dispatcher.Invoke(interfaceUpdateHandle, new string[] { Encoding.ASCII.GetString(buf) });
        //    }
        //    catch(Exception e)
        //    {
        //        MessageBox.Show(e.Message);
        //    }
        //    serialPort.Close();
        //}
        //private void UpdateTextBox( string text)
        //{
        //    Activation_ActiveCode.Text = text;
        //}


    }
}
