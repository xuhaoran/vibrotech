﻿using System;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using Vibrotech;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
namespace Vibrotech_DPS2016
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {

        private DpsDevice device;
        private DpsDevice.ChannelParameters[] ChannelParamenters = new DpsDevice.ChannelParameters[2];
        private DpsDevice.DeviceInfo Information = new DpsDevice.DeviceInfo();
        private DpsDevice.RelayConfig[] RelayConfig = new DpsDevice.RelayConfig[2];
        private DpsDevice.ModbusConfig ModbusConfig = new DpsDevice.ModbusConfig();
        private int[] DacTrimArray1;
        private int[] DacTrimArray2;
        private Vibrotech_DPS2016.Active Activation = new Vibrotech_DPS2016.Active();


        public MainWindow()
        {
            InitializeComponent();
            Usb_State_Off.Visibility = Visibility.Visible;
            device = new DpsDevice();
            device.OnConnected += DeviceOnConnectedThread;
            device.OnDisConnected += DeviceOnDisConnectedThread;
            device.OnMeasurementReceived += DeviceMeasurement;
            Connect();


        }
        private void DeviceOnConnectedThread()
        {
            Dispatcher.Invoke(new Action(DeviceOnConnected));
        }
        private void Connect()
        {
            if(device.IsDeviceConnected)
            {
                //if (Information.IsActivated)
                //{
                DeviceOnConnected();
                IsActive_Information();
                //}
                //else
                //{
                //    DeviceOnDisConnected();
                //}
            }
            else
            {
                DeviceOnDisConnected();
            }
        }
        //private void Activaion_Information()
        //{
        //    if(Information.IsActivated)
        //    {
        //        DeviceOnConnected();
        //    }
        //    else
        //    {
        //        DeviceOnDisConnected();
        //    }
        //}

        private async void DeviceOnConnected()
        {
            try
            {
                Usb_State_On.Visibility = Visibility.Visible;
                Usb_State_Off.Visibility = Visibility.Hidden;
                Information = await device.GetDeviceInfo();
                ChannelParamenters[0] = await device.GetChannelParameters(0);
                ChannelParamenters[1] = await device.GetChannelParameters(1);
                RelayConfig[0] = await device.GetRelayConfig(0);
                RelayConfig[1] = await device.GetRelayConfig(1);
                ModbusConfig = await device.GetModbusConfig();
                DacTrimArray1 = await device.GetDacTrimArray(0);
                DacTrimArray2 = await device.GetDacTrimArray(1);
                await device.SetFixedOutCurrent(0, 0);
                await device.SetFixedOutCurrent(1, 0);
                Channel1.IsChecked = true;
                Channel2.IsChecked = false;
                //Product_Information();
                //Product_Choose_Information();
                Connect_Information();
                IsActive_Information();

            }
            catch (Exception)
            {
                //MessageBox.Show( e.ToString(),"ERROR");
            }

        }
        private void Connect_Information()
        {
            Product_Choose_Information();
            Product_Information();
            Channel_Paramenters.IsEnabled = true;
            Channel_CalibrationADC_DAC.IsEnabled = true;
            Channel_Relay.IsEnabled = true;
            Product_information.IsEnabled = true;
            Output_Information.IsEnabled = true;
            Paramenters.IsEnabled = true;
            Type_Information();
            //Relay_Information();
            Relay_ALL_Information();
            //Relay_1_Information();
            //Relay_2_Information();

            if (Channel1.IsChecked == true)
            {
                Interface_Channel1_Information();
            }
            else if (Channel2.IsChecked == true)
            {
                Interface_Channel2_Information();
            }
            RS485_Information();
            Channel_Calibration_Information();
            DAC_Trim_Information();

        }
        private void Product_Information()
        {
            Product_HardwareSn.Text = Information.HardwareSn;
            Product_FirmwareVersion.Text = Information.FirmwareVersion;
            Product_MfgDate.Text = Information.MfgDate.ToShortDateString();
            Product_MfgSn.Text = Information.MfgSn;

        }
        private async void IsActive_Information()
        {
            try
            {
                string a;
                if (Information.IsActivated)
                {
                    Channel_Paramenters.IsEnabled = true;
                    Restart.IsEnabled = true;
                    Channel_CalibrationADC_DAC.IsEnabled = true;
                    Channel_Relay.IsEnabled = true;
                    Product_information.IsEnabled = true;
                    Output_Information.IsEnabled = true;
                    Paramenters.IsEnabled =true;
                    //DeviceOnConnected();

                }
                else
                {
                    Channel_Paramenters.IsEnabled = false;
                    Restart.IsEnabled = true;
                    Channel_CalibrationADC_DAC.IsEnabled = false;
                    Channel_Relay.IsEnabled = false;
                    Product_information.IsEnabled = true;
                    Output_Information.IsEnabled = false;
                    Paramenters.IsEnabled = false;
                    Activation.Activation_HardwareSn.Text = Information.HardwareSn;
                    Activation.ShowDialog();
                    a = Activation.Activation_ActiveCode.Text;
                    await device.WriteActivationCode(a);
                    Connect_Information();
                }
            }
            catch (Exception)
            {
                //MessageBox.Show(e.ToString(), "ERROR");
            }
        }
            private void Type_Information()
        {
            Channel1_UintString.Content = ChannelParamenters[0].OutputUnitStr;
            Channel2_UintString.Content = ChannelParamenters[1].OutputUnitStr;

            if (ChannelParamenters[0].TConfig == DpsDevice.TConfig.Position)
            {
                Txtb_Channel1_Type.Text = "轴向位移";
            }
            else if (ChannelParamenters[0].TConfig == DpsDevice.TConfig.Vibration)
            {
                Txtb_Channel1_Type.Text = "壳体振动";
            }
            else if (ChannelParamenters[0].TConfig == DpsDevice.TConfig.Tacho)
            {
                Txtb_Channel1_Type.Text = "转速/键相位";
            }
            if (ChannelParamenters[1].TConfig == DpsDevice.TConfig.Position)
            {
                Txtb_Channel2_Type.Text = "轴向位移";
            }
            else if (ChannelParamenters[1].TConfig == DpsDevice.TConfig.Vibration)
            {
                Txtb_Channel2_Type.Text = "壳体振动";
            }
            else if (ChannelParamenters[1].TConfig == DpsDevice.TConfig.Tacho)
            {
                Txtb_Channel2_Type.Text = "转速/键相位";
            }

        }
        //private void Relay_Information()
        //{
        //    Relay1_State();
        //    Relay1_Channel_Config();
        //    Relay1_LogicOperation_Information();
        //    Relay1_SelfLatching_Information();

        //}
        private void Product_Choose2168_Information()
        {
            if (ChannelParamenters[0].SensorUnit == DpsDevice.SensorUnit.MvPerG || ChannelParamenters[0].SensorUnit == DpsDevice.SensorUnit.MvPerIps || ChannelParamenters[0].SensorUnit == DpsDevice.SensorUnit.MvPerMmps)
            {
                Channel1_SensorUnit_Information();
                Channel1_SendSensorUnitInformation();
            }
            else if (ChannelParamenters[1].SensorUnit == DpsDevice.SensorUnit.MvPerG || ChannelParamenters[1].SensorUnit == DpsDevice.SensorUnit.MvPerIps || ChannelParamenters[1].SensorUnit == DpsDevice.SensorUnit.MvPerMmps)
            {
                Channel2_SensorUnit_Information();
                Channel2_SendSensorUnitInformation();
            }
            else
            {
                ChannelParamenters[0].SensorUnit = DpsDevice.SensorUnit.MvPerG;
                ChannelParamenters[1].SensorUnit = DpsDevice.SensorUnit.MvPerG;
                Channel1_SensorUnit_Information();
                Channel1_SendSensorUnitInformation();
                Channel2_OutputUnit_Information();
                Channel2_SendOutputUnitInformation();

            }
        }
        private void Product_Choose2016_Information()
        {

            if (ChannelParamenters[0].SensorUnit == DpsDevice.SensorUnit.VoltPerMm || ChannelParamenters[0].SensorUnit == DpsDevice.SensorUnit.VoltPerInch)
            {
                Channel1_SensorUnit_Information();
                Channel1_SendSensorUnitInformation();
            }
            else
            {
                ChannelParamenters[0].SensorUnit = DpsDevice.SensorUnit.VoltPerMm;
                Channel1_OutputUnit_Information();
                Channel1_SendOutputUnitInformation();
            }

            if (ChannelParamenters[1].SensorUnit == DpsDevice.SensorUnit.VoltPerMm || ChannelParamenters[1].SensorUnit == DpsDevice.SensorUnit.VoltPerInch )
            {
                Channel2_SensorUnit_Information();
                Channel2_SendSensorUnitInformation();
            }
            else
            {
                ChannelParamenters[1].SensorUnit = DpsDevice.SensorUnit.VoltPerMm;
                Channel2_OutputUnit_Information();
                Channel2_SendOutputUnitInformation();
            }

            if (ChannelParamenters[0].OutputUnit == DpsDevice.OutputUnit.Um|| ChannelParamenters[0].OutputUnit == DpsDevice.OutputUnit.Mm|| ChannelParamenters[0].OutputUnit == DpsDevice.OutputUnit.Mil)
            {
                Channel1_OutputUnit_Information();
                Channel1_SendOutputUnitInformation();
            }
            else
            {
                ChannelParamenters[0].OutputUnit = DpsDevice.OutputUnit.Um;
                Channel1_OutputUnit_Information();
                Channel1_SendOutputUnitInformation();
            }
            if (ChannelParamenters[1].OutputUnit == DpsDevice.OutputUnit.Um || ChannelParamenters[1].OutputUnit == DpsDevice.OutputUnit.Mm || ChannelParamenters[1].OutputUnit == DpsDevice.OutputUnit.Mil)
            {
                Channel2_OutputUnit_Information();
                Channel2_SendOutputUnitInformation();
            }
            else
            {
                ChannelParamenters[1].OutputUnit = DpsDevice.OutputUnit.Um;
                Channel2_OutputUnit_Information();
                Channel2_SendOutputUnitInformation();
            }

        }
        private void Product_Choose_Information()
        {
            if (Information.DeviceModel == "VibraTrol2168")
            {

                Product_DeviceModel.SelectedItem = Product_DeviceModel_VibraTrol2168;
                Channel1_Type.SelectedItem = Channel1_Vibration_Choose;
                Channel2_Type.SelectedItem = Channel2_Vibration_Choose;
                Channel1_SendTypeInformation();
                Channel2_SendTypeInformation();
                Channel1_Type.IsEnabled = false;
                Channel2_Type.IsEnabled = false;
                Product_Choose2168_Information();
                //ChannelParamenters[0].SensorUnit = DpsDevice.SensorUnit.MvPerG;
                //ChannelParamenters[1].SensorUnit = DpsDevice.SensorUnit.MvPerG;

                if (Channel1.IsChecked == true)
                {
                    //Channel1_SensorUnit_Information();
                    //Channel1_SendSensorUnitInformation();
                    Interface_Channel1_Information();
                }
                else if (Channel1.IsChecked == false)
                {
                    //Channel2_SensorUnit_Information();
                    //Channel2_SendSensorUnitInformation();
                    Interface_Channel2_Information();
                }
                Channel1_SensorUnit.Items.Remove(Channel1_SensorUnit_VoltPerInch);
                Channel1_SensorUnit.Items.Remove(Channel1_SensorUnit_VoltPerMm);

                //Channel1_SensorUnit_MvPerG.IsEnabled = true;
                //Channel1_SensorUnit_MvPerIps.IsEnabled = true;
                //Channel1_SensorUnit_MvPerMmps.IsEnabled = true;
                //Channel1_OutputUnit_G.IsEnabled = true;
                //Channel1_OutputUnit_Ips.IsEnabled = true;
                //Channel1_OutputUnit_Mmps.IsEnabled = true;
                //Channel1_OutputUnit_Mps2.IsEnabled = true;
                Channel2_SensorUnit.Items.Remove(Channel2_SensorUnit_VoltPerInch);
                Channel2_SensorUnit.Items.Remove(Channel2_SensorUnit_VoltPerMm);

                //Channel2_SensorUnit_MvPerG.IsEnabled = true;
                //Channel2_SensorUnit_MvPerIps.IsEnabled = true;
                //Channel2_SensorUnit_MvPerMmps.IsEnabled = true;
                //Channel2_OutputUnit_G.IsEnabled = true;
                //Channel2_OutputUnit_Ips.IsEnabled = true;
                //Channel2_OutputUnit_Mmps.IsEnabled = true;
                //Channel2_OutputUnit_Mps2.IsEnabled = true;
                //Channel1_OutputUnit_G.IsEnabled = true;
                //Channel1_OutputUnit_Ips.IsEnabled = true;
                //Channel1_OutputUnit_Mmps.IsEnabled = true;
                //Channel1_OutputUnit_Mps2.IsEnabled = true;
                //Channel2_OutputUnit_G.IsEnabled = true;
                //Channel2_OutputUnit_Ips.IsEnabled = true;
                //Channel2_OutputUnit_Mmps.IsEnabled = true;
                //Channel2_OutputUnit_Mps2.IsEnabled = true;


            }
            else if (Information.DeviceModel == "")
            {
                Product_DeviceModel.SelectedItem = Product_DeviceModel_DPS2016;
                Channel1_SendTypeInformation();
                Channel2_SendTypeInformation();
                Channel1_Type.IsEnabled = true;
                Channel2_Type.IsEnabled = true;
                Product_Choose2016_Information();
                //ChannelParamenters[0].OutputUnit = DpsDevice.OutputUnit.Um;
                //ChannelParamenters[1].OutputUnit = DpsDevice.OutputUnit.Um;

                if (Channel1.IsChecked == true)
                {
                    //Channel1_SensorUnit_Information();
                    //Channel1_SendSensorUnitInformation();
                    //Channel1_OutputUnit_Information();
                    //Channel1_SendOutputUnitInformation();

                    Interface_Channel1_Information();
                }
                else if (Channel1.IsChecked == false)
                {
                    //Channel2_SensorUnit_Information();
                    //Channel2_SendSensorUnitInformation();
                    //Channel2_OutputUnit_Information();
                    //Channel2_SendOutputUnitInformation();

                    Interface_Channel2_Information();
                }
                //Channel1_SensorUnit.Items.Add(Channel1_SensorUnit_VoltPerInch);
                //Channel1_SensorUnit.Items.Add(Channel1_SensorUnit_VoltPerMm);
                Channel1_SensorUnit.Items.Remove(Channel1_SensorUnit_MvPerG);
                Channel1_SensorUnit.Items.Remove(Channel1_SensorUnit_MvPerIps);
                Channel1_SensorUnit.Items.Remove(Channel1_SensorUnit_MvPerMmps);
                Channel1_OutputUnit.Items.Remove(Channel1_OutputUnit_G);
                Channel1_OutputUnit.Items.Remove(Channel1_OutputUnit_Ips);
                Channel1_OutputUnit.Items.Remove(Channel1_OutputUnit_Mmps);
                Channel1_OutputUnit.Items.Remove(Channel1_OutputUnit_Mps2);
                //Channel2_SensorUnit.Items.Add(Channel2_SensorUnit_VoltPerInch);
                //Channel2_SensorUnit.Items.Add(Channel2_SensorUnit_VoltPerMm);
                Channel2_SensorUnit.Items.Remove(Channel2_SensorUnit_MvPerG);
                Channel2_SensorUnit.Items.Remove(Channel2_SensorUnit_MvPerIps);
                Channel2_SensorUnit.Items.Remove(Channel2_SensorUnit_MvPerMmps);
                Channel2_OutputUnit.Items.Remove(Channel2_OutputUnit_G);
                Channel2_OutputUnit.Items.Remove(Channel2_OutputUnit_Ips);
                Channel2_OutputUnit.Items.Remove(Channel2_OutputUnit_Mmps);
                Channel2_OutputUnit.Items.Remove(Channel2_OutputUnit_Mps2);

                //Channel1_SensorUnit_MvPerG.IsEnabled = false;
                //Channel1_SensorUnit_MvPerIps.IsEnabled = false;
                //Channel1_SensorUnit_MvPerMmps.IsEnabled = false;
                //Channel1_OutputUnit_G.IsEnabled = false;
                //Channel1_OutputUnit_Ips.IsEnabled = false;
                //Channel1_OutputUnit_Mmps.IsEnabled = false;
                //Channel1_OutputUnit_Mps2.IsEnabled = false;
                //Channel2_SensorUnit_VoltPerInch.IsEnabled = true;
                //Channel2_SensorUnit_VoltPerMm.IsEnabled = true;
                //Channel2_SensorUnit_MvPerG.IsEnabled = false;
                //Channel2_SensorUnit_MvPerIps.IsEnabled = false;
                //Channel2_SensorUnit_MvPerMmps.IsEnabled = false;
                //Channel2_OutputUnit_G.IsEnabled = false;
                //Channel2_OutputUnit_Ips.IsEnabled = false;
                //Channel2_OutputUnit_Mmps.IsEnabled = false;
                //Channel2_OutputUnit_Mps2.IsEnabled = false;

            }
            else if (Information.DeviceModel == "DPS2016")
            {
                Product_DeviceModel.SelectedItem = Product_DeviceModel_DPS2016;
                Channel1_SendTypeInformation();
                Channel2_SendTypeInformation();
                Channel1_Type.IsEnabled = true;
                Channel2_Type.IsEnabled = true;
                Product_Choose2016_Information();
                //ChannelParamenters[0].SensorUnit = DpsDevice.SensorUnit.VoltPerMm;
                //ChannelParamenters[1].SensorUnit = DpsDevice.SensorUnit.VoltPerMm;
                //ChannelParamenters[0].OutputUnit = DpsDevice.OutputUnit.Um;
                //ChannelParamenters[1].OutputUnit = DpsDevice.OutputUnit.Um;

                if (Channel1.IsChecked == true)
                {
                    //Channel1_SensorUnit_Information();
                    //Channel1_SendSensorUnitInformation();
                    //Channel1_OutputUnit_Information();
                    //Channel1_SendOutputUnitInformation();

                    Interface_Channel1_Information();
                }
                else if (Channel1.IsChecked == false)
                {
                    //Channel2_SensorUnit_Information();
                    //Channel2_SendSensorUnitInformation();
                    //Channel2_OutputUnit_Information();
                    //Channel2_SendOutputUnitInformation();

                    Interface_Channel2_Information();
                }
                //Channel1_SensorUnit.Items.Add(Channel1_SensorUnit_VoltPerInch);
                //Channel1_SensorUnit.Items.Add(Channel1_SensorUnit_VoltPerMm);
                Channel1_SensorUnit.Items.Remove(Channel1_SensorUnit_MvPerG);
                Channel1_SensorUnit.Items.Remove(Channel1_SensorUnit_MvPerIps);
                Channel1_SensorUnit.Items.Remove(Channel1_SensorUnit_MvPerMmps);
                Channel1_OutputUnit.Items.Remove(Channel1_OutputUnit_G);
                Channel1_OutputUnit.Items.Remove(Channel1_OutputUnit_Ips);
                Channel1_OutputUnit.Items.Remove(Channel1_OutputUnit_Mmps);
                Channel1_OutputUnit.Items.Remove(Channel1_OutputUnit_Mps2);
                //Channel2_SensorUnit.Items.Add(Channel2_SensorUnit_VoltPerInch);
                //Channel2_SensorUnit.Items.Add(Channel2_SensorUnit_VoltPerMm);
                Channel2_SensorUnit.Items.Remove(Channel2_SensorUnit_MvPerG);
                Channel2_SensorUnit.Items.Remove(Channel2_SensorUnit_MvPerIps);
                Channel2_SensorUnit.Items.Remove(Channel2_SensorUnit_MvPerMmps);
                Channel2_OutputUnit.Items.Remove(Channel2_OutputUnit_G);
                Channel2_OutputUnit.Items.Remove(Channel2_OutputUnit_Ips);
                Channel2_OutputUnit.Items.Remove(Channel2_OutputUnit_Mmps);
                Channel2_OutputUnit.Items.Remove(Channel2_OutputUnit_Mps2);

                //Channel1_SensorUnit_VoltPerInch.IsEnabled = true;
                //Channel1_SensorUnit_VoltPerMm.IsEnabled = true;
                //Channel1_SensorUnit_MvPerG.IsEnabled = false;
                //Channel1_SensorUnit_MvPerIps.IsEnabled = false;
                //Channel1_SensorUnit_MvPerMmps.IsEnabled = false;
                //Channel1_OutputUnit_G.IsEnabled = false;
                //Channel1_OutputUnit_Ips.IsEnabled = false;
                //Channel1_OutputUnit_Mmps.IsEnabled = false;
                //Channel1_OutputUnit_Mps2.IsEnabled = false;
                //Channel2_SensorUnit_VoltPerInch.IsEnabled = true;
                //Channel2_SensorUnit_VoltPerMm.IsEnabled = true;
                //Channel2_SensorUnit_MvPerG.IsEnabled = false;
                //Channel2_SensorUnit_MvPerIps.IsEnabled = false;
                //Channel2_SensorUnit_MvPerMmps.IsEnabled = false;
                //Channel2_OutputUnit_G.IsEnabled = false;
                //Channel2_OutputUnit_Ips.IsEnabled = false;
                //Channel2_OutputUnit_Mmps.IsEnabled = false;
                //Channel2_OutputUnit_Mps2.IsEnabled = false;
                //Channel1_OutputUnit_G.IsEnabled = false;
                //Channel1_OutputUnit_Ips.IsEnabled = false;
                //Channel1_OutputUnit_Mmps.IsEnabled = false;
                //Channel1_OutputUnit_Mps2.IsEnabled = false;
                //Channel2_OutputUnit_G.IsEnabled = false;
                //Channel2_OutputUnit_Ips.IsEnabled = false;
                //Channel2_OutputUnit_Mmps.IsEnabled = false;
                //Channel2_OutputUnit_Mps2.IsEnabled = false;

            }
            //Product_DeviceModel.Text = Product_DeviceModel;
            //Product_DeviceModel.Content = Information.DeviceModel;
            //Connect_Information();
        }
        private void Relay_Logic_Information()
        {
            try
            {
                if(Relay1_Channel.SelectedItem== Relay_1_Channel1&& Relay2_Channel.SelectedItem == Relay_2_Channel1)
                {
                    Relay1_Channel_RelayLogicOperation_And.IsEnabled = false;
                    Relay1_Channel_RelayLogicOperation_Or.IsEnabled = false;
                    Relay2_Channel_RelayLogicOperation_And.IsEnabled = false;
                    Relay2_Channel_RelayLogicOperation_Or.IsEnabled = false;
                    Relay1_ThresholdOther.IsEnabled = false;
                    Relay2_ThresholdOther.IsEnabled = false;
                    Ch1_Threshold_Unitstring.Text = ChannelParamenters[0].OutputUnitStr;
                    Ch1_Thresholdother_Unitstring.Text = ChannelParamenters[0].OutputUnitStr;
                    Ch2_Threshold_Unitstring.Text = ChannelParamenters[0].OutputUnitStr;
                    Ch2_Thresholdother_Unitstring.Text = ChannelParamenters[0].OutputUnitStr;
                    Relay1_RelayLogicOperation.SelectedItem = Relay1_Channel_RelayLogicOperation_None;
                    Relay2_RelayLogicOperation.SelectedItem = Relay2_Channel_RelayLogicOperation_None;
                }
                else if(Relay1_Channel.SelectedItem == Relay_1_Channel2 && Relay2_Channel.SelectedItem == Relay_2_Channel2)
                {
                    Relay1_Channel_RelayLogicOperation_And.IsEnabled = false;
                    Relay1_Channel_RelayLogicOperation_Or.IsEnabled = false;
                    Relay2_Channel_RelayLogicOperation_And.IsEnabled = false;
                    Relay2_Channel_RelayLogicOperation_Or.IsEnabled = false;
                    Relay1_ThresholdOther.IsEnabled = false;
                    Relay2_ThresholdOther.IsEnabled = false;
                    Ch1_Threshold_Unitstring.Text = ChannelParamenters[1].OutputUnitStr;
                    Ch1_Thresholdother_Unitstring.Text = ChannelParamenters[1].OutputUnitStr;
                    Ch2_Threshold_Unitstring.Text = ChannelParamenters[1].OutputUnitStr;
                    Ch2_Thresholdother_Unitstring.Text = ChannelParamenters[1].OutputUnitStr;
                    Relay1_RelayLogicOperation.SelectedItem = Relay1_Channel_RelayLogicOperation_None;
                    Relay2_RelayLogicOperation.SelectedItem = Relay2_Channel_RelayLogicOperation_None;

                }
                else
                {
                    Relay1_Channel_RelayLogicOperation_And.IsEnabled = true;
                    Relay1_Channel_RelayLogicOperation_Or.IsEnabled = true;
                    Relay2_Channel_RelayLogicOperation_And.IsEnabled = true;
                    Relay2_Channel_RelayLogicOperation_Or.IsEnabled = true;
                    Relay1_ThresholdOther.IsEnabled = true;
                    Relay2_ThresholdOther.IsEnabled = true;
                    //Relay1_Channel_Config();
                    //Relay2_Channel_Config();

                }
                if (Relay1_RelayLogicOperation.SelectedItem== Relay1_Channel_RelayLogicOperation_None)
                    {
                        Relay1_ThresholdOther.IsEnabled = false;
                    }
                    if(Relay2_RelayLogicOperation.SelectedItem == Relay2_Channel_RelayLogicOperation_None)
                    {
                        Relay2_ThresholdOther.IsEnabled = false;
                    }

            }
            catch(Exception)
            {

            }
        }
        private void Relay_ALL_Information()
        {
            Relay1_Channel_Config();
            Relay1_State();
            Relay1_LogicOperation_Information();
            Relay1_SelfLatching_Information();
            Relay2_Channel_Config();
            Relay2_State();
            Relay2_LogicOperation_Information();
            Relay2_SelfLatching_Information();
            Relay_1_Information();
            Relay_2_Information();
            Relay_Logic_Information();
        }
        private void Relay_1_Information()
        {
            if (RelayConfig[0].ChannelSelect == 0)
            {
                //Channel2_UintString.Content = ChannelParamenters[1].OutputUnitStr;
                //Channel2_UintString.Content = ChannelParamenters[1].OutputUnitStr;
                //Relay1_State();
                //Relay1_Channel_Config();

                //Relay1_LogicOperation_Information();
                //Relay1_SelfLatching_Information();
                //Relay_Logic_Information();

                float DelayTimeMS1_Ch1;
                DelayTimeMS1_Ch1 = Convert.ToSingle(RelayConfig[0].DelayTimeMs) / 1000;
                Relay1_DelayTimeMs.Text = DelayTimeMS1_Ch1.ToString("F3");
                if (ChannelParamenters[0].TConfig == DpsDevice.TConfig.Position)
                {
                    float position_threshold1, position_thresholdOther1;
                    position_threshold1 = RelayConfig[0].Threshold * 100;
                    Relay1_Threshold.Text = position_threshold1.ToString("F3");
                     Ch1_Threshold_Unitstring.Text = ChannelParamenters[0].OutputUnitStr;
                    if (ChannelParamenters[1].TConfig == DpsDevice.TConfig.Position)
                    {
                        position_thresholdOther1 = RelayConfig[0].ThresholdOther * 100;
                        Relay1_ThresholdOther.Text = position_thresholdOther1.ToString("F3");
                        Ch1_Thresholdother_Unitstring.Text = ChannelParamenters[1].OutputUnitStr;
                    }
                    else if (ChannelParamenters[1].TConfig == DpsDevice.TConfig.Vibration)
                    {
                        position_thresholdOther1 = RelayConfig[0].ThresholdOther * ChannelParamenters[1].OutputFullScale;
                        Relay1_ThresholdOther.Text = position_thresholdOther1.ToString("F3");
                        Ch1_Thresholdother_Unitstring.Text = ChannelParamenters[1].OutputUnitStr;
                    }
                    else if (ChannelParamenters[1].TConfig == DpsDevice.TConfig.Tacho)
                    {
                        position_thresholdOther1 = RelayConfig[0].ThresholdOther * ChannelParamenters[1].TachoRange;
                        Relay1_ThresholdOther.Text = position_thresholdOther1.ToString("F3");
                        Ch1_Thresholdother_Unitstring.Text = ChannelParamenters[1].OutputUnitStr;
                    }

                }
                else if (ChannelParamenters[0].TConfig == DpsDevice.TConfig.Vibration)
                {
                    float vibration_threshold1, vibration_thresholdOther1;
                    vibration_threshold1 = RelayConfig[0].Threshold * ChannelParamenters[0].OutputFullScale;
                    Relay1_Threshold.Text = vibration_threshold1.ToString("F3");
                    Ch1_Threshold_Unitstring.Text = ChannelParamenters[0].OutputUnitStr;
                    if (ChannelParamenters[1].TConfig == DpsDevice.TConfig.Position)
                    {
                        vibration_thresholdOther1 = RelayConfig[0].ThresholdOther * 100;
                        Relay1_ThresholdOther.Text = vibration_thresholdOther1.ToString("F3");
                        Ch1_Thresholdother_Unitstring.Text = ChannelParamenters[1].OutputUnitStr;
                    }
                    else if (ChannelParamenters[1].TConfig == DpsDevice.TConfig.Vibration)
                    {
                        vibration_thresholdOther1 = RelayConfig[0].ThresholdOther * ChannelParamenters[1].OutputFullScale;
                        Relay1_ThresholdOther.Text = vibration_thresholdOther1.ToString("F3");
                        Ch1_Thresholdother_Unitstring.Text = ChannelParamenters[1].OutputUnitStr;
                    }
                    else if (ChannelParamenters[1].TConfig == DpsDevice.TConfig.Tacho)
                    {
                        vibration_thresholdOther1 = RelayConfig[0].ThresholdOther * ChannelParamenters[1].TachoRange;
                        Relay1_ThresholdOther.Text = vibration_thresholdOther1.ToString("F3");
                        Ch1_Thresholdother_Unitstring.Text = ChannelParamenters[1].OutputUnitStr;
                    }

                }
                else if (ChannelParamenters[0].TConfig == DpsDevice.TConfig.Tacho)
                {
                    float tacho_threshold1, tacho_thresholdOther1;
                    tacho_threshold1 = RelayConfig[0].Threshold * ChannelParamenters[0].TachoRange;

                    Relay1_Threshold.Text = tacho_threshold1.ToString("F3");
                    Ch1_Threshold_Unitstring.Text = ChannelParamenters[0].OutputUnitStr;
                    if (ChannelParamenters[1].TConfig == DpsDevice.TConfig.Position)
                    {
                        tacho_thresholdOther1 = RelayConfig[0].ThresholdOther * 100;
                        Relay1_ThresholdOther.Text = tacho_thresholdOther1.ToString("F3");
                        Ch1_Thresholdother_Unitstring.Text = ChannelParamenters[1].OutputUnitStr;
                    }
                    else if (ChannelParamenters[1].TConfig == DpsDevice.TConfig.Vibration)
                    {
                        tacho_thresholdOther1 = RelayConfig[0].ThresholdOther * ChannelParamenters[1].OutputFullScale;
                        Relay1_ThresholdOther.Text = tacho_thresholdOther1.ToString("F3");
                        Ch1_Thresholdother_Unitstring.Text = ChannelParamenters[1].OutputUnitStr;
                    }
                    else if (ChannelParamenters[1].TConfig == DpsDevice.TConfig.Tacho)
                    {
                        tacho_thresholdOther1 = RelayConfig[0].ThresholdOther * ChannelParamenters[1].TachoRange;
                        Relay1_ThresholdOther.Text = tacho_thresholdOther1.ToString("F3");
                        Ch1_Thresholdother_Unitstring.Text = ChannelParamenters[1].OutputUnitStr;
                    }

                }

            }
            //Channel1_UintString.Content = ChannelParamenters[0].OutputUnitStr;
            else if (RelayConfig[0].ChannelSelect == 1)
            {
                //Relay2_State();
                //Relay2_Channel_Config();
                //Relay2_LogicOperation_Information();
                //Relay2_SelfLatching_Information();
                //Relay1_State();
                //Relay1_Channel_Config();
                //Relay1_LogicOperation_Information();
                //Relay1_SelfLatching_Information();
                //Relay_Logic_Information();
                float DelayTimeMS1_Ch2;
                DelayTimeMS1_Ch2 = Convert.ToSingle(RelayConfig[0].DelayTimeMs) / 1000;
                Relay1_DelayTimeMs.Text = DelayTimeMS1_Ch2.ToString("F3");
                if (ChannelParamenters[1].TConfig == DpsDevice.TConfig.Position)
                {
                    float position_threshold1, position_thresholdOther1;
                    position_threshold1 = RelayConfig[0].Threshold * 100;
                    Relay1_Threshold.Text = position_threshold1.ToString("F3");
                    Ch1_Threshold_Unitstring.Text = ChannelParamenters[1].OutputUnitStr;
                    if (ChannelParamenters[0].TConfig == DpsDevice.TConfig.Position)
                    {
                        position_thresholdOther1 = RelayConfig[0].ThresholdOther * 100;
                        Relay1_ThresholdOther.Text = position_thresholdOther1.ToString("F3");
                        Ch1_Thresholdother_Unitstring.Text = ChannelParamenters[0].OutputUnitStr;
                    }
                    else if (ChannelParamenters[0].TConfig == DpsDevice.TConfig.Vibration)
                    {
                        position_thresholdOther1 = RelayConfig[0].ThresholdOther * ChannelParamenters[0].OutputFullScale;
                        Relay1_ThresholdOther.Text = position_thresholdOther1.ToString("F3");
                        Ch1_Thresholdother_Unitstring.Text = ChannelParamenters[0].OutputUnitStr;
                    }
                    else if (ChannelParamenters[0].TConfig == DpsDevice.TConfig.Tacho)
                    {
                        position_thresholdOther1 = RelayConfig[0].ThresholdOther * ChannelParamenters[0].TachoRange;
                        Relay1_ThresholdOther.Text = position_thresholdOther1.ToString("F3");
                        Ch1_Thresholdother_Unitstring.Text = ChannelParamenters[0].OutputUnitStr;
                    }

                }
                else if (ChannelParamenters[1].TConfig == DpsDevice.TConfig.Vibration)
                {
                    float vibration_threshold1, vibration_thresholdOther1;
                    vibration_threshold1 = RelayConfig[0].Threshold * ChannelParamenters[1].OutputFullScale;
                    Relay1_Threshold.Text = vibration_threshold1.ToString("F3");
                    Ch1_Threshold_Unitstring.Text = ChannelParamenters[1].OutputUnitStr;
                    if (ChannelParamenters[0].TConfig == DpsDevice.TConfig.Position)
                    {
                        vibration_thresholdOther1 = RelayConfig[0].ThresholdOther * 100;
                        Relay1_ThresholdOther.Text = vibration_thresholdOther1.ToString("F3");
                        Ch1_Thresholdother_Unitstring.Text = ChannelParamenters[0].OutputUnitStr;
                    }
                    else if (ChannelParamenters[0].TConfig == DpsDevice.TConfig.Vibration)
                    {
                        vibration_thresholdOther1 = RelayConfig[0].ThresholdOther * ChannelParamenters[0].OutputFullScale;
                        Relay1_ThresholdOther.Text = vibration_thresholdOther1.ToString("F3");
                        Ch1_Thresholdother_Unitstring.Text = ChannelParamenters[0].OutputUnitStr;
                    }
                    else if (ChannelParamenters[0].TConfig == DpsDevice.TConfig.Tacho)
                    {
                        vibration_thresholdOther1 = RelayConfig[0].ThresholdOther * ChannelParamenters[0].TachoRange;
                        Relay1_ThresholdOther.Text = vibration_thresholdOther1.ToString("F3");
                        Ch1_Thresholdother_Unitstring.Text = ChannelParamenters[0].OutputUnitStr;
                    }


                }
                else if (ChannelParamenters[1].TConfig == DpsDevice.TConfig.Tacho)
                {
                    float tacho_threshold1, tacho_thresholdOther1;
                    tacho_threshold1 = RelayConfig[0].Threshold * ChannelParamenters[1].TachoRange;
                    Relay1_Threshold.Text = tacho_threshold1.ToString("F3");
                    Ch1_Threshold_Unitstring.Text = ChannelParamenters[1].OutputUnitStr;
                    if (ChannelParamenters[0].TConfig == DpsDevice.TConfig.Position)
                    {
                        tacho_thresholdOther1 = RelayConfig[0].ThresholdOther * 100;
                        Relay1_ThresholdOther.Text = tacho_thresholdOther1.ToString("F3");
                        Ch1_Thresholdother_Unitstring.Text = ChannelParamenters[0].OutputUnitStr;
                    }
                    else if (ChannelParamenters[0].TConfig == DpsDevice.TConfig.Vibration)
                    {
                        tacho_thresholdOther1 = RelayConfig[0].ThresholdOther * ChannelParamenters[0].OutputFullScale;
                        Relay1_ThresholdOther.Text = tacho_thresholdOther1.ToString("F3");
                        Ch1_Thresholdother_Unitstring.Text = ChannelParamenters[0].OutputUnitStr;
                    }
                    else if (ChannelParamenters[0].TConfig == DpsDevice.TConfig.Tacho)
                    {
                        tacho_thresholdOther1 = RelayConfig[0].ThresholdOther * ChannelParamenters[0].TachoRange;
                        Relay1_ThresholdOther.Text = tacho_thresholdOther1.ToString("F3");
                        Ch1_Thresholdother_Unitstring.Text = ChannelParamenters[0].OutputUnitStr;
                    }


                }
            }

        }

        private void Relay_2_Information()
        {
            if (RelayConfig[1].ChannelSelect == 0)
            {

                //Channel1_UintString.Content = ChannelParamenters[0].OutputUnitStr;
                //Channel2_UintString.Content = ChannelParamenters[1].OutputUnitStr;
                //Relay2_State();
                //Relay2_Channel_Config();
                //Relay2_LogicOperation_Information();
                //Relay2_SelfLatching_Information();
                //Relay_Logic_Information();
                float DelayTimeMS1;
                DelayTimeMS1 = Convert.ToSingle(RelayConfig[1].DelayTimeMs) / 1000;
                Relay2_DelayTimeMs.Text = DelayTimeMS1.ToString("F3");
                if (ChannelParamenters[0].TConfig == DpsDevice.TConfig.Position)
                {
                    float position_threshold2, position_thresholdOther2;
                    position_threshold2 = RelayConfig[1].Threshold * 100;
                    Relay2_Threshold.Text = position_threshold2.ToString("F3");
                    Ch2_Threshold_Unitstring.Text = ChannelParamenters[0].OutputUnitStr;
                    if (ChannelParamenters[1].TConfig == DpsDevice.TConfig.Position)
                    {
                        position_thresholdOther2 = RelayConfig[1].ThresholdOther * 100;
                        Relay2_ThresholdOther.Text = position_thresholdOther2.ToString("F3");
                        Ch2_Thresholdother_Unitstring.Text = ChannelParamenters[1].OutputUnitStr;
                    }
                    else if (ChannelParamenters[1].TConfig == DpsDevice.TConfig.Vibration)
                    {
                        position_thresholdOther2 = RelayConfig[1].ThresholdOther * ChannelParamenters[1].OutputFullScale;
                        Relay2_ThresholdOther.Text = position_thresholdOther2.ToString("F3");
                        Ch2_Thresholdother_Unitstring.Text = ChannelParamenters[1].OutputUnitStr;
                    }
                    else if (ChannelParamenters[1].TConfig == DpsDevice.TConfig.Tacho)
                    {
                        position_thresholdOther2 = RelayConfig[1].ThresholdOther * ChannelParamenters[1].TachoRange;
                        Relay2_ThresholdOther.Text = position_thresholdOther2.ToString("F3");
                        Ch2_Thresholdother_Unitstring.Text = ChannelParamenters[1].OutputUnitStr;
                    }

                }
                else if (ChannelParamenters[0].TConfig == DpsDevice.TConfig.Vibration)
                {
                    float vibration_threshold2, vibration_thresholdOther2;
                    vibration_threshold2 = RelayConfig[1].Threshold * ChannelParamenters[0].OutputFullScale;
                    Relay2_Threshold.Text = vibration_threshold2.ToString("F3");
                    Ch2_Threshold_Unitstring.Text = ChannelParamenters[0].OutputUnitStr;
                    if (ChannelParamenters[1].TConfig == DpsDevice.TConfig.Position)
                    {
                        vibration_thresholdOther2 = RelayConfig[1].ThresholdOther * 100;
                        Relay2_ThresholdOther.Text = vibration_thresholdOther2.ToString("F3");
                        Ch2_Thresholdother_Unitstring.Text = ChannelParamenters[1].OutputUnitStr;
                    }
                    else if (ChannelParamenters[1].TConfig == DpsDevice.TConfig.Vibration)
                    {
                        vibration_thresholdOther2 = RelayConfig[1].ThresholdOther * ChannelParamenters[1].OutputFullScale;
                        Relay2_ThresholdOther.Text = vibration_thresholdOther2.ToString("F3");
                        Ch2_Thresholdother_Unitstring.Text = ChannelParamenters[1].OutputUnitStr;
                    }
                    else if (ChannelParamenters[1].TConfig == DpsDevice.TConfig.Tacho)
                    {
                        vibration_thresholdOther2 = RelayConfig[1].ThresholdOther * ChannelParamenters[1].TachoRange;
                        Relay2_ThresholdOther.Text = vibration_thresholdOther2.ToString("F3");
                        Ch2_Thresholdother_Unitstring.Text = ChannelParamenters[1].OutputUnitStr;
                    }


                }
                else if (ChannelParamenters[0].TConfig == DpsDevice.TConfig.Tacho)
                {
                    float tacho_threshold2, tacho_thresholdOther2;
                    tacho_threshold2 = RelayConfig[1].Threshold * ChannelParamenters[0].TachoRange;
                    Relay2_Threshold.Text = tacho_threshold2.ToString("F3");
                    Ch2_Threshold_Unitstring.Text = ChannelParamenters[0].OutputUnitStr;
                    if (ChannelParamenters[1].TConfig == DpsDevice.TConfig.Position)
                    {
                        tacho_thresholdOther2 = RelayConfig[1].ThresholdOther * 100;
                        Relay2_ThresholdOther.Text = tacho_thresholdOther2.ToString("F3");
                        Ch2_Thresholdother_Unitstring.Text = ChannelParamenters[1].OutputUnitStr;
                    }
                    else if (ChannelParamenters[1].TConfig == DpsDevice.TConfig.Vibration)
                    {
                        tacho_thresholdOther2 = RelayConfig[1].ThresholdOther * ChannelParamenters[1].OutputFullScale;
                        Relay2_ThresholdOther.Text = tacho_thresholdOther2.ToString("F3");
                        Ch2_Thresholdother_Unitstring.Text = ChannelParamenters[1].OutputUnitStr;
                    }
                    else if (ChannelParamenters[1].TConfig == DpsDevice.TConfig.Tacho)
                    {
                        tacho_thresholdOther2 = RelayConfig[1].ThresholdOther * ChannelParamenters[1].TachoRange;
                        Relay2_ThresholdOther.Text = tacho_thresholdOther2.ToString("F3");
                        Ch2_Thresholdother_Unitstring.Text = ChannelParamenters[1].OutputUnitStr;
                    }

                }
            }
            else if (RelayConfig[1].ChannelSelect == 1)
            {
                //Relay2_State();
                //Relay2_Channel_Config();
                //Relay2_LogicOperation_Information();
                //Relay2_SelfLatching_Information();
                //Relay_Logic_Information();
                float DelayTimeMS2;
                DelayTimeMS2 = Convert.ToSingle(RelayConfig[1].DelayTimeMs) / 1000;
                Relay2_DelayTimeMs.Text = DelayTimeMS2.ToString("F3");
                if (ChannelParamenters[1].TConfig == DpsDevice.TConfig.Position)
                {
                    float position_threshold2, position_thresholdOther2;
                    position_threshold2 = RelayConfig[1].Threshold * 100;
                    Relay2_Threshold.Text = position_threshold2.ToString("F3");
                    Ch2_Threshold_Unitstring.Text = ChannelParamenters[1].OutputUnitStr;
                    if (ChannelParamenters[0].TConfig == DpsDevice.TConfig.Position)
                    {
                        position_thresholdOther2 = RelayConfig[1].ThresholdOther * 100;
                        Relay2_ThresholdOther.Text = position_thresholdOther2.ToString("F3");
                        Ch2_Thresholdother_Unitstring.Text = ChannelParamenters[0].OutputUnitStr;
                    }
                    else if (ChannelParamenters[0].TConfig == DpsDevice.TConfig.Vibration)
                    {
                        position_thresholdOther2 = RelayConfig[1].ThresholdOther * ChannelParamenters[0].OutputFullScale;
                        Relay2_ThresholdOther.Text = position_thresholdOther2.ToString("F3");
                        Ch2_Thresholdother_Unitstring.Text = ChannelParamenters[0].OutputUnitStr;
                    }
                    else if (ChannelParamenters[0].TConfig == DpsDevice.TConfig.Tacho)
                    {
                        position_thresholdOther2 = RelayConfig[1].ThresholdOther * ChannelParamenters[0].TachoRange;
                        Relay2_ThresholdOther.Text = position_thresholdOther2.ToString("F3");
                        Ch2_Thresholdother_Unitstring.Text = ChannelParamenters[0].OutputUnitStr;
                    }


                }
                else if (ChannelParamenters[1].TConfig == DpsDevice.TConfig.Vibration)
                {
                    float vibration_threshold2, vibration_thresholdOther2;
                    vibration_threshold2 = RelayConfig[1].Threshold * ChannelParamenters[1].OutputFullScale;
                    Relay2_Threshold.Text = vibration_threshold2.ToString("F3");
                    Ch2_Threshold_Unitstring.Text = ChannelParamenters[1].OutputUnitStr;
                    if (ChannelParamenters[0].TConfig == DpsDevice.TConfig.Position)
                    {
                        vibration_thresholdOther2 = RelayConfig[1].ThresholdOther * 100;
                        Relay2_ThresholdOther.Text = vibration_thresholdOther2.ToString("F3");
                        Ch2_Thresholdother_Unitstring.Text = ChannelParamenters[0].OutputUnitStr;
                    }
                    else if (ChannelParamenters[0].TConfig == DpsDevice.TConfig.Vibration)
                    {
                        vibration_thresholdOther2 = RelayConfig[1].ThresholdOther * ChannelParamenters[0].OutputFullScale;
                        Relay2_ThresholdOther.Text = vibration_thresholdOther2.ToString("F3");
                        Ch2_Thresholdother_Unitstring.Text = ChannelParamenters[0].OutputUnitStr;
                    }
                    else if (ChannelParamenters[0].TConfig == DpsDevice.TConfig.Tacho)
                    {
                        vibration_thresholdOther2 = RelayConfig[1].ThresholdOther * ChannelParamenters[0].TachoRange;
                        Relay2_ThresholdOther.Text = vibration_thresholdOther2.ToString("F3");
                        Ch2_Thresholdother_Unitstring.Text = ChannelParamenters[0].OutputUnitStr;
                    }

                }
                else if (ChannelParamenters[1].TConfig == DpsDevice.TConfig.Tacho)
                {
                    float tacho_threshold2, tacho_thresholdOther2;
                    tacho_threshold2 = RelayConfig[1].Threshold * ChannelParamenters[1].TachoRange;
                    Relay2_Threshold.Text = tacho_threshold2.ToString("F3");
                    Ch2_Threshold_Unitstring.Text = ChannelParamenters[1].OutputUnitStr;
                    if (ChannelParamenters[0].TConfig == DpsDevice.TConfig.Position)
                    {
                        tacho_thresholdOther2 = RelayConfig[1].ThresholdOther * 100;
                        Relay2_ThresholdOther.Text = tacho_thresholdOther2.ToString("F3");
                        Ch2_Thresholdother_Unitstring.Text = ChannelParamenters[0].OutputUnitStr;
                    }
                    else if (ChannelParamenters[0].TConfig == DpsDevice.TConfig.Vibration)
                    {
                        tacho_thresholdOther2 = RelayConfig[1].ThresholdOther * ChannelParamenters[0].OutputFullScale;
                        Relay2_ThresholdOther.Text = tacho_thresholdOther2.ToString("F3");
                        Ch2_Thresholdother_Unitstring.Text = ChannelParamenters[0].OutputUnitStr;
                    }
                    else if (ChannelParamenters[0].TConfig == DpsDevice.TConfig.Tacho)
                    {
                        tacho_thresholdOther2 = RelayConfig[1].ThresholdOther * ChannelParamenters[0].TachoRange;
                        Relay2_ThresholdOther.Text = tacho_thresholdOther2.ToString("F3");
                        Ch2_Thresholdother_Unitstring.Text = ChannelParamenters[0].OutputUnitStr;
                    }

                }

            }
            
        }
        private void Interface_Channel1_Information()
        {
            try
            {
                Channel1_UintString.Content = ChannelParamenters[0].OutputUnitStr;                
                Channel2_Universal.Visibility = Visibility.Hidden;
                Channel2_Position.Visibility = Visibility.Hidden;
                Channel2_Vibration.Visibility = Visibility.Hidden;
                Channel2_Tacho.Visibility = Visibility.Hidden;
                Relay_ALL_Information();
                float DampingTime, RslTime;/*, DelayTimeMS*/;
                DampingTime =Convert.ToSingle (ChannelParamenters[0].DampingTimeMs)/1000;
                RslTime= Convert.ToSingle(ChannelParamenters[0].RslTimeMs) / 1000;
                if (ChannelParamenters[0].TConfig == DpsDevice.TConfig.Position)
                {
                    Txtb_Channel1_Type.Text = "轴向位移";
                    Channel1_Universal.Visibility = Visibility.Visible;
                    Channel1_Position.Visibility = Visibility.Visible;
                    Channel1_Vibration.Visibility = Visibility.Hidden;
                    Channel1_Tacho.Visibility = Visibility.Hidden;
                    Channel1_Type.SelectedItem= Channel1_Position_Choose;
                    Channel1_DampingTimeMs.Text = DampingTime.ToString();
                    Channel1_PositionLowerVoltage.Text = ChannelParamenters[0].PositionLowerVoltage.ToString();
                    Channel1_PositionUpperVoltage.Text = ChannelParamenters[0].PositionUpperVoltage.ToString();


                }
                else if (ChannelParamenters[0].TConfig == DpsDevice.TConfig.Vibration)
                {
                    Txtb_Channel1_Type.Text = "壳体振动";
                    Channel1_Universal.Visibility = Visibility.Visible;
                    Channel1_Position.Visibility = Visibility.Hidden;
                    Channel1_Vibration.Visibility = Visibility.Visible;
                    Channel1_Tacho.Visibility = Visibility.Hidden;
                    Channel1_Type.SelectedItem = Channel1_Vibration_Choose;
                    Channel1_DampingTimeMs.Text = DampingTime.ToString();
                    Channel1_SensorSensitivity.Text = ChannelParamenters[0].SensorSensitivity.ToString("F3");
                    Channel1_SensorUnit_Information();
                    Channel1_OutputFullScale.Text = ChannelParamenters[0].OutputFullScale.ToString("F3");
                    Channel1_OutputUnit_Information();
                    Channel1_OutputType_Information();
                    float noiseGate1;
                    noiseGate1 = ChannelParamenters[0].NoiseGate * ChannelParamenters[0].OutputFullScale;
                    Channel1_NoiseGate.Text = noiseGate1.ToString("F3");
                    Channel1_OutputGain.Text = ChannelParamenters[0].OutputGain.ToString("F3");
                    Channel1_LfCutoffFreq.Text = ChannelParamenters[0].LfCutoffFreq.ToString("F3");
                    Channel1_HfCutoffFreq.Text = ChannelParamenters[0].HfCutoffFreq.ToString("F3");
                    Channel1_RslTimeMs.Text = RslTime.ToString("F3");


                }
                else if (ChannelParamenters[0].TConfig == DpsDevice.TConfig.Tacho)
                {
                    float tacho1;
                    Txtb_Channel1_Type.Text = "转速/键相位";
                    Channel1_Universal.Visibility = Visibility.Visible;
                    Channel1_Position.Visibility = Visibility.Hidden;
                    Channel1_Vibration.Visibility = Visibility.Hidden;
                    Channel1_Tacho.Visibility = Visibility.Visible;
                    Channel1_Type.SelectedItem = Channel1_Tacho_Choose;
                    Channel1_TachoRange.Text = ChannelParamenters[0].TachoRange.ToString("F3");
                    tacho1 = 60 / ChannelParamenters[0].TachoNormalizer;
                    Channel1_TachoNormalizer.Text = tacho1.ToString("F3");
                    Channel1_TachoTriggerThreshold.Text = ChannelParamenters[0].TachoTriggerThreshold.ToString("F3");
                    Channel1_DampingTimeMs.Text = DampingTime.ToString();

                }



            }
            catch (Exception)
            {
                //MessageBox.Show(e.ToString(), "ERROR");

            }

        }
        private void Channel1_SensorUnit_Information()
        {
            if (ChannelParamenters[0].SensorUnit == DpsDevice.SensorUnit.MvPerG)
            {
                Channel1_SensorUnit.SelectedItem = Channel1_SensorUnit_MvPerG;
            }
            else if (ChannelParamenters[0].SensorUnit == DpsDevice.SensorUnit.MvPerIps)
            {
                Channel1_SensorUnit.SelectedItem = Channel1_SensorUnit_MvPerIps;
            }
            else if (ChannelParamenters[0].SensorUnit == DpsDevice.SensorUnit.MvPerMmps)
            {
                Channel1_SensorUnit.SelectedItem = Channel1_SensorUnit_MvPerMmps;
            }
            else if (ChannelParamenters[0].SensorUnit == DpsDevice.SensorUnit.VoltPerInch)
            {
                Channel1_SensorUnit.SelectedItem = Channel1_SensorUnit_VoltPerInch;
            }
            else if (ChannelParamenters[0].SensorUnit == DpsDevice.SensorUnit.VoltPerMm)
            {
                Channel1_SensorUnit.SelectedItem = Channel1_SensorUnit_VoltPerMm;
            }
        }
        private void Channel1_OutputUnit_Information()
        {
            if (ChannelParamenters[0].OutputUnit == DpsDevice.OutputUnit.G)
            {
                Channel1_OutputUnit.SelectedItem = Channel1_OutputUnit_G;
            }
           else if (ChannelParamenters[0].OutputUnit == DpsDevice.OutputUnit.Ips)
            {
                Channel1_OutputUnit.SelectedItem = Channel1_OutputUnit_Ips;
            }
            else if (ChannelParamenters[0].OutputUnit == DpsDevice.OutputUnit.Mil)
            {
                Channel1_OutputUnit.SelectedItem = Channel1_OutputUnit_Mil;
            }
            else if (ChannelParamenters[0].OutputUnit == DpsDevice.OutputUnit.Mm)
            {
                Channel1_OutputUnit.SelectedItem = Channel1_OutputUnit_Mm;
            }
            else if (ChannelParamenters[0].OutputUnit == DpsDevice.OutputUnit.Mmps)
            {
                Channel1_OutputUnit.SelectedItem = Channel1_OutputUnit_Mmps;
            }
            else if (ChannelParamenters[0].OutputUnit == DpsDevice.OutputUnit.Mps2)
            {
                Channel1_OutputUnit.SelectedItem = Channel1_OutputUnit_Mps2;
            }
            else if (ChannelParamenters[0].OutputUnit == DpsDevice.OutputUnit.Um)
            {
                Channel1_OutputUnit.SelectedItem = Channel1_OutputUnit_Um;
            }

        }
        private void Channel1_OutputType_Information()
        {
            if (ChannelParamenters[0].OutputType == DpsDevice.OutputType.Peak)
            {
                Channel1_OutputType.SelectedItem = Channel1_OutputType_Peak;
            }
            else if (ChannelParamenters[0].OutputType == DpsDevice.OutputType.Pkpk)
            {
                Channel1_OutputType.SelectedItem = Channel1_OutputType_Pkpk;
            }
            else if (ChannelParamenters[0].OutputType == DpsDevice.OutputType.Rms)
            {
                Channel1_OutputType.SelectedItem = Channel1_OutputType_Rms;
            }
        }


        private async void Channel1_SendTypeInformation()
        {
            try
            {         

                if (Channel1_Type.SelectedItem == Channel1_Position_Choose)
                {
                    ChannelParamenters[0].TConfig = DpsDevice.TConfig.Position;
                }
                else if(Channel1_Type.SelectedItem == Channel1_Vibration_Choose)
                {
                    ChannelParamenters[0].TConfig = DpsDevice.TConfig.Vibration;

                }
                else if (Channel1_Type.SelectedItem == Channel1_Tacho_Choose)
                {
                    ChannelParamenters[0].TConfig = DpsDevice.TConfig.Tacho;
                }
                ChannelParamenters[0] = await device.SetChannelParameters(0, ChannelParamenters[0]);


            }
            catch (Exception)
            {
                //MessageBox.Show(e.ToString(), "ERROR");
            }
        }
        private  void Interface_Channel2_Information()
        {
            try
            {


                Channel2_UintString.Content = ChannelParamenters[1].OutputUnitStr;
                Channel1_Universal.Visibility = Visibility.Hidden;
                Channel1_Position.Visibility = Visibility.Hidden;
                Channel1_Vibration.Visibility = Visibility.Hidden;
                Channel1_Tacho.Visibility = Visibility.Hidden;
                Relay_ALL_Information();

                float DampingTime, RslTime;/*, DelayTimeMS;*/
                DampingTime = Convert.ToSingle(ChannelParamenters[1].DampingTimeMs) / 1000;
                RslTime = Convert.ToSingle(ChannelParamenters[1].RslTimeMs) / 1000;
                if (ChannelParamenters[1].TConfig == DpsDevice.TConfig.Position)
                {
                    Txtb_Channel2_Type.Text = "轴向位移";
                    Channel2_Universal.Visibility = Visibility.Visible;
                    Channel2_Position.Visibility = Visibility.Visible;
                    Channel2_Vibration.Visibility = Visibility.Hidden;
                    Channel2_Tacho.Visibility = Visibility.Hidden;
                    Channel2_Type.SelectedItem = Channel2_Position_Choose;
                    Channel2_DampingTimeMs.Text = DampingTime.ToString();
                    Channel2_PositionLowerVoltage.Text = ChannelParamenters[1].PositionLowerVoltage.ToString();
                    Channel2_PositionUpperVoltage.Text = ChannelParamenters[1].PositionUpperVoltage.ToString();


                }
                else if (ChannelParamenters[1].TConfig == DpsDevice.TConfig.Vibration)
                {
                    Txtb_Channel2_Type.Text = "壳体振动";
                    Channel2_Universal.Visibility = Visibility.Visible;
                    Channel2_Position.Visibility = Visibility.Hidden;
                    Channel2_Vibration.Visibility = Visibility.Visible;
                    Channel2_Tacho.Visibility = Visibility.Hidden;
                    Channel2_Type.SelectedItem = Channel2_Vibration_Choose;
                    Channel2_DampingTimeMs.Text = DampingTime.ToString();
                    Channel2_SensorSensitivity.Text = ChannelParamenters[1].SensorSensitivity.ToString("F3");
                    Channel2_SensorUnit_Information();
                    Channel2_OutputFullScale.Text = ChannelParamenters[1].OutputFullScale.ToString("F3");
                    Channel2_OutputUnit_Information();
                    Channel2_OutputType_Information();
                    float noiseGate2;
                    noiseGate2 = ChannelParamenters[1].NoiseGate * ChannelParamenters[1].OutputFullScale;
                    Channel2_NoiseGate.Text = noiseGate2.ToString("F3");
                    Channel2_OutputGain.Text = ChannelParamenters[1].OutputGain.ToString("F3");
                    Channel2_LfCutoffFreq.Text = ChannelParamenters[1].LfCutoffFreq.ToString("F3");
                    Channel2_HfCutoffFreq.Text = ChannelParamenters[1].HfCutoffFreq.ToString("F3");
                    Channel2_RslTimeMs.Text = RslTime.ToString("F3");


                }
                else if (ChannelParamenters[1].TConfig == DpsDevice.TConfig.Tacho)
                {
                    float tacho2;
                    Txtb_Channel2_Type.Text = "转速/键相位";
                    Channel2_Universal.Visibility = Visibility.Visible;
                    Channel2_Position.Visibility = Visibility.Hidden;
                    Channel2_Vibration.Visibility = Visibility.Hidden;
                    Channel2_Tacho.Visibility = Visibility.Visible;
                    Channel2_Type.SelectedItem = Channel2_Tacho_Choose;
                    Channel2_TachoRange.Text = ChannelParamenters[1].TachoRange.ToString("F3");
                    tacho2 = 60 / ChannelParamenters[1].TachoNormalizer;
                    Channel2_TachoNormalizer.Text = tacho2.ToString("F3");
                    Channel2_TachoTriggerThreshold.Text = ChannelParamenters[1].TachoTriggerThreshold.ToString("F3");
                    Channel2_DampingTimeMs.Text = DampingTime.ToString();

                }



            }
            catch (Exception)
            {
                //MessageBox.Show(e.ToString(), "ERROR");

            }

        }
        private void Channel2_SensorUnit_Information()
        {
            if (ChannelParamenters[1].SensorUnit == DpsDevice.SensorUnit.MvPerG)
            {
                Channel2_SensorUnit.SelectedItem = Channel2_SensorUnit_MvPerG;
            }
            else if (ChannelParamenters[1].SensorUnit == DpsDevice.SensorUnit.MvPerIps)
            {
                Channel2_SensorUnit.SelectedItem = Channel2_SensorUnit_MvPerIps;
            }
            else if (ChannelParamenters[1].SensorUnit == DpsDevice.SensorUnit.MvPerMmps)
            {
                Channel2_SensorUnit.SelectedItem = Channel2_SensorUnit_MvPerMmps;
            }
            else if (ChannelParamenters[1].SensorUnit == DpsDevice.SensorUnit.VoltPerInch)
            {
                Channel2_SensorUnit.SelectedItem = Channel2_SensorUnit_VoltPerInch;
            }
            else if (ChannelParamenters[1].SensorUnit == DpsDevice.SensorUnit.VoltPerMm)
            {
                Channel2_SensorUnit.SelectedItem = Channel2_SensorUnit_VoltPerMm;
            }
        }
        private void Channel2_OutputUnit_Information()
        {
            if (ChannelParamenters[1].OutputUnit == DpsDevice.OutputUnit.G)
            {
                Channel2_OutputUnit.SelectedItem = Channel2_OutputUnit_G;
            }
            else if (ChannelParamenters[1].OutputUnit == DpsDevice.OutputUnit.Ips)
            {
                Channel2_OutputUnit.SelectedItem = Channel2_OutputUnit_Ips;
            }
            else if (ChannelParamenters[1].OutputUnit == DpsDevice.OutputUnit.Mil)
            {
                Channel2_OutputUnit.SelectedItem = Channel2_OutputUnit_Mil;
            }
            else if (ChannelParamenters[1].OutputUnit == DpsDevice.OutputUnit.Mm)
            {
                Channel2_OutputUnit.SelectedItem = Channel2_OutputUnit_Mm;
            }
            else if (ChannelParamenters[1].OutputUnit == DpsDevice.OutputUnit.Mmps)
            {
                Channel2_OutputUnit.SelectedItem = Channel2_OutputUnit_Mmps;
            }
            else if (ChannelParamenters[1].OutputUnit == DpsDevice.OutputUnit.Mps2)
            {
                Channel2_OutputUnit.SelectedItem = Channel2_OutputUnit_Mps2;
            }
            else if (ChannelParamenters[1].OutputUnit == DpsDevice.OutputUnit.Um)
            {
                Channel2_OutputUnit.SelectedItem = Channel2_OutputUnit_Um;
            }

        }
        private void Channel2_OutputType_Information()
        {
            if (ChannelParamenters[1].OutputType == DpsDevice.OutputType.Peak)
            {
                Channel2_OutputType.SelectedItem = Channel2_OutputType_Peak;
            }
            else if (ChannelParamenters[1].OutputType == DpsDevice.OutputType.Pkpk)
            {
                Channel2_OutputType.SelectedItem = Channel2_OutputType_Pkpk;
            }
            else if (ChannelParamenters[1].OutputType == DpsDevice.OutputType.Rms)
            {
                Channel2_OutputType.SelectedItem = Channel2_OutputType_Rms;
            }
        }
        private async void Channel2_SendTypeInformation()
        {
            try
            {

                if (Channel2_Type.SelectedItem == Channel2_Position_Choose)
                {
                    ChannelParamenters[1].TConfig = DpsDevice.TConfig.Position;
                    //ChannelParamenters[1] = await device.SetChannelParameters(1, ChannelParamenters[1]);
                }
                else if (Channel2_Type.SelectedItem == Channel2_Vibration_Choose)
                {
                    ChannelParamenters[1].TConfig = DpsDevice.TConfig.Vibration;
                    //ChannelParamenters[1] = await device.SetChannelParameters(1, ChannelParamenters[1]);

                }
                else if (Channel2_Type.SelectedItem == Channel2_Tacho_Choose)
                {
                    ChannelParamenters[1].TConfig = DpsDevice.TConfig.Tacho;
                    //ChannelParamenters[1] = await device.SetChannelParameters(1, ChannelParamenters[1]);

                }
                ChannelParamenters[1] = await device.SetChannelParameters(1, ChannelParamenters[1]);


            }
            catch (Exception)
            {
                //MessageBox.Show(e.ToString(), "ERROR");
            }
        }


        private void DeviceOnDisConnectedThread()
        {
            Dispatcher.Invoke(new Action(DeviceOnDisConnected));
        }

        private  void DeviceOnDisConnected()
        {
            Usb_State_On.Visibility = Visibility.Hidden;
            Usb_State_Off.Visibility = Visibility.Visible;
            Relay1_Gray.Visibility = Visibility.Visible;
            Relay1_Green.Visibility = Visibility.Hidden;
            Relay1_Red.Visibility = Visibility.Hidden;
            Relay2_Gray.Visibility = Visibility.Visible;
            Relay2_Green.Visibility = Visibility.Hidden;
            Relay2_Red.Visibility = Visibility.Hidden;
            Channel_Paramenters.IsEnabled = false;
            Restart.IsEnabled = true;
            Channel_CalibrationADC_DAC.IsEnabled = false;
            Channel_Relay.IsEnabled = false;
            Product_information.IsEnabled = false;
            Output_Information.IsEnabled = false;
            Paramenters.IsEnabled = false;
            Activation.Close();
        }
        bool Relay_State_1;
        bool Relay_State_2;
        float Measurement_Voltage1;
        float Measurement_Voltage2;
        private void DeviceMeasurement(MeasurementReport measurement)
        {
            try
            {
            Dispatcher.Invoke(() =>
            {
                
                Channel1_Current.Content = measurement.Current[0].ToString("F3");
                Channel1_Voltage.Content = measurement.Voltage[0].ToString("F3");
                Measurement_Voltage1 = measurement.Voltage[0];
                Channel1_Result.Content = measurement.Output[0].ToString("F3");
                Channel2_Current.Content = measurement.Current[1].ToString("F3");
                Channel2_Voltage.Content = measurement.Voltage[1].ToString("F3");
                Measurement_Voltage2 = measurement.Voltage[1];
                Channel2_Result.Content = measurement.Output[1].ToString("F3");
                CPU_Usage.Content = measurement.CpuUsage.ToString("F1");
                Channel1_ThreshRef.Content = measurement.ThreshRef[0].ToString("F3");
                Channel2_ThreshRef.Content = measurement.ThreshRef[1].ToString("F3");
                Relay_State_1 = measurement.RelayState[0];
                Relay_State_2= measurement.RelayState[1];
                Relay1_State();
                Relay2_State();
            });
            }
            catch(Exception)
            {
                //MessageBox.Show(e.ToString(), "ERROR");

            }
        }
        private void Relay1_State()
        {
            if (!Relay_State_1)
            {
                Relay1_Green.Visibility = Visibility.Visible;
                Relay1_Red.Visibility = Visibility.Hidden;
                Relay1_Gray.Visibility = Visibility.Hidden;
            }
            else
            {
                Relay1_Red.Visibility = Visibility.Visible;
                Relay1_Gray.Visibility = Visibility.Hidden;
                Relay1_Green.Visibility = Visibility.Hidden;
            }
        }
        private void Relay2_State()
        { 
            if(!Relay_State_2)
            {
                Relay2_Green.Visibility = Visibility.Visible;
                Relay2_Red.Visibility = Visibility.Hidden;
                Relay2_Gray.Visibility = Visibility.Hidden;
            }
            else
            {
                Relay2_Red.Visibility = Visibility.Visible;
                Relay2_Gray.Visibility = Visibility.Hidden;
                Relay2_Green.Visibility = Visibility.Hidden;
            }

        }
        private void Relay1_Channel_Config()
        {
            try
            {
                    if (RelayConfig[0].ChannelSelect == 0)
                    {
                        Relay1_Channel.SelectedItem = Relay_1_Channel1;
                        //Ch1_Threshold_Unitstring.Text = ChannelParamenters[0].OutputUnitStr;
                        //Ch1_Thresholdother_Unitstring.Text = ChannelParamenters[0].OutputUnitStr;
                    }
                    else if (RelayConfig[0].ChannelSelect == 1)
                    {
                        Relay1_Channel.SelectedItem = Relay_1_Channel2;
                        //Ch1_Threshold_Unitstring.Text = ChannelParamenters[1].OutputUnitStr;
                        //Ch1_Thresholdother_Unitstring.Text = ChannelParamenters[1].OutputUnitStr;
                    }
                Relay_1_Information();


            }
            catch (Exception )
            {
                //MessageBox.Show(e.ToString(), "ERROR");

            }
        }
        private void Relay2_Channel_Config()
        {
            try
            {
                if (RelayConfig[1].ChannelSelect == 0)
                {
                    Relay2_Channel.SelectedItem = Relay_2_Channel1;
                    //Ch2_Threshold_Unitstring.Text = ChannelParamenters[0].OutputUnitStr;
                    //Ch2_Thresholdother_Unitstring.Text = ChannelParamenters[0].OutputUnitStr;

                }
                else if (RelayConfig[1].ChannelSelect == 1)
                {
                    Relay2_Channel.SelectedItem = Relay_2_Channel2;
                    //Ch2_Threshold_Unitstring.Text = ChannelParamenters[1].OutputUnitStr;
                    //Ch2_Thresholdother_Unitstring.Text = ChannelParamenters[1].OutputUnitStr;

                }
                Relay_2_Information();

            }
            catch (Exception)
            {
                //MessageBox.Show(e.ToString(), "ERROR");

            }
        }
        private void Relay1_LogicOperation_Information()
        {
            try
            {
                    if (RelayConfig[0].RelayLogicOperation == DpsDevice.RelayLogicOperation.And)
                    {
                        Relay1_RelayLogicOperation.SelectedItem = Relay1_Channel_RelayLogicOperation_And;
                    }
                    else if (RelayConfig[0].RelayLogicOperation == DpsDevice.RelayLogicOperation.None)
                    {
                        Relay1_RelayLogicOperation.SelectedItem = Relay1_Channel_RelayLogicOperation_None;
                    }
                    else if (RelayConfig[0].RelayLogicOperation == DpsDevice.RelayLogicOperation.Or)
                    {
                        Relay1_RelayLogicOperation.SelectedItem = Relay1_Channel_RelayLogicOperation_Or;
                    }

            }
            catch (Exception)
            {
                //MessageBox.Show(e.ToString(), "ERROR");

            }
        }
        private void Relay2_LogicOperation_Information()
        {
            try
            {
                if (RelayConfig[1].RelayLogicOperation == DpsDevice.RelayLogicOperation.And)
                {
                    Relay2_RelayLogicOperation.SelectedItem = Relay2_Channel_RelayLogicOperation_And;
                }
                else if (RelayConfig[1].RelayLogicOperation == DpsDevice.RelayLogicOperation.None)
                {
                    Relay2_RelayLogicOperation.SelectedItem = Relay2_Channel_RelayLogicOperation_None;
                }
                else if (RelayConfig[1].RelayLogicOperation == DpsDevice.RelayLogicOperation.Or)
                {
                    Relay2_RelayLogicOperation.SelectedItem = Relay2_Channel_RelayLogicOperation_Or;
                }

            }
            catch (Exception)
            {
                //MessageBox.Show(e.ToString(), "ERROR");

            }
        }
        private void Relay1_SelfLatching_Information()
        {
            if (RelayConfig[0].SelfLatching)
            {
                Relay1_SelfLatching.IsChecked = true;
            }
            else
            { Relay1_SelfLatching.IsChecked = false; }
            

        }
        private void Relay2_SelfLatching_Information()
        {
            if (RelayConfig[1].SelfLatching)
            {
                Relay2_SelfLatching.IsChecked = true;
            }
            else
            {
                Relay2_SelfLatching.IsChecked = false;
            }

        }
        private async void Auto_MfgDate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Product_MfgDate.Text = String.Format("{0:d}", DateTime.Now);
                Information.MfgDate = DateTime.Now;
                //Product_MfgDate.Text = "4/20/2018";
                await device.SetMfgDate(Information.MfgDate);

            }
            catch (Exception )
            {
                //MessageBox.Show(ex.ToString(), "ERROR");

            }
        }

        private void Product_MfgSn_GotFocus(object sender, RoutedEventArgs e)
        {
            Product_MfgSn.Background = Brushes.White;
        }

        private async void Product_MfgSn_LostFocus(object sender, RoutedEventArgs e)
        {
            Product_MfgSn.Background = null;
            Information.MfgSn= Product_MfgSn.Text;
            await device.SetMfgSn(Information.MfgSn);
        }

        private void Channel1_Checked(object sender, RoutedEventArgs e)
        {
            Channel1.Background = Brushes.LightSkyBlue;
            Channel2.Background = null;
            Channel_1.Background = Brushes.LightSkyBlue;
            Channel_2.Background = null;
            //Product_Information();
            Interface_Channel1_Information();
        }

        private void Channel2_Checked(object sender, RoutedEventArgs e)
        {
            Channel2.Background = Brushes.LightSkyBlue;
            Channel1.Background = null;
            Channel_2.Background = Brushes.LightSkyBlue;
            Channel_1.Background = null;
            //Product_Information();
            Interface_Channel2_Information();
        }

        private void Channel1_Type_LostFocus(object sender, RoutedEventArgs e)
        {
            Channel1_SendTypeInformation();
            Interface_Channel1_Information();

        }
        private void Channel2_Type_LostFocus(object sender, RoutedEventArgs e)
        {
            Channel2_SendTypeInformation();
            Interface_Channel2_Information();

        }

        private async void Channel1_DampingTimeMs_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                    float dampingTimeMs1;
                    dampingTimeMs1 = Convert.ToSingle(Channel1_DampingTimeMs.Text) * 1000;

                    if ((dampingTimeMs1 - UInt16.MinValue) <= 0)
                    {
                        ChannelParamenters[0].DampingTimeMs = 0;
                    }
                    else if ((dampingTimeMs1 - UInt16.MaxValue) >= 0)
                    {
                        ChannelParamenters[0].DampingTimeMs = 60000;
                    }
                    else
                    {
                        ChannelParamenters[0].DampingTimeMs = (ushort)dampingTimeMs1;
                    }
                    ChannelParamenters[0] = await device.SetChannelParameters(0, ChannelParamenters[0]);
                    Interface_Channel1_Information();

            }
            catch (Exception)
            {
                MessageBox.Show("输入错误，请重新输入！", "ERROR");
            }
        }

        private async void Channel2_DampingTimeMs_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                float dampingTimeMs2;

                    dampingTimeMs2 = Convert.ToSingle(Channel2_DampingTimeMs.Text) * 1000;
                    if ((dampingTimeMs2 - UInt16.MinValue) <= 0)
                    {
                        ChannelParamenters[1].DampingTimeMs = 0;
                    }
                    else if ((dampingTimeMs2 - UInt16.MaxValue) >= 0)
                    {
                        ChannelParamenters[1].DampingTimeMs = 60000;
                    }
                    else
                    {
                        ChannelParamenters[1].DampingTimeMs = (ushort)dampingTimeMs2;
                    }
                    ChannelParamenters[1] = await device.SetChannelParameters(1, ChannelParamenters[1]);
                    Interface_Channel2_Information();

                
            }
            catch (Exception )
            {
                MessageBox.Show("输入错误，请重新输入！", "ERROR");

            }

        }
        /*Position*/
        private async void Channel1_PositionLowerVoltage_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                ChannelParamenters[0].PositionLowerVoltage = Convert.ToSingle(Channel1_PositionLowerVoltage.Text);
                ChannelParamenters[0] = await device.SetChannelParameters(0, ChannelParamenters[0]);
                Interface_Channel1_Information();

            }
            catch (Exception)
            {
                MessageBox.Show("输入错误，请重新输入！", "ERROR");
            }

        }

        private async void Channel1_PositionUpperVoltage_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                ChannelParamenters[0].PositionUpperVoltage = Convert.ToSingle(Channel1_PositionUpperVoltage.Text);
                ChannelParamenters[0] = await device.SetChannelParameters(0, ChannelParamenters[0]);
                Interface_Channel1_Information();
            }
            catch (Exception)
            {
                MessageBox.Show("输入错误，请重新输入！", "ERROR");
            }
        
        }
        private async void Channel2_PositionLowerVoltage_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                ChannelParamenters[1].PositionLowerVoltage = Convert.ToSingle(Channel2_PositionLowerVoltage.Text);
                ChannelParamenters[1] = await device.SetChannelParameters(1, ChannelParamenters[1]);
                Interface_Channel2_Information();
            }
            catch(Exception)
            {
                MessageBox.Show("输入错误，请重新输入！", "ERROR");
            }

        }

        private async void Channel2_PositionUpperVoltage_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                ChannelParamenters[1].PositionUpperVoltage = Convert.ToSingle(Channel2_PositionUpperVoltage.Text);
                ChannelParamenters[1] = await device.SetChannelParameters(1, ChannelParamenters[1]);
                Interface_Channel2_Information();
            }
            catch(Exception)
            {
                MessageBox.Show("输入错误，请重新输入！", "ERROR");
            }
        }
        /*Vibration*/

        private async void Channel1_SensorSensitivity_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                ChannelParamenters[0].SensorSensitivity = Convert.ToSingle(Channel1_SensorSensitivity.Text);
                ChannelParamenters[0] = await device.SetChannelParameters(0, ChannelParamenters[0]);
                Interface_Channel1_Information();
            }
            catch (Exception)
            {
                MessageBox.Show("输入错误，请重新输入！", "ERROR");
            }
        }

        private async void Channel1_OutputFullScale_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                ChannelParamenters[0].OutputFullScale = Convert.ToSingle(Channel1_OutputFullScale.Text);
                ChannelParamenters[0] = await device.SetChannelParameters(0, ChannelParamenters[0]);
                Interface_Channel1_Information();
            }
            catch (Exception)
            {
                MessageBox.Show("输入错误，请重新输入！", "ERROR");
            }


        }
        private async void Channel1_SendSensorUnitInformation()
        {
            try
            {

                if (Channel1_SensorUnit.SelectedItem == Channel1_SensorUnit_MvPerG)
                {
                    ChannelParamenters[0].SensorUnit = DpsDevice.SensorUnit.MvPerG;
                    //ChannelParamenters[0] = await device.SetChannelParameters(0, ChannelParamenters[0]);
                }
                else if (Channel1_SensorUnit.SelectedItem == Channel1_SensorUnit_MvPerIps)
                {
                    ChannelParamenters[0].SensorUnit = DpsDevice.SensorUnit.MvPerIps;
                    //ChannelParamenters[0] = await device.SetChannelParameters(0, ChannelParamenters[0]);

                }
                else if (Channel1_SensorUnit.SelectedItem == Channel1_SensorUnit_MvPerMmps)
                {
                    ChannelParamenters[0].SensorUnit = DpsDevice.SensorUnit.MvPerMmps;
                    //ChannelParamenters[0] = await device.SetChannelParameters(0, ChannelParamenters[0]);
                }
                 else if (Channel1_SensorUnit.SelectedItem == Channel1_SensorUnit_VoltPerInch)
                {
                    ChannelParamenters[0].SensorUnit = DpsDevice.SensorUnit.VoltPerInch;
                    //ChannelParamenters[0] = await device.SetChannelParameters(0, ChannelParamenters[0]);
                }
                else if (Channel1_SensorUnit.SelectedItem == Channel1_SensorUnit_VoltPerMm)
                {
                    ChannelParamenters[0].SensorUnit = DpsDevice.SensorUnit.VoltPerMm;
                    //ChannelParamenters[0] = await device.SetChannelParameters(0, ChannelParamenters[0]);
                }
                ChannelParamenters[0] = await device.SetChannelParameters(0, ChannelParamenters[0]);



            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "ERROR");
            }
        }

        private void Channel1_SensorUnit_LostFocus(object sender, RoutedEventArgs e)
        {
            Channel1_SendSensorUnitInformation();
            Interface_Channel1_Information();

        }
        private async void Channel1_SendOutputUnitInformation()
        {
            try
            {
                if (Channel1_OutputUnit.SelectedItem == Channel1_OutputUnit_G)
                {
                    ChannelParamenters[0].OutputUnit = DpsDevice.OutputUnit.G;
                }

                else if (Channel1_OutputUnit.SelectedItem == Channel1_OutputUnit_Ips)
                {
                    ChannelParamenters[0].OutputUnit = DpsDevice.OutputUnit.Ips;
                }
                else if (Channel1_OutputUnit.SelectedItem == Channel1_OutputUnit_Mmps)
                {
                    ChannelParamenters[0].OutputUnit = DpsDevice.OutputUnit.Mmps;
                }
                else if (Channel1_OutputUnit.SelectedItem == Channel1_OutputUnit_Mps2)
                {
                    ChannelParamenters[0].OutputUnit = DpsDevice.OutputUnit.Mps2;
                }

                else if (Channel1_OutputUnit.SelectedItem == Channel1_OutputUnit_Mil)
                {
                    ChannelParamenters[0].OutputUnit = DpsDevice.OutputUnit.Mil;
                }
                else if (Channel1_OutputUnit.SelectedItem == Channel1_OutputUnit_Mm)
                {
                    ChannelParamenters[0].OutputUnit = DpsDevice.OutputUnit.Mm;
                }
                else if (Channel1_OutputUnit.SelectedItem == Channel1_OutputUnit_Um)
                {
                    ChannelParamenters[0].OutputUnit = DpsDevice.OutputUnit.Um;
                }
                ChannelParamenters[0] = await device.SetChannelParameters(0, ChannelParamenters[0]);



            }
            catch (Exception )
            {
                //MessageBox.Show(e.ToString(), "ERROR");
            }
        }

        private void Channel1_OutputUnit_LostFocus(object sender, RoutedEventArgs e)
        {
            Channel1_SendOutputUnitInformation();
            Interface_Channel1_Information();

        }
        private async void Channel1_SendOutputTypeInformation()
        {
            try
            {

                if (Channel1_OutputType.SelectedItem == Channel1_OutputType_Peak)
                {
                    ChannelParamenters[0].OutputType = DpsDevice.OutputType.Peak;
                    ChannelParamenters[0] = await device.SetChannelParameters(0, ChannelParamenters[0]);
                }
                else if (Channel1_OutputType.SelectedItem == Channel1_OutputType_Pkpk)
                {
                    ChannelParamenters[0].OutputType = DpsDevice.OutputType.Pkpk;
                    ChannelParamenters[0] = await device.SetChannelParameters(0, ChannelParamenters[0]);
                }
                else if (Channel1_OutputType.SelectedItem == Channel1_OutputType_Rms)
                {
                    ChannelParamenters[0].OutputType = DpsDevice.OutputType.Rms;
                    ChannelParamenters[0] = await device.SetChannelParameters(0, ChannelParamenters[0]);
                }
            }
            catch (Exception)
            {
                //MessageBox.Show(e.ToString(), "ERROR");
            }
        }

        private void Channel1_OutputType_LostFocus(object sender, RoutedEventArgs e)
        {
            Channel1_SendOutputTypeInformation();
            Interface_Channel1_Information();
        }

        private async void Channel1_NoiseGate_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                float noisegate;
                noisegate = Convert.ToSingle(Channel1_NoiseGate.Text);
                ChannelParamenters[0].NoiseGate = noisegate/Convert.ToSingle(Channel1_OutputFullScale.Text);
                ChannelParamenters[0] = await device.SetChannelParameters(0, ChannelParamenters[0]);
                Interface_Channel1_Information();
            }
            catch (Exception)
            {
                MessageBox.Show("输入错误，请重新输入！", "ERROR");
            }

        }

        private async void Channel1_OutputGain_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                ChannelParamenters[0].OutputGain = Convert.ToSingle(Channel1_OutputGain.Text);
                ChannelParamenters[0] = await device.SetChannelParameters(0, ChannelParamenters[0]);
                Interface_Channel1_Information();
            }
            catch (Exception)
            {
                MessageBox.Show("输入错误，请重新输入！", "ERROR");
            }


        }

        private async void Channel1_LfCutoffFreq_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                ChannelParamenters[0].LfCutoffFreq = Convert.ToSingle(Channel1_LfCutoffFreq.Text);
                ChannelParamenters[0] = await device.SetChannelParameters(0, ChannelParamenters[0]);
                Interface_Channel1_Information();
            }
            catch (Exception)
            {
                MessageBox.Show("输入错误，请重新输入！", "ERROR");
            }

        }

        private async void Channel1_HfCutoffFreq_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                ChannelParamenters[0].HfCutoffFreq = Convert.ToSingle(Channel1_HfCutoffFreq.Text);
                ChannelParamenters[0] = await device.SetChannelParameters(0, ChannelParamenters[0]);
                Interface_Channel1_Information();
            }
            catch (Exception)
            {
                MessageBox.Show("输入错误，请重新输入！", "ERROR");
            }

        }

        private async void Channel1_RslTimeMs_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                float rslTimeMs1;
                rslTimeMs1 = Convert.ToSingle(Channel1_RslTimeMs.Text) * 1000;
                if ((rslTimeMs1 - UInt16.MinValue) <= 0)
                {
                    ChannelParamenters[0].RslTimeMs = 0;
                }
                else if ((rslTimeMs1 - UInt16.MaxValue) >= 0)
                {
                    ChannelParamenters[0].RslTimeMs = 60000;
                }
                else
                {
                    ChannelParamenters[0].RslTimeMs = (ushort)rslTimeMs1;
                }
                ChannelParamenters[0] = await device.SetChannelParameters(0, ChannelParamenters[0]);
                Interface_Channel1_Information();
                
            }
            catch (Exception )
            {
                MessageBox.Show("输入错误，请重新输入！", "ERROR");

            }

        }

        private async void Channel2_SensorSensitivity_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                ChannelParamenters[1].SensorSensitivity = Convert.ToSingle(Channel2_SensorSensitivity.Text);
                ChannelParamenters[1] = await device.SetChannelParameters(1, ChannelParamenters[1]);
                Interface_Channel2_Information();
            }
            catch (Exception)
            {
                MessageBox.Show("输入错误，请重新输入！", "ERROR");
            }


        }

        private async void Channel2_OutputFullScale_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                ChannelParamenters[1].OutputFullScale = Convert.ToSingle(Channel2_OutputFullScale.Text);
                ChannelParamenters[1] = await device.SetChannelParameters(1, ChannelParamenters[1]);
                Interface_Channel2_Information();
            }
            catch (Exception)
            {
                MessageBox.Show("输入错误，请重新输入！", "ERROR");
            }


        }
        private async void Channel2_SendSensorUnitInformation()
        {
            try
            {

                if (Channel2_SensorUnit.SelectedItem == Channel2_SensorUnit_MvPerG)
                {
                    ChannelParamenters[1].SensorUnit = DpsDevice.SensorUnit.MvPerG;
                }
                else if (Channel2_SensorUnit.SelectedItem == Channel2_SensorUnit_MvPerIps)
                {
                    ChannelParamenters[1].SensorUnit = DpsDevice.SensorUnit.MvPerIps;

                }
                else if (Channel2_SensorUnit.SelectedItem == Channel2_SensorUnit_MvPerMmps)
                {
                    ChannelParamenters[1].SensorUnit = DpsDevice.SensorUnit.MvPerMmps;
                }
                if (Channel2_SensorUnit.SelectedItem == Channel2_SensorUnit_VoltPerInch)
                {
                    ChannelParamenters[1].SensorUnit = DpsDevice.SensorUnit.VoltPerInch;
                }
                else if (Channel2_SensorUnit.SelectedItem == Channel2_SensorUnit_VoltPerMm)
                {
                    ChannelParamenters[1].SensorUnit = DpsDevice.SensorUnit.VoltPerMm;
                }
                ChannelParamenters[1] = await device.SetChannelParameters(1, ChannelParamenters[1]);

            }
            catch (Exception)
            {
                //MessageBox.Show(e.ToString(), "ERROR");
            }
        }

        private void Channel2_SensorUnit_LostFocus(object sender, RoutedEventArgs e)
        {
            Channel2_SendSensorUnitInformation();
            Interface_Channel2_Information();

        }
        private async void Channel2_SendOutputUnitInformation()
        {
            try
            {
                if (Channel2_OutputUnit.SelectedItem == Channel2_OutputUnit_G)
                {
                    ChannelParamenters[1].OutputUnit = DpsDevice.OutputUnit.G;
                    ChannelParamenters[1] = await device.SetChannelParameters(1, ChannelParamenters[1]);
                }

                else if (Channel2_OutputUnit.SelectedItem == Channel2_OutputUnit_Ips)
                {
                    ChannelParamenters[1].OutputUnit = DpsDevice.OutputUnit.Ips;
                    ChannelParamenters[1] = await device.SetChannelParameters(1, ChannelParamenters[1]);
                }
                else if (Channel2_OutputUnit.SelectedItem == Channel2_OutputUnit_Mmps)
                {
                    ChannelParamenters[1].OutputUnit = DpsDevice.OutputUnit.Mmps;
                    ChannelParamenters[1] = await device.SetChannelParameters(1, ChannelParamenters[1]);
                }
                else if (Channel2_OutputUnit.SelectedItem == Channel2_OutputUnit_Mps2)
                {
                    ChannelParamenters[1].OutputUnit = DpsDevice.OutputUnit.Mps2;
                    ChannelParamenters[1] = await device.SetChannelParameters(1, ChannelParamenters[1]);
                }
                else if (Channel2_OutputUnit.SelectedItem == Channel2_OutputUnit_Mil)
                {
                    ChannelParamenters[1].OutputUnit = DpsDevice.OutputUnit.Mil;
                    ChannelParamenters[1] = await device.SetChannelParameters(1, ChannelParamenters[1]);
                }
                else if (Channel2_OutputUnit.SelectedItem == Channel2_OutputUnit_Mm)
                {
                    ChannelParamenters[1].OutputUnit = DpsDevice.OutputUnit.Mm;
                    ChannelParamenters[1] = await device.SetChannelParameters(1, ChannelParamenters[1]);
                }
                else if (Channel2_OutputUnit.SelectedItem == Channel2_OutputUnit_Um)
                {
                    ChannelParamenters[1].OutputUnit = DpsDevice.OutputUnit.Um;
                    ChannelParamenters[1] = await device.SetChannelParameters(1, ChannelParamenters[1]);
                }

                //Interface_Channel2_Information();



            }
            catch (Exception )
            {
                //MessageBox.Show(e.ToString(), "ERROR");
            }
        }

        private void Channel2_OutputUnit_LostFocus(object sender, RoutedEventArgs e)
        {
            Channel2_SendOutputUnitInformation();
            Interface_Channel2_Information();

        }
        private async void Channel2_SendOutputTypeInformation()
        {
            try
            {

                if (Channel2_OutputType.SelectedItem == Channel2_OutputType_Peak)
                {
                    ChannelParamenters[1].OutputType = DpsDevice.OutputType.Peak;
                }
                else if (Channel2_OutputType.SelectedItem == Channel2_OutputType_Pkpk)
                {
                    ChannelParamenters[1].OutputType = DpsDevice.OutputType.Pkpk;
                }
                else if (Channel2_OutputType.SelectedItem == Channel2_OutputType_Rms)
                {
                    ChannelParamenters[1].OutputType = DpsDevice.OutputType.Rms;
                }
                ChannelParamenters[1] = await device.SetChannelParameters(1, ChannelParamenters[1]);

            }
            catch (Exception)
            {
                //MessageBox.Show(e.ToString(), "ERROR");
            }
        }

        private void Channel2_OutputType_LostFocus(object sender, RoutedEventArgs e)
        {
            Channel2_SendOutputTypeInformation();
            Interface_Channel2_Information();

        }

        private async void Channel2_NoiseGate_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                float noisegate;
                noisegate = Convert.ToSingle(Channel2_NoiseGate.Text);
                ChannelParamenters[1].NoiseGate = noisegate / Convert.ToSingle(Channel2_OutputFullScale.Text);
                ChannelParamenters[1] = await device.SetChannelParameters(1, ChannelParamenters[1]);
                Interface_Channel2_Information();
            }
            catch (Exception)
            {
                MessageBox.Show("输入错误，请重新输入！", "ERROR");
            }


        }

        private async void Channel2_OutputGain_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                ChannelParamenters[1].OutputGain = Convert.ToSingle(Channel2_OutputGain.Text);
                ChannelParamenters[1] = await device.SetChannelParameters(1, ChannelParamenters[1]);
                Interface_Channel2_Information();
            }
            catch (Exception)
            {
                MessageBox.Show("输入错误，请重新输入！", "ERROR");
            }


        }

        private async void Channel2_LfCutoffFreq_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                ChannelParamenters[1].LfCutoffFreq = Convert.ToSingle(Channel2_LfCutoffFreq.Text);
                ChannelParamenters[1] = await device.SetChannelParameters(1, ChannelParamenters[1]);
                Interface_Channel2_Information();
            }
            catch (Exception)
            {
                MessageBox.Show("输入错误，请重新输入！", "ERROR");
            }


        }

        private async void Channel2_HfCutoffFreq_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                ChannelParamenters[1].HfCutoffFreq = Convert.ToSingle(Channel2_HfCutoffFreq.Text);
                ChannelParamenters[1] = await device.SetChannelParameters(1, ChannelParamenters[1]);
                Interface_Channel2_Information();
            }
            catch (Exception)
            {
                MessageBox.Show("输入错误，请重新输入！", "ERROR");
            }


        }

        private async void Channel2_RslTimeMs_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                float rslTimeMs2;
                rslTimeMs2 = Convert.ToSingle(Channel2_RslTimeMs.Text) * 1000;
                if ((rslTimeMs2 - UInt16.MinValue) <= 0)
                {
                    ChannelParamenters[1].RslTimeMs = 0;
                }
                else if ((rslTimeMs2 - UInt16.MaxValue) >= 0)
                {
                    ChannelParamenters[1].RslTimeMs = 60000;
                }
                else
                {
                    ChannelParamenters[1].RslTimeMs = (ushort)rslTimeMs2;
                }
                ChannelParamenters[1] = await device.SetChannelParameters(1, ChannelParamenters[1]);
                Interface_Channel2_Information();

            }
            catch (Exception )
            {
                MessageBox.Show("输入错误，请重新输入！", "ERROR");

            }

        }
        /*Tacho*/
        private async void Channel1_TachoRange_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                ChannelParamenters[0].TachoRange = Convert.ToSingle(Channel1_TachoRange.Text);
                ChannelParamenters[0] = await device.SetChannelParameters(0, ChannelParamenters[0]);
                Interface_Channel1_Information();
            }
            catch (Exception)
            {
                MessageBox.Show("输入错误，请重新输入！", "ERROR");
            }


        }

        private async void Channel1_TachoNormalizer_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                float tacho1;
                tacho1 = Convert.ToSingle(Channel1_TachoNormalizer.Text);
                ChannelParamenters[0].TachoNormalizer = Convert.ToSingle(60 / tacho1);
                ChannelParamenters[0] = await device.SetChannelParameters(0, ChannelParamenters[0]);
                Interface_Channel1_Information();
            }
            catch (Exception)
            {
                MessageBox.Show("输入错误，请重新输入！", "ERROR");
            }


        }

        private async void Channel1_TachoTriggerThreshold_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                ChannelParamenters[0].TachoTriggerThreshold = Convert.ToSingle(Channel1_TachoTriggerThreshold.Text);
                ChannelParamenters[0] = await device.SetChannelParameters(0, ChannelParamenters[0]);
                Interface_Channel1_Information();
            }
            catch (Exception)
            {
                MessageBox.Show("输入错误，请重新输入！", "ERROR");
            }


        }

        private async void Channel2_TachoRange_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                ChannelParamenters[1].TachoRange = Convert.ToSingle(Channel2_TachoRange.Text);
                ChannelParamenters[1] = await device.SetChannelParameters(1, ChannelParamenters[1]);
                Interface_Channel2_Information();
            }
            catch (Exception)
            {
                MessageBox.Show("输入错误，请重新输入！", "ERROR");
            }


        }

        private async void Channel2_TachoNormalizer_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                float tacho2;
                tacho2= Convert.ToSingle(Channel2_TachoNormalizer.Text);
                ChannelParamenters[1].TachoNormalizer = Convert.ToSingle(60/ tacho2);
                ChannelParamenters[1] = await device.SetChannelParameters(1, ChannelParamenters[1]);
                Interface_Channel2_Information();
            }
            catch (Exception)
            {
                MessageBox.Show("输入错误，请重新输入！", "ERROR");
            }


        }

        private async void Channel2_TachoTriggerThreshold_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                ChannelParamenters[1].TachoTriggerThreshold = Convert.ToSingle(Channel2_TachoTriggerThreshold.Text);
                ChannelParamenters[1] = await device.SetChannelParameters(1, ChannelParamenters[1]);
                Interface_Channel2_Information();
            }
            catch (Exception)
            {
                MessageBox.Show("输入错误，请重新输入！", "ERROR");
            }


        }

        private async void Relay1_Channel_choose()
        {
            try
            {

                if (Relay1_Channel.SelectedItem == Relay_1_Channel1)
                {
                    RelayConfig[0].ChannelSelect = 0;
                    //Ch1_Threshold_Unitstring.Text = ChannelParamenters[0].OutputUnitStr;
                    //Ch1_Thresholdother_Unitstring.Text = ChannelParamenters[0].OutputUnitStr;
                    //RelayConfig[0] = await device.SetRelayConfig(0, RelayConfig[0]);
                    //Interface_Channel1_Information();

                }
                else if (Relay1_Channel.SelectedItem == Relay_1_Channel2)
                {
                    RelayConfig[0].ChannelSelect = 1;
                    //Ch1_Threshold_Unitstring.Text = ChannelParamenters[1].OutputUnitStr;
                    //Ch1_Thresholdother_Unitstring.Text = ChannelParamenters[1].OutputUnitStr;
                    //RelayConfig[0] = await device.SetRelayConfig(0, RelayConfig[0]);
                    //Interface_Channel2_Information();

                }
                RelayConfig[0] = await device.SetRelayConfig(0, RelayConfig[0]);
                //Relay_ALL_Information();
                //Relay_1_Information();
                //Relay1_RelayLogicOperation_Send();
            }
            catch (Exception )
            {
                //MessageBox.Show(e.ToString(), "ERROR");
            }
        }

    
        private void Relay1_Channel_LostFocus(object sender, RoutedEventArgs e)
        {
            Relay1_Channel_choose();
            Relay_ALL_Information();
            ////Relay1_State();
            //Relay1_Channel_Config();
            //Relay1_LogicOperation_Information();
            //Relay1_SelfLatching_Information();


        }
        private async void Relay2_Channel_choose()
        {
            try
            {

                if (Relay2_Channel.SelectedItem == Relay_2_Channel1)
                {
                    RelayConfig[1].ChannelSelect = 0;
                    //Ch2_Threshold_Unitstring.Text = ChannelParamenters[0].OutputUnitStr;
                    //Ch2_Thresholdother_Unitstring.Text = ChannelParamenters[0].OutputUnitStr;
                //Relay_1_Information();

                }
                else if (Relay2_Channel.SelectedItem == Relay_2_Channel2)
                {
                    RelayConfig[1].ChannelSelect = 1;
                    //Ch2_Threshold_Unitstring.Text = ChannelParamenters[1].OutputUnitStr;
                    //Ch2_Thresholdother_Unitstring.Text = ChannelParamenters[1].OutputUnitStr;
                //Relay_2_Information();

                }
                RelayConfig[1] = await device.SetRelayConfig(1, RelayConfig[1]);
                //Relay_ALL_Information();
                //Relay2_RelayLogicOperation_Send();



            }
            catch (Exception )
            {
                //MessageBox.Show(e.ToString(), "ERROR");
            }
        }

        private void Relay2_Channel_LostFocus(object sender, RoutedEventArgs e)
        {
             Relay2_Channel_choose();
            Relay_ALL_Information();

        }

        private async void Relay1_DelayTimeMs_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Relay1_Channel.SelectedItem== Relay_1_Channel1)
                {
                    float delayTimeMs1;
                    delayTimeMs1 = Convert.ToSingle(Relay1_DelayTimeMs.Text) * 1000;
                    if ((delayTimeMs1 - UInt16.MinValue) <= 0)
                    {
                        RelayConfig[0].DelayTimeMs = 0;
                    }
                    else if ((delayTimeMs1 - UInt16.MaxValue) >= 0)
                    {
                        RelayConfig[0].DelayTimeMs = 60000;
                    }
                    else
                    {
                        RelayConfig[0].DelayTimeMs = (ushort)delayTimeMs1;
                    }

                    RelayConfig[0] = await device.SetRelayConfig(0, RelayConfig[0]);
                }
                else if(Relay1_Channel.SelectedItem == Relay_2_Channel1)
                {
                    float delayTimeMs2;
                    delayTimeMs2 = Convert.ToSingle(Relay1_DelayTimeMs.Text) * 1000;
                    if ((delayTimeMs2 - UInt16.MinValue) <= 0)
                    {
                        RelayConfig[1].DelayTimeMs = 0;
                    }
                    else if ((delayTimeMs2 - UInt16.MaxValue) >= 0)
                    {
                        RelayConfig[1].DelayTimeMs = 60000;
                    }
                    else
                    {
                        RelayConfig[1].DelayTimeMs = (ushort)delayTimeMs2;
                    }

                    RelayConfig[1] = await device.SetRelayConfig(1, RelayConfig[1]);
                }
                //Interface_Channel1_Information();
                //Interface_Channel2_Information();
                Relay_ALL_Information();

            }
            catch (Exception )
            {
                MessageBox.Show("输入错误，请重新输入！", "ERROR");

            }

        }

        private async void Relay2_DelayTimeMs_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                float delayTimeMs2;
                delayTimeMs2 = Convert.ToSingle(Relay2_DelayTimeMs.Text) * 1000;
                if ((delayTimeMs2 - UInt16.MinValue) <= 0)
                {
                    RelayConfig[1].DelayTimeMs = 0;
                }
                else if ((delayTimeMs2 - UInt16.MaxValue) >= 0)
                {
                    RelayConfig[1].DelayTimeMs = 60000;
                }
                else
                {
                    RelayConfig[1].DelayTimeMs = (ushort)delayTimeMs2;
                }
                RelayConfig[1] = await device.SetRelayConfig(1, RelayConfig[1]);
                Relay_ALL_Information();

            }
            catch (Exception )
            {
                MessageBox.Show("输入错误，请重新输入！", "ERROR");

            }

        }

        private async void Btn_Save_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MessageBoxResult result = MessageBox.Show("是否保存", "保存", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                if (result == MessageBoxResult.Yes)
                {
                    await device.SaveSettingsToFlash();
                    Connect_Information();
                    MessageBox.Show("保存成功");
                }
                else
                {
                    Connect_Information();
                }

            }
            catch (Exception )
            {
                //MessageBox.Show(ex.ToString(), "ERROR");

            }
        }

        private async void Btn_Cancel_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("是否取消", "取消", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
            {
                await device.DiscardSettings();
                Connect_Information();
                //DeviceOnConnected();
                MessageBox.Show("取消成功");
            }
            else
            {
                Connect_Information();
            }
        }
        private async void Relay1_RelayLogicOperation_Send()
        {
            try
            {

                if (Relay1_RelayLogicOperation.SelectedItem == Relay1_Channel_RelayLogicOperation_And)
                {
                    RelayConfig[0].RelayLogicOperation = DpsDevice.RelayLogicOperation.And;
                }
                else if (Relay1_RelayLogicOperation.SelectedItem == Relay1_Channel_RelayLogicOperation_Or)
                {
                    RelayConfig[0].RelayLogicOperation = DpsDevice.RelayLogicOperation.Or;
                }
                else if (Relay1_RelayLogicOperation.SelectedItem == Relay1_Channel_RelayLogicOperation_None)
                {
                    RelayConfig[0].RelayLogicOperation = DpsDevice.RelayLogicOperation.None;
                }
                RelayConfig[0] = await device.SetRelayConfig(0, RelayConfig[0]);

            }
            catch (Exception)
            {
                //MessageBox.Show(e.ToString(), "ERROR");
            }
        }

        private void Relay1_RelayLogicOperation_LostFocus(object sender, RoutedEventArgs e)
        {
            Relay1_RelayLogicOperation_Send();
            Relay_ALL_Information();

        }
        private async void Relay2_RelayLogicOperation_Send()
        {
            try
            {

                if (Relay2_RelayLogicOperation.SelectedItem == Relay2_Channel_RelayLogicOperation_And)
                {
                    RelayConfig[1].RelayLogicOperation = DpsDevice.RelayLogicOperation.And;
                }
                else if (Relay2_RelayLogicOperation.SelectedItem == Relay2_Channel_RelayLogicOperation_Or)
                {
                    RelayConfig[1].RelayLogicOperation = DpsDevice.RelayLogicOperation.Or;
                }
                else if (Relay2_RelayLogicOperation.SelectedItem == Relay2_Channel_RelayLogicOperation_None)
                {
                    RelayConfig[1].RelayLogicOperation = DpsDevice.RelayLogicOperation.None;
                }
                RelayConfig[1] = await device.SetRelayConfig(1, RelayConfig[1]);


            }
            catch (Exception )
            {
                //MessageBox.Show(e.ToString(), "ERROR");
            }
        }

        private void Relay2_RelayLogicOperation_LostFocus(object sender, RoutedEventArgs e)
        {
            Relay2_RelayLogicOperation_Send();
            Relay_ALL_Information();

        }
        private async void Relay1_Send_SelfLatching()
        {
            if (Relay1_SelfLatching.IsChecked == true)
            {
                RelayConfig[0].SelfLatching = true;
            }
            else if(Relay1_SelfLatching.IsChecked == false)
            {
                RelayConfig[0].SelfLatching = false;
            }
            RelayConfig[0] = await device.SetRelayConfig(0, RelayConfig[0]);


        }
        private void Relay1_SelfLatching_Click(object sender, RoutedEventArgs e)
        {
            Relay1_Send_SelfLatching();
            Relay_ALL_Information();

        }


        private async void Relay2_Send_SelfLatching()
        {
            if (Relay2_SelfLatching.IsChecked == true)
            {
                RelayConfig[1].SelfLatching = true;
            }
            else if (Relay2_SelfLatching.IsChecked == false)
            {
                RelayConfig[1].SelfLatching = false;
            }
            RelayConfig[1] = await device.SetRelayConfig(1, RelayConfig[1]);


        }
        private void Relay2_SelfLatching_Click(object sender, RoutedEventArgs e)
        {
            Relay2_Send_SelfLatching();
            Relay_ALL_Information();

        }

        private async void Restart_Click(object sender, RoutedEventArgs e)
        {
            try
            {
            await device.RestartDevice();
                //Connect_Information();
                DeviceOnConnected();
            }
            catch (Exception )
            {
                //MessageBox.Show(ex.ToString(), "ERROR");
                //throw;
            }
        }

        private void RS485_Information()
        {
            try
            {
                RS485_Id.Text = ModbusConfig.Id.ToString();
                RS485_BaudRate_Information();
                RS485_Parity_Information();
                RS485_NumStopBit_Information();
            }
            catch(Exception e)
            {
                MessageBox.Show(e.ToString(), "ERROR");

            }
        }
        private void RS485_BaudRate_Information()
        {
            if (ModbusConfig.BaudRate == 300)
            {
                RS485_BaudRate.SelectedItem = RS485_BaudRate_300;       
            }
            else if(ModbusConfig.BaudRate == 600)
            {
                RS485_BaudRate.SelectedItem = RS485_BaudRate_600;
            }
            else if (ModbusConfig.BaudRate == 1200)
            {
                RS485_BaudRate.SelectedItem = RS485_BaudRate_1200;
            }
            else if (ModbusConfig.BaudRate == 2400)
            {
                RS485_BaudRate.SelectedItem = RS485_BaudRate_2400;
            }
            else if (ModbusConfig.BaudRate == 4800)
            {
                RS485_BaudRate.SelectedItem = RS485_BaudRate_4800;
            }
            else if (ModbusConfig.BaudRate == 9600)
            {
                RS485_BaudRate.SelectedItem = RS485_BaudRate_9600;
            }
            else if (ModbusConfig.BaudRate == 19200)
            {
                RS485_BaudRate.SelectedItem = RS485_BaudRate_19200;
            }
            else if (ModbusConfig.BaudRate == 38400)
            {
                RS485_BaudRate.SelectedItem = RS485_BaudRate_38400;
            }
            else if (ModbusConfig.BaudRate == 57600)
            {
                RS485_BaudRate.SelectedItem = RS485_BaudRate_57600;
            }
            else if (ModbusConfig.BaudRate == 115200)
            {
                RS485_BaudRate.SelectedItem = RS485_BaudRate_115200;
            }

        }
        private void RS485_Parity_Information()
        {
            if (ModbusConfig.Parity == DpsDevice.ModbusParity.Even)
            {
                RS485_Parity.SelectedItem = RS485_Parity_Even;
            }
            else if (ModbusConfig.Parity == DpsDevice.ModbusParity.None)
            {
                RS485_Parity.SelectedItem = RS485_Parity_None;
            }
            else if (ModbusConfig.Parity == DpsDevice.ModbusParity.Odd)
            {
                RS485_Parity.SelectedItem = RS485_Parity_Odd;
            }

        }
        private void RS485_NumStopBit_Information()
        {
            if (ModbusConfig.NumStopBit == DpsDevice.ModbusNumStopBit.One)
            {
                RS485_NumStopBit.SelectedItem = RS485_NumStopBit_1;
            }
            else if (ModbusConfig.NumStopBit == DpsDevice.ModbusNumStopBit.Two)
            {
                RS485_NumStopBit.SelectedItem = RS485_NumStopBit_2;
            }

        }

        private void RS485_Id_GotFocus(object sender, RoutedEventArgs e)
        {
            RS485_Id.Background = Brushes.White;
        }

        private async void RS485_Id_LostFocus(object sender, RoutedEventArgs e)
        {
            RS485_Id.Background = null;
            try
            {
                RS485_Id.Background = null;
                byte id_b;
                int id_i;
                id_i = Convert.ToInt16(RS485_Id.Text);
                if (id_i <= byte.MinValue)
                {
                    id_i = 0;
                }
                else if (id_i >= byte.MaxValue)
                {
                    id_i = 255;
                }
                id_b = (byte)id_i;
                ModbusConfig.Id = id_b;
                await device.SetModbusConfig(ModbusConfig);
                RS485_Information();
            }
            catch (Exception)
            {
                //MessageBox.Show(ex.ToString(), "ERROR");
            }

        }

        private void RS485_Id_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9]+");
            e.Handled = re.IsMatch(e.Text);
        }

        private async void SendRS485_BaudRate()
        {
            try
            {
                if (RS485_BaudRate.SelectedItem == RS485_BaudRate_300)
                {
                    ModbusConfig.BaudRate=300;
                }
                else if (RS485_BaudRate.SelectedItem == RS485_BaudRate_600)
                {
                    ModbusConfig.BaudRate = 600;
                }
                else if (RS485_BaudRate.SelectedItem == RS485_BaudRate_1200)
                {
                    ModbusConfig.BaudRate = 1200;
                }
                else if (RS485_BaudRate.SelectedItem == RS485_BaudRate_2400)
                {
                    ModbusConfig.BaudRate = 2400;
                }
                else if (RS485_BaudRate.SelectedItem == RS485_BaudRate_4800)
                {
                    ModbusConfig.BaudRate = 4800;
                }
                else if (RS485_BaudRate.SelectedItem == RS485_BaudRate_9600)
                {
                    ModbusConfig.BaudRate = 9600;
                }
                else if (RS485_BaudRate.SelectedItem == RS485_BaudRate_19200)
                {
                    ModbusConfig.BaudRate = 19200;
                }
                else if (RS485_BaudRate.SelectedItem == RS485_BaudRate_38400)
                {
                    ModbusConfig.BaudRate = 38400;
                }
                else if (RS485_BaudRate.SelectedItem == RS485_BaudRate_57600)
                {
                    ModbusConfig.BaudRate = 57600;
                }
                else if (RS485_BaudRate.SelectedItem == RS485_BaudRate_115200)
                {
                    ModbusConfig.BaudRate = 115200;
                }
                await device.SetModbusConfig(ModbusConfig);
                RS485_Information();
            }
            catch (Exception )
            {
                //MessageBox.Show(e.ToString(), "ERROR");
            }

        }
        private void RS485_BaudRate_LostFocus(object sender, RoutedEventArgs e)
        {
            SendRS485_BaudRate();
        }
        private async void SendRS485_Parity()
        {
            try
            {
                if (RS485_Parity.SelectedItem == RS485_Parity_Even)
                {
                    ModbusConfig.Parity = DpsDevice.ModbusParity.Even;
                }
                else if (RS485_Parity.SelectedItem == RS485_Parity_None)
                {
                    ModbusConfig.Parity = DpsDevice.ModbusParity.None;
                }
                else if (RS485_Parity.SelectedItem == RS485_Parity_Odd)
                {
                    ModbusConfig.Parity = DpsDevice.ModbusParity.Odd;
                }
                await device.SetModbusConfig(ModbusConfig);
                RS485_Information();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "ERROR");
            }

        }
        private void RS485_Parity_LostFocus(object sender, RoutedEventArgs e)
        {
            SendRS485_Parity();
        }
        private async void Relay1_Send_Threshold()
        {
            if (RelayConfig[0].ChannelSelect == 0)
            {

                if (ChannelParamenters[0].TConfig == DpsDevice.TConfig.Position)
                {
                    float thresholdsendPosition;
                    thresholdsendPosition = Convert.ToSingle(Relay1_Threshold.Text);
                    RelayConfig[0].Threshold = thresholdsendPosition / 100;
                }
                else if (ChannelParamenters[0].TConfig == DpsDevice.TConfig.Vibration)
                {
                    float thresholdsendVibration;
                    thresholdsendVibration = Convert.ToSingle(Relay1_Threshold.Text);
                    RelayConfig[0].Threshold = thresholdsendVibration / ChannelParamenters[0].OutputFullScale;
                }
                else if (ChannelParamenters[0].TConfig == DpsDevice.TConfig.Tacho)
                {
                    float thresholdsendTacho;
                    thresholdsendTacho = Convert.ToSingle(Relay1_Threshold.Text);
                    RelayConfig[0].Threshold = thresholdsendTacho / ChannelParamenters[0].TachoRange;
                }
            }
            else if (RelayConfig[0].ChannelSelect == 1)
            {

                if (ChannelParamenters[1].TConfig == DpsDevice.TConfig.Position)
                {
                    float thresholdsendPosition;
                    thresholdsendPosition = Convert.ToSingle(Relay1_Threshold.Text);
                    RelayConfig[0].Threshold = thresholdsendPosition / 100;
                }
                else if (ChannelParamenters[1].TConfig == DpsDevice.TConfig.Vibration)
                {
                    float thresholdsendVibration;
                    thresholdsendVibration = Convert.ToSingle(Relay1_Threshold.Text);
                    RelayConfig[0].Threshold = thresholdsendVibration / ChannelParamenters[1].OutputFullScale;
                }
                else if (ChannelParamenters[1].TConfig == DpsDevice.TConfig.Tacho)
                {
                    float thresholdsendTacho;
                    thresholdsendTacho = Convert.ToSingle(Relay1_Threshold.Text);
                    RelayConfig[0].Threshold = thresholdsendTacho / ChannelParamenters[1].TachoRange;
                }
            }

            RelayConfig[0]= await device.SetRelayConfig(0, RelayConfig[0]);

        }
        private void Relay1_Threshold_LostFocus(object sender, RoutedEventArgs e)
        {
            Relay1_Send_Threshold();
            Relay_ALL_Information();
        }
        private async void Relay2_Send_Threshold()
        {
            if (RelayConfig[1].ChannelSelect == 0)
            {

                if (ChannelParamenters[0].TConfig == DpsDevice.TConfig.Position)
                {
                    float thresholdsendPosition;
                    thresholdsendPosition = Convert.ToSingle(Relay2_Threshold.Text);
                    RelayConfig[1].Threshold = thresholdsendPosition / 100;
                }
                else if (ChannelParamenters[0].TConfig == DpsDevice.TConfig.Vibration)
                {
                    float thresholdsendVibration;
                    thresholdsendVibration = Convert.ToSingle(Relay2_Threshold.Text);
                    RelayConfig[1].Threshold = thresholdsendVibration / ChannelParamenters[0].OutputFullScale;
                }
                else if (ChannelParamenters[0].TConfig == DpsDevice.TConfig.Tacho)
                {
                    float thresholdsendTacho;
                    thresholdsendTacho = Convert.ToSingle(Relay2_Threshold.Text);
                    RelayConfig[1].Threshold = thresholdsendTacho / ChannelParamenters[0].TachoRange;
                }
            }
            else if (RelayConfig[1].ChannelSelect == 1)
            {

                if (ChannelParamenters[1].TConfig == DpsDevice.TConfig.Position)
                {
                    float thresholdsendPosition;
                    thresholdsendPosition = Convert.ToSingle(Relay2_Threshold.Text);
                    RelayConfig[1].Threshold = thresholdsendPosition / 100;
                }
                else if (ChannelParamenters[1].TConfig == DpsDevice.TConfig.Vibration)
                {
                    float thresholdsendVibration;
                    thresholdsendVibration = Convert.ToSingle(Relay2_Threshold.Text);
                    RelayConfig[1].Threshold = thresholdsendVibration / ChannelParamenters[1].OutputFullScale;
                }
                else if (ChannelParamenters[1].TConfig == DpsDevice.TConfig.Tacho)
                {
                    float thresholdsendTacho;
                    thresholdsendTacho = Convert.ToSingle(Relay2_Threshold.Text);
                    RelayConfig[1].Threshold = thresholdsendTacho / ChannelParamenters[1].TachoRange;
                }
            }

            RelayConfig[1] = await device.SetRelayConfig(1, RelayConfig[1]);

        }

        private void Relay2_Threshold_LostFocus(object sender, RoutedEventArgs e)
        {
            Relay2_Send_Threshold();
            Relay_ALL_Information();
        }
        private async void Relay1_Send_ThresholdOther()
        {
            if (RelayConfig[0].ChannelSelect == 0)
            {

                if (ChannelParamenters[1].TConfig == DpsDevice.TConfig.Position)
                {
                    float thresholdsendPosition;
                    thresholdsendPosition = Convert.ToSingle(Relay1_ThresholdOther.Text);
                    RelayConfig[0].ThresholdOther = thresholdsendPosition / 100;
                }
                else if (ChannelParamenters[1].TConfig == DpsDevice.TConfig.Vibration)
                {
                    float thresholdsendVibration;
                    thresholdsendVibration = Convert.ToSingle(Relay1_ThresholdOther.Text);
                    RelayConfig[0].ThresholdOther = thresholdsendVibration / ChannelParamenters[1].OutputFullScale;
                }
                else if (ChannelParamenters[1].TConfig == DpsDevice.TConfig.Tacho)
                {
                    float thresholdsendTacho;
                    thresholdsendTacho = Convert.ToSingle(Relay1_ThresholdOther.Text);
                    RelayConfig[0].ThresholdOther = thresholdsendTacho / ChannelParamenters[1].TachoRange;
                }
            }
            else if (RelayConfig[0].ChannelSelect == 1)
            {

                if (ChannelParamenters[0].TConfig == DpsDevice.TConfig.Position)
                {
                    float thresholdsendPosition;
                    thresholdsendPosition = Convert.ToSingle(Relay1_ThresholdOther.Text);
                    RelayConfig[0].ThresholdOther = thresholdsendPosition / 100;
                }
                else if (ChannelParamenters[0].TConfig == DpsDevice.TConfig.Vibration)
                {
                    float thresholdsendVibration;
                    thresholdsendVibration = Convert.ToSingle(Relay1_ThresholdOther.Text);
                    RelayConfig[0].ThresholdOther = thresholdsendVibration / ChannelParamenters[0].OutputFullScale;
                }
                else if (ChannelParamenters[0].TConfig == DpsDevice.TConfig.Tacho)
                {
                    float thresholdsendTacho;
                    thresholdsendTacho = Convert.ToSingle(Relay1_ThresholdOther.Text);
                    RelayConfig[0].ThresholdOther = thresholdsendTacho / ChannelParamenters[0].TachoRange;
                }
            }

            RelayConfig[0] = await device.SetRelayConfig(0, RelayConfig[0]);

        }
        private void Relay1_ThresholdOther_LostFocus(object sender, RoutedEventArgs e)
        {
            Relay1_Send_ThresholdOther();
            Relay_ALL_Information();
        }
        private async void Relay2_Send_ThresholdOther()
        {
            if (RelayConfig[1].ChannelSelect == 0)
            {

                if (ChannelParamenters[1].TConfig == DpsDevice.TConfig.Position)
                {
                    float thresholdsendPosition;
                    thresholdsendPosition = Convert.ToSingle(Relay2_ThresholdOther.Text);
                    RelayConfig[1].ThresholdOther = thresholdsendPosition / 100;
                }
                else if (ChannelParamenters[1].TConfig == DpsDevice.TConfig.Vibration)
                {
                    float thresholdsendVibration;
                    thresholdsendVibration = Convert.ToSingle(Relay2_ThresholdOther.Text);
                    RelayConfig[1].ThresholdOther = thresholdsendVibration / ChannelParamenters[1].OutputFullScale;
                }
                else if (ChannelParamenters[1].TConfig == DpsDevice.TConfig.Tacho)
                {
                    float thresholdsendTacho;
                    thresholdsendTacho = Convert.ToSingle(Relay2_ThresholdOther.Text);
                    RelayConfig[1].ThresholdOther = thresholdsendTacho / ChannelParamenters[1].TachoRange;
                }
            }
            else if (RelayConfig[1].ChannelSelect == 1)
            {

                if (ChannelParamenters[0].TConfig == DpsDevice.TConfig.Position)
                {
                    float thresholdsendPosition;
                    thresholdsendPosition = Convert.ToSingle(Relay2_ThresholdOther.Text);
                    RelayConfig[1].ThresholdOther = thresholdsendPosition / 100;
                }
                else if (ChannelParamenters[0].TConfig == DpsDevice.TConfig.Vibration)
                {
                    float thresholdsendVibration;
                    thresholdsendVibration = Convert.ToSingle(Relay2_ThresholdOther.Text);
                    RelayConfig[1].ThresholdOther = thresholdsendVibration / ChannelParamenters[0].OutputFullScale;
                }
                else if (ChannelParamenters[0].TConfig == DpsDevice.TConfig.Tacho)
                {
                    float thresholdsendTacho;
                    thresholdsendTacho = Convert.ToSingle(Relay2_ThresholdOther.Text);
                    RelayConfig[1].ThresholdOther = thresholdsendTacho / ChannelParamenters[0].TachoRange;
                }
            }

            RelayConfig[1] = await device.SetRelayConfig(1, RelayConfig[1]);

        }
        private void Relay2_ThresholdOther_LostFocus(object sender, RoutedEventArgs e)
        {
            Relay2_Send_ThresholdOther();
            Relay_ALL_Information();
        }
        float Channel1_v0, Channel1_x0, Channel1_v1, Channel1_x1, Channel1_adcZeroVoltage, Channel1_adcFullVoltage;



        private void Btn_Channel1_adcFullVoltage_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("是否取消", "取消", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
            {
                Channel1_v1 = Convert.ToSingle(Channel1_ADC_UpVoltage.Text);
                Thread.Sleep(3);
                Channel1_x1 = (Measurement_Voltage1 - ChannelParamenters[0].AdcZeroVoltage) / (ChannelParamenters[0].AdcFullVoltage - ChannelParamenters[0].AdcZeroVoltage);
                Thread.Sleep(5);
                Channel1_Calibration();
                MessageBox.Show("Channel1 ADC标定成功");
                Channel1_ADC_LowVoltage.IsEnabled = true;
                Channel1_ADC_UpVoltage.IsEnabled = false;
                Btn_Channel1_adcZeroVoltage.IsEnabled = true;
                Btn_Channel1_adcFullVoltage.IsEnabled = false;
            }
            else 
            {
                Channel1_ADC_LowVoltage.IsEnabled = true;
                Channel1_ADC_UpVoltage.IsEnabled = false;
                Btn_Channel1_adcZeroVoltage.IsEnabled = true;
                Btn_Channel1_adcFullVoltage.IsEnabled = false;
            }


        }
        private async void Channel1_Calibration()
        {
            Channel1_adcZeroVoltage = (Channel1_x1 * Channel1_v0 - Channel1_x0 * Channel1_v1) / (Channel1_x1 - Channel1_x0);
            Channel1_adcFullVoltage = (Channel1_v0 * (Channel1_x1 - 1) - Channel1_v1 * (Channel1_x0 - 1)) / (Channel1_x1 - Channel1_x0);
            ChannelParamenters[0].AdcZeroVoltage = Channel1_adcZeroVoltage;
            ChannelParamenters[0].AdcFullVoltage = Channel1_adcFullVoltage;
            ChannelParamenters[0] = await device.SetChannelParameters(0, ChannelParamenters[0]);
        }
        float Channel2_v0, Channel2_x0, Channel2_v1, Channel2_x1, Channel2_adcZeroVoltage, Channel2_adcFullVoltage;


        private void Btn_Channel2_adcZeroVoltage_Click(object sender, RoutedEventArgs e)
        {
            Channel2_v0 = Convert.ToSingle(Channel2_ADC_LowVoltage.Text);
            Thread.Sleep(3);
            Channel2_x0 = (Measurement_Voltage2-ChannelParamenters[1].AdcZeroVoltage) / (ChannelParamenters[1].AdcFullVoltage- ChannelParamenters[1].AdcZeroVoltage);
            MessageBox.Show("Channel2 ADCZero设定成功,请调整信号源至Channel2上限电压值");
            Btn_Channel2_adcZeroVoltage.IsEnabled = false;
            Btn_Channel2_adcFullVoltage.IsEnabled = true;
            Channel2_ADC_LowVoltage.IsEnabled = false;
            Channel2_ADC_UpVoltage.IsEnabled = true;

        }


        private void Btn_Channel2_adcFullVoltage_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("是否校准", "校准", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
            {

                Channel2_v1 = Convert.ToSingle(Channel2_ADC_UpVoltage.Text);
                Thread.Sleep(3);
                Channel2_x1 = (Measurement_Voltage2 - ChannelParamenters[1].AdcZeroVoltage) / (ChannelParamenters[1].AdcFullVoltage - ChannelParamenters[1].AdcZeroVoltage);
                Thread.Sleep(5);
                Channel2_Calibration();
                MessageBox.Show("Channel2 ADC标定成功");
                Channel2_ADC_LowVoltage.IsEnabled = true;
                Channel2_ADC_UpVoltage.IsEnabled = false;
                Btn_Channel2_adcZeroVoltage.IsEnabled = true;
                Btn_Channel2_adcFullVoltage.IsEnabled = false;
            }
            else
            {
                Channel2_ADC_LowVoltage.IsEnabled = true;
                Channel2_ADC_UpVoltage.IsEnabled = false;
                Btn_Channel2_adcZeroVoltage.IsEnabled = true;
                Btn_Channel2_adcFullVoltage.IsEnabled = false;

            }
        }


        private async void Channel2_Calibration()
        {
            Channel2_adcZeroVoltage = (Channel2_x1 * Channel2_v0 - Channel2_x0 * Channel2_v1) / (Channel2_x1 - Channel2_x0);
            Channel2_adcFullVoltage = (Channel2_v0 * (Channel2_x1 - 1) - Channel2_v1 * (Channel2_x0 - 1)) / (Channel2_x1 - Channel2_x0);
            ChannelParamenters[1].AdcZeroVoltage = Channel2_adcZeroVoltage;
            ChannelParamenters[1].AdcFullVoltage = Channel2_adcFullVoltage;
            ChannelParamenters[1]=await device.SetChannelParameters(1, ChannelParamenters[1]);
         }


        private void Channel_Calibration_Information()
        {
            Channel1_ADC_LowVoltage.IsEnabled = true;
            Channel1_ADC_UpVoltage.IsEnabled = false;
            Channel2_ADC_LowVoltage.IsEnabled = true;
            Channel2_ADC_UpVoltage.IsEnabled = false;
            Btn_Channel1_adcZeroVoltage.IsEnabled = true;
            Btn_Channel1_adcFullVoltage.IsEnabled = false;
            Btn_Channel2_adcZeroVoltage.IsEnabled = true;
            Btn_Channel2_adcFullVoltage.IsEnabled = false;
            //Channel1_DAC_Calibration.IsEnabled = false;
            //Channel2_DAC_Calibration.IsEnabled = false;
            Channel1_FixedOutCurrent.IsChecked = false;
            Channel2_FixedOutCurrent.IsChecked = false;
            Ch1_FixedOutCurrent_4.IsEnabled = false;
            Ch1_FixedOutCurrent_8.IsEnabled = false;
            Ch1_FixedOutCurrent_12.IsEnabled = false;
            Ch1_FixedOutCurrent_16.IsEnabled = false;
            Ch1_FixedOutCurrent_20.IsEnabled = false;
            Channel1_DACTrim_4mA.IsEnabled = false;
            Channel1_DACTrim_8mA.IsEnabled = false;
            Channel1_DACTrim_12mA.IsEnabled = false;
            Channel1_DACTrim_16mA.IsEnabled = false;
            Channel1_DACTrim_20mA.IsEnabled = false;
            Channel1_Custom_current.IsEnabled = false;
            Ch1_Custom_current.IsEnabled = false;
            Ch2_FixedOutCurrent_4.IsEnabled = false;
            Ch2_FixedOutCurrent_8.IsEnabled = false;
            Ch2_FixedOutCurrent_12.IsEnabled = false;
            Ch2_FixedOutCurrent_16.IsEnabled = false;
            Ch2_FixedOutCurrent_20.IsEnabled = false;
            Channel2_DACTrim_4mA.IsEnabled = false;
            Channel2_DACTrim_8mA.IsEnabled = false;
            Channel2_DACTrim_12mA.IsEnabled = false;
            Channel2_DACTrim_16mA.IsEnabled = false;
            Channel2_DACTrim_20mA.IsEnabled = false;
            Channel2_Custom_current.IsEnabled = false;
            Ch2_Custom_current.IsEnabled = false;
        }


        private async void Channel1_FixedOutCurrent_Click(object sender, RoutedEventArgs e)
        {
            Channel1_DACTrim_4mA.Text = DacTrimArray1[0].ToString();
            Channel1_DACTrim_8mA.Text = DacTrimArray1[1].ToString();
            Channel1_DACTrim_12mA.Text = DacTrimArray1[2].ToString();
            Channel1_DACTrim_16mA.Text = DacTrimArray1[3].ToString();
            Channel1_DACTrim_20mA.Text = DacTrimArray1[4].ToString();
            if (Channel1_FixedOutCurrent.IsChecked == true)
            {
                Ch1_FixedOutCurrent_4.IsEnabled = true;
                Ch1_FixedOutCurrent_4.IsChecked = true;
                DAC_Trim_Information();
                Ch1_FixedOutCurrent_8.IsEnabled = true;
                Ch1_FixedOutCurrent_12.IsEnabled = true;
                Ch1_FixedOutCurrent_16.IsEnabled = true;
                Ch1_FixedOutCurrent_20.IsEnabled = true;
                Channel1_DACTrim_4mA.IsEnabled = true;
                Channel1_DACTrim_8mA.IsEnabled = true;
                Channel1_DACTrim_12mA.IsEnabled = true;
                Channel1_DACTrim_16mA.IsEnabled = true;
                Channel1_DACTrim_20mA.IsEnabled = true;
                Channel1_Custom_current.IsEnabled = true;
                Ch1_Custom_current.IsEnabled = true;
            }
            else
            {
                await device.SetFixedOutCurrent(0,0);
                Ch1_FixedOutCurrent_4.IsEnabled = false;
                Ch1_FixedOutCurrent_4.IsChecked = false;
                Ch1_FixedOutCurrent_8.IsEnabled = false;
                Ch1_FixedOutCurrent_12.IsEnabled = false;
                Ch1_FixedOutCurrent_16.IsEnabled = false;
                Ch1_FixedOutCurrent_20.IsEnabled = false;
                Channel1_DACTrim_4mA.IsEnabled = false;
                Channel1_DACTrim_8mA.IsEnabled = false;
                Channel1_DACTrim_12mA.IsEnabled = false;
                Channel1_DACTrim_16mA.IsEnabled = false;
                Channel1_DACTrim_20mA.IsEnabled = false;
                Channel1_Custom_current.IsEnabled = false;
                Ch1_Custom_current.IsEnabled = false;
            }

        }

        private async void DAC_Trim_Information()
        {
            if(Ch1_FixedOutCurrent_4.IsChecked==true)
            {
                await device.SetFixedOutCurrent(0, 4);
                Channel1_DACTrim_4mA.Text =DacTrimArray1[0].ToString();
            }
            else if (Ch1_FixedOutCurrent_8.IsChecked == true)
            {
                await device.SetFixedOutCurrent(0, 8);
                Channel1_DACTrim_8mA.Text = DacTrimArray1[1].ToString();

            }
            else if (Ch1_FixedOutCurrent_12.IsChecked == true)
            {
                await device.SetFixedOutCurrent(0, 12);
                Channel1_DACTrim_12mA.Text = DacTrimArray1[2].ToString();

            }
            else if (Ch1_FixedOutCurrent_16.IsChecked == true)
            {
                await device.SetFixedOutCurrent(0, 16);
                Channel1_DACTrim_16mA.Text = DacTrimArray1[3].ToString();

            }
            else if (Ch1_FixedOutCurrent_20.IsChecked == true)
            {
                await device.SetFixedOutCurrent(0, 20);
                Channel1_DACTrim_20mA.Text = DacTrimArray1[4].ToString();
            }
            if (Ch2_FixedOutCurrent_4.IsChecked == true)
            {
                await device.SetFixedOutCurrent(1, 4);
                Channel2_DACTrim_4mA.Text = DacTrimArray2[0].ToString();
            }
            else if (Ch2_FixedOutCurrent_8.IsChecked == true)
            {
                await device.SetFixedOutCurrent(1, 8);
                Channel2_DACTrim_8mA.Text = DacTrimArray2[1].ToString();

            }
            else if (Ch2_FixedOutCurrent_12.IsChecked == true)
            {
                await device.SetFixedOutCurrent(1, 12);
                Channel2_DACTrim_12mA.Text = DacTrimArray2[2].ToString();

            }
            else if (Ch2_FixedOutCurrent_16.IsChecked == true)
            {
                await device.SetFixedOutCurrent(1, 16);
                Channel2_DACTrim_16mA.Text = DacTrimArray2[3].ToString();

            }
            else if (Ch2_FixedOutCurrent_20.IsChecked == true)
            {
                await device.SetFixedOutCurrent(1, 20);
                Channel2_DACTrim_20mA.Text = DacTrimArray2[4].ToString();
            }

        }


        private async void Channel2_FixedOutCurrent_Click(object sender, RoutedEventArgs e)
        {
            Channel2_DACTrim_4mA.Text = DacTrimArray2[0].ToString();
            Channel2_DACTrim_8mA.Text = DacTrimArray2[1].ToString();
            Channel2_DACTrim_12mA.Text = DacTrimArray2[2].ToString();
            Channel2_DACTrim_16mA.Text = DacTrimArray2[3].ToString();
            Channel2_DACTrim_20mA.Text = DacTrimArray2[4].ToString();

            if (Channel2_FixedOutCurrent.IsChecked == true)
            {
                Ch2_FixedOutCurrent_4.IsEnabled = true;
                Ch2_FixedOutCurrent_4.IsChecked = true;
                DAC_Trim_Information();
                Ch2_FixedOutCurrent_8.IsEnabled = true;
                Ch2_FixedOutCurrent_12.IsEnabled = true;
                Ch2_FixedOutCurrent_16.IsEnabled = true;
                Ch2_FixedOutCurrent_20.IsEnabled = true;
                Channel2_DACTrim_4mA.IsEnabled = true;
                Channel2_DACTrim_8mA.IsEnabled = true;
                Channel2_DACTrim_12mA.IsEnabled = true;
                Channel2_DACTrim_16mA.IsEnabled = true;
                Channel2_DACTrim_20mA.IsEnabled = true;
                Channel2_Custom_current.IsEnabled = true;
                Ch2_Custom_current.IsEnabled = true;

            }
            else
            {
                await device.SetFixedOutCurrent(1, 0);
                Ch2_FixedOutCurrent_4.IsEnabled = false;
                Ch2_FixedOutCurrent_4.IsChecked = false;
                Ch2_FixedOutCurrent_8.IsEnabled = false;
                Ch2_FixedOutCurrent_12.IsEnabled = false;
                Ch2_FixedOutCurrent_16.IsEnabled = false;
                Ch2_FixedOutCurrent_20.IsEnabled = false;
                Channel2_DACTrim_4mA.IsEnabled = false;
                Channel2_DACTrim_8mA.IsEnabled = false;
                Channel2_DACTrim_12mA.IsEnabled = false;
                Channel2_DACTrim_16mA.IsEnabled = false;
                Channel2_DACTrim_20mA.IsEnabled = false;
                Channel2_Custom_current.IsEnabled = false;
                Ch2_Custom_current.IsEnabled = false;

            }

        }


        float FixedOutCurrent_1, FixedOutCurrent_2;


        private async void Ch1_FixedOutCurrent_4_Click(object sender, RoutedEventArgs e)
        {
            FixedOutCurrent_1 = 4f;
            Channel1_DACTrim_4mA.Text = DacTrimArray1[0].ToString();
            await device.SetFixedOutCurrent(0, FixedOutCurrent_1);

        }


        private async void Ch1_FixedOutCurrent_8_Click(object sender, RoutedEventArgs e)
        {
            FixedOutCurrent_1 = 8f;
            Channel1_DACTrim_8mA.Text = DacTrimArray1[1].ToString();
            await device.SetFixedOutCurrent(0, FixedOutCurrent_1);

        }


        private async void Ch1_FixedOutCurrent_12_Click(object sender, RoutedEventArgs e)
        {
            FixedOutCurrent_1 = 12f;
            Channel1_DACTrim_12mA.Text = DacTrimArray1[2].ToString();
            await device.SetFixedOutCurrent(0, FixedOutCurrent_1);

        }


        private async void Ch1_FixedOutCurrent_16_Click(object sender, RoutedEventArgs e)
        {
            FixedOutCurrent_1 = 16f;
            Channel1_DACTrim_16mA.Text = DacTrimArray1[3].ToString();
            await device.SetFixedOutCurrent(0, FixedOutCurrent_1);

        }
        private async void Ch1_FixedOutCurrent_20_Click(object sender, RoutedEventArgs e)
        {
            FixedOutCurrent_1 = 20f;
            Channel1_DACTrim_20mA.Text = DacTrimArray1[4].ToString();
            await device.SetFixedOutCurrent(0, FixedOutCurrent_1);

        }


        private async void Channel1_Custom_current_Click(object sender, RoutedEventArgs e)
        {
            FixedOutCurrent_1 = Convert.ToSingle(Ch1_Custom_current.Text);
            await device.SetFixedOutCurrent(0, FixedOutCurrent_1);
        }


        private async void Ch1_Custom_current_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                FixedOutCurrent_1 = Convert.ToSingle(Ch1_Custom_current.Text);
                if(FixedOutCurrent_1>24f)
                {
                    FixedOutCurrent_1 = 24f;
                    MessageBox.Show("输入电流大于24mA");
                    Ch1_Custom_current.Text = 24.ToString();
                }
                else if(FixedOutCurrent_1 < 3.5f)
                {
                    FixedOutCurrent_1 = 3.5f;
                    MessageBox.Show("输入电流小于3.5mA");
                    Ch1_Custom_current.Text = 3.5.ToString();
                }
                await device.SetFixedOutCurrent(0, FixedOutCurrent_1);
                Ch1_Custom_current.Text = FixedOutCurrent_1.ToString();
                

            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString(), "ERROR");
            }
        }


        private async void Ch2_FixedOutCurrent_4_Click(object sender, RoutedEventArgs e)
        {
            FixedOutCurrent_2 = 4f;
            Channel2_DACTrim_4mA.Text = DacTrimArray2[0].ToString();
            await device.SetFixedOutCurrent(1, FixedOutCurrent_2);

        }


        private async void Ch2_FixedOutCurrent_8_Click(object sender, RoutedEventArgs e)
        {
            FixedOutCurrent_2 = 8f;
            Channel2_DACTrim_8mA.Text = DacTrimArray2[1].ToString();
            await device.SetFixedOutCurrent(1, FixedOutCurrent_2);
        }


        private async void Ch2_FixedOutCurrent_12_Click(object sender, RoutedEventArgs e)
        {
            FixedOutCurrent_2 = 12f;
            Channel2_DACTrim_12mA.Text = DacTrimArray2[2].ToString();
            await device.SetFixedOutCurrent(1, FixedOutCurrent_2);

        }


        private async void Ch2_FixedOutCurrent_16_Click(object sender, RoutedEventArgs e)
        {
            FixedOutCurrent_2 = 16f;
            Channel2_DACTrim_16mA.Text = DacTrimArray2[3].ToString();
            await device.SetFixedOutCurrent(1, FixedOutCurrent_2);

        }


        private async void Ch2_FixedOutCurrent_20_Click(object sender, RoutedEventArgs e)
        {
            FixedOutCurrent_2 = 20f;
            Channel2_DACTrim_20mA.Text = DacTrimArray2[4].ToString();
            await device.SetFixedOutCurrent(1, FixedOutCurrent_2);
        }


        private async void Channel2_Custom_current_Click(object sender, RoutedEventArgs e)
        {
            FixedOutCurrent_2 = Convert.ToSingle(Ch2_Custom_current.Text);
            await device.SetFixedOutCurrent(1, FixedOutCurrent_2);
        }


        private async void Ch2_Custom_current_LostFocus(object sender, RoutedEventArgs e)
        {
            FixedOutCurrent_2 = Convert.ToSingle(Ch2_Custom_current.Text);
            if (FixedOutCurrent_2 > 24f)
            {
                FixedOutCurrent_2 = 24f;
                MessageBox.Show("输入电流大于24mA");
                Ch2_Custom_current.Text = 24.ToString();
            }
            else if (FixedOutCurrent_2 < 3.5f)
            {
                FixedOutCurrent_2 = 3.5f;
                MessageBox.Show("输入电流小于3.5mA");
                Ch2_Custom_current.Text = 3.5.ToString();
            }

            await device.SetFixedOutCurrent(1, FixedOutCurrent_2);
            Ch2_Custom_current.Text = FixedOutCurrent_2.ToString();
        }

        private void Relay1_DelayTimeMs_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9.]+");
            e.Handled = re.IsMatch(e.Text);

        }

        private void Relay1_Threshold_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9.]+");
            e.Handled = re.IsMatch(e.Text);

        }

        private void Relay1_ThresholdOther_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9.]+");
            e.Handled = re.IsMatch(e.Text);

        }

        private void Relay2_DelayTimeMs_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9.]+");
            e.Handled = re.IsMatch(e.Text);

        }

        private void Relay2_Threshold_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9.]+");
            e.Handled = re.IsMatch(e.Text);

        }

        private void Relay2_ThresholdOther_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9.]+");
            e.Handled = re.IsMatch(e.Text);

        }

        private void Channel1_ADC_LowVoltage_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9.]+");
            e.Handled = re.IsMatch(e.Text);

        }

        private void Channel1_ADC_UpVoltage_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9.]+");
            e.Handled = re.IsMatch(e.Text);

        }

        private void Channel2_ADC_LowVoltage_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9.]+");
            e.Handled = re.IsMatch(e.Text);

        }

        private void Channel2_ADC_UpVoltage_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9.]+");
            e.Handled = re.IsMatch(e.Text);

        }

        private void Channel1_DACTrim_4mA_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9-+]+");
            e.Handled = re.IsMatch(e.Text);

        }

        private void Channel1_DACTrim_8mA_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9-+]+");
            e.Handled = re.IsMatch(e.Text);

        }

        private void Channel1_DACTrim_12mA_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9-+]+");
            e.Handled = re.IsMatch(e.Text);
        }

        private void Channel1_DACTrim_16mA_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9-+]+");
            e.Handled = re.IsMatch(e.Text);
        }

        private void Channel1_DACTrim_20mA_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9-+]+");
            e.Handled = re.IsMatch(e.Text);
        }

        private void Ch1_Custom_current_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9.]+");
            e.Handled = re.IsMatch(e.Text);
        }

        private void Channel2_DACTrim_4mA_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9-+]+");
            e.Handled = re.IsMatch(e.Text);
        }

        private void Channel2_DACTrim_8mA_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9-+]+");
            e.Handled = re.IsMatch(e.Text);
        }

        private void Channel2_DACTrim_12mA_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9-+]+");
            e.Handled = re.IsMatch(e.Text);
        }

        private void Channel2_DACTrim_16mA_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9-+]+");
            e.Handled = re.IsMatch(e.Text);
        }

        private void Channel2_DACTrim_20mA_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9-+]+");
            e.Handled = re.IsMatch(e.Text);
        }

        private void Ch2_Custom_current_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9.]+");
            e.Handled = re.IsMatch(e.Text);

        }

        private void Channel2_DampingTimeMs_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9.]+");
            e.Handled = re.IsMatch(e.Text);
        }

        private void Channel1_DampingTimeMs_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9.]+");
            e.Handled = re.IsMatch(e.Text);

        }

        private void Channel1_PositionLowerVoltage_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9.]+");
            e.Handled = re.IsMatch(e.Text);

        }

        private void Channel1_PositionUpperVoltage_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9.]+");
            e.Handled = re.IsMatch(e.Text);

        }

        private void Channel2_PositionLowerVoltage_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9.]+");
            e.Handled = re.IsMatch(e.Text);

        }

        private void Channel2_PositionUpperVoltage_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9.]+");
            e.Handled = re.IsMatch(e.Text);

        }

        private void Channel1_SensorSensitivity_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9.]+");
            e.Handled = re.IsMatch(e.Text);

        }

        private void Channel1_OutputFullScale_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9.]+");
            e.Handled = re.IsMatch(e.Text);

        }

        private void Channel1_NoiseGate_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9.]+");
            e.Handled = re.IsMatch(e.Text);

        }

        private void Channel1_OutputGain_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9.]+");
            e.Handled = re.IsMatch(e.Text);

        }

        private void Channel1_LfCutoffFreq_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9.]+");
            e.Handled = re.IsMatch(e.Text);

        }

        private void Channel1_HfCutoffFreq_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9.]+");
            e.Handled = re.IsMatch(e.Text);

        }

        private void Channel1_RslTimeMs_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9.]+");
            e.Handled = re.IsMatch(e.Text);

        }

        private void Channel2_SensorSensitivity_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9.]+");
            e.Handled = re.IsMatch(e.Text);

        }

        private void Channel2_OutputFullScale_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9.]+");
            e.Handled = re.IsMatch(e.Text);

        }

        private void Channel2_NoiseGate_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9.]+");
            e.Handled = re.IsMatch(e.Text);

        }

        private void Channel2_OutputGain_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9.]+");
            e.Handled = re.IsMatch(e.Text);

        }

        private void Channel2_LfCutoffFreq_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9.]+");
            e.Handled = re.IsMatch(e.Text);

        }

        private void Channel2_HfCutoffFreq_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9.]+");
            e.Handled = re.IsMatch(e.Text);

        }

        private void Channel2_RslTimeMs_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9.]+");
            e.Handled = re.IsMatch(e.Text);

        }

        private void Channel1_TachoRange_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9.]+");
            e.Handled = re.IsMatch(e.Text);

        }

        private void Channel1_TachoNormalizer_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9.]+");
            e.Handled = re.IsMatch(e.Text);

        }

        private void Channel1_TachoTriggerThreshold_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9.]+");
            e.Handled = re.IsMatch(e.Text);

        }

        private void Channel2_TachoRange_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9.]+");
            e.Handled = re.IsMatch(e.Text);

        }

        private void Channel2_TachoNormalizer_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9.]+");
            e.Handled = re.IsMatch(e.Text);

        }

        private void Channel2_TachoTriggerThreshold_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9.]+");
            e.Handled = re.IsMatch(e.Text);
        }

        private void Product_MfgSn_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex re = new Regex("[^0-9]+");
            e.Handled = re.IsMatch(e.Text);

        }

        private void Btn_Channel1_adcZeroVoltage_Click_1(object sender, RoutedEventArgs e)
        {
            Channel1_v0 = Convert.ToSingle(Channel1_ADC_LowVoltage.Text);
            Thread.Sleep(3);
            Channel1_x0 = (Measurement_Voltage1 - ChannelParamenters[0].AdcZeroVoltage) / (ChannelParamenters[0].AdcFullVoltage - ChannelParamenters[0].AdcZeroVoltage);
            MessageBox.Show("Channel1 ADCZero设定成功,请调整信号源至Channel1上限电压值");
            Btn_Channel1_adcZeroVoltage.IsEnabled = false;
            Btn_Channel1_adcFullVoltage.IsEnabled = true;
            Channel1_ADC_LowVoltage.IsEnabled = false;
            Channel1_ADC_UpVoltage.IsEnabled = true;

        }

        private async void Default_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                await device.RestoreDefaultSettings();
                DeviceOnConnected();
                MessageBox.Show("恢复默认设置成功");
            }
            catch (Exception)
            {
                //MessageBox.Show(ex.ToString(), "ERROR");
                //throw;
            }

        }

        private async void Auto_MfgSn_Click(object sender, RoutedEventArgs e)
        {
            Information.MfgSn = Product_MfgSn.Text;
            await device.SetMfgSn(Information.MfgSn);
        }
        private async void Product_DeviceModel_Send()
        {
            if(Product_DeviceModel.SelectedItem==Product_DeviceModel_VibraTrol2168)
            {
                Channel1_Type.SelectedItem = Channel1_Vibration_Choose;
                Channel2_Type.SelectedItem = Channel2_Vibration_Choose;
                //Channel1_SendTypeInformation();
                //Channel2_SendTypeInformation();
                Channel1_Type.IsEnabled = false;
                Channel2_Type.IsEnabled = false;
                Information.DeviceModel = "VibraTrol2168";
                Product_Choose2168_Information();
                //ChannelParamenters[0].SensorUnit = DpsDevice.SensorUnit.MvPerG;
                //ChannelParamenters[1].SensorUnit = DpsDevice.SensorUnit.MvPerG;
                //ChannelParamenters[0].OutputUnit = DpsDevice.OutputUnit.Mmps;
                //ChannelParamenters[1].OutputUnit = DpsDevice.OutputUnit.Mmps;

                if (Channel1.IsChecked == true)
                {
                    //Channel1_SensorUnit_Information();
                    //Channel1_SendSensorUnitInformation();
                    //Channel1_OutputUnit_Information();
                    //Channel1_SendOutputUnitInformation();

                    Interface_Channel1_Information();
                }
                else if (Channel1.IsChecked == true)
                {
                    //Channel2_SensorUnit_Information();
                    //Channel2_SendSensorUnitInformation();
                    //Channel2_OutputUnit_Information();
                    //Channel2_SendOutputUnitInformation();

                    Interface_Channel2_Information();
                }
                Channel1_SensorUnit.Items.Remove(Channel1_SensorUnit_VoltPerInch);
                Channel1_SensorUnit.Items.Remove(Channel1_SensorUnit_VoltPerMm);
                Channel2_SensorUnit.Items.Remove(Channel2_SensorUnit_VoltPerInch);
                Channel2_SensorUnit.Items.Remove(Channel2_SensorUnit_VoltPerMm);

                //Channel1_SensorUnit_MvPerG.IsEnabled = true;
                //Channel1_SensorUnit_MvPerIps.IsEnabled = true;
                //Channel1_SensorUnit_MvPerMmps.IsEnabled = true;
                //Channel1_OutputUnit_G.IsEnabled = true;
                //Channel1_OutputUnit_Ips.IsEnabled = true;
                //Channel1_OutputUnit_Mmps.IsEnabled = true;
                //Channel1_OutputUnit_Mps2.IsEnabled = true;
                //Channel2_SensorUnit_MvPerG.IsEnabled = true;
                //Channel2_SensorUnit_MvPerIps.IsEnabled = true;
                //Channel2_SensorUnit_MvPerMmps.IsEnabled = true;
                //Channel2_OutputUnit_G.IsEnabled = true;
                //Channel2_OutputUnit_Ips.IsEnabled = true;
                //Channel2_OutputUnit_Mmps.IsEnabled = true;
                //Channel2_OutputUnit_Mps2.IsEnabled = true;
                //Channel1_OutputUnit_G.IsEnabled = true;
                //Channel1_OutputUnit_Ips.IsEnabled = true;
                //Channel1_OutputUnit_Mmps.IsEnabled = true;
                //Channel1_OutputUnit_Mps2.IsEnabled = true;
                //Channel2_OutputUnit_G.IsEnabled = true;
                //Channel2_OutputUnit_Ips.IsEnabled = true;
                //Channel2_OutputUnit_Mmps.IsEnabled = true;
                //Channel2_OutputUnit_Mps2.IsEnabled = true;




            }
            else if(Product_DeviceModel.SelectedItem == Product_DeviceModel_DPS2016)
            {
                //Channel1_SendTypeInformation();
                //Channel2_SendTypeInformation();
                Channel1_Type.IsEnabled = true;
                Channel2_Type.IsEnabled = true;
                Information.DeviceModel = "DPS2016";
                Product_Choose2016_Information();

                //ChannelParamenters[0].SensorUnit = DpsDevice.SensorUnit.VoltPerMm;
                //ChannelParamenters[1].SensorUnit = DpsDevice.SensorUnit.VoltPerMm;
                //ChannelParamenters[0].OutputUnit = DpsDevice.OutputUnit.Um;
                //ChannelParamenters[1].OutputUnit = DpsDevice.OutputUnit.Um;


                if (Channel1.IsChecked == true)
                {
                    //Channel1_SensorUnit_Information();
                    //Channel1_SendSensorUnitInformation();
                    //Channel1_OutputUnit_Information();
                    //Channel1_SendOutputUnitInformation();

                    Interface_Channel1_Information();

                }
                else if (Channel2.IsChecked == true)
                {
                    //Channel2_SensorUnit_Information();
                    //Channel2_SendSensorUnitInformation();
                    //Channel2_OutputUnit_Information();
                    //Channel2_SendOutputUnitInformation();

                    Interface_Channel2_Information();

                }
                //Channel1_SensorUnit(Channel1_SensorUnit_VoltPerInch);
                Channel1_SensorUnit.Items.Remove(Channel1_SensorUnit_MvPerG);
                Channel1_SensorUnit.Items.Remove(Channel1_SensorUnit_MvPerIps);
                Channel1_SensorUnit.Items.Remove(Channel1_SensorUnit_MvPerMmps);
                Channel1_OutputUnit.Items.Remove(Channel1_OutputUnit_G);
                Channel1_OutputUnit.Items.Remove(Channel1_OutputUnit_Ips);
                Channel1_OutputUnit.Items.Remove(Channel1_OutputUnit_Mmps);
                Channel1_OutputUnit.Items.Remove(Channel1_OutputUnit_Mps2);
                //Channel2_SensorUnit.Items.Add(Channel2_SensorUnit_VoltPerInch);
                //Channel2_SensorUnit.Items.Add(Channel2_SensorUnit_VoltPerMm);
                Channel2_SensorUnit.Items.Remove(Channel2_SensorUnit_MvPerG);
                Channel2_SensorUnit.Items.Remove(Channel2_SensorUnit_MvPerIps);
                Channel2_SensorUnit.Items.Remove(Channel2_SensorUnit_MvPerMmps);
                Channel2_OutputUnit.Items.Remove(Channel2_OutputUnit_G);
                Channel2_OutputUnit.Items.Remove(Channel2_OutputUnit_Ips);
                Channel2_OutputUnit.Items.Remove(Channel2_OutputUnit_Mmps);
                Channel2_OutputUnit.Items.Remove(Channel2_OutputUnit_Mps2);

                //Channel1_SensorUnit_VoltPerInch.IsEnabled = true;
                //Channel1_SensorUnit_VoltPerMm.IsEnabled = true;
                //Channel1_SensorUnit_MvPerG.IsEnabled = false;
                //Channel1_SensorUnit_MvPerIps.IsEnabled = false;
                //Channel1_SensorUnit_MvPerMmps.IsEnabled = false;
                //Channel1_OutputUnit_G.IsEnabled = false;
                //Channel1_OutputUnit_Ips.IsEnabled = false;
                //Channel1_OutputUnit_Mmps.IsEnabled = false;
                //Channel1_OutputUnit_Mps2.IsEnabled = false;
                //Channel2_SensorUnit_VoltPerInch.IsEnabled = true;
                //Channel2_SensorUnit_VoltPerMm.IsEnabled = true;
                //Channel2_SensorUnit_MvPerG.IsEnabled = false;
                //Channel2_SensorUnit_MvPerIps.IsEnabled = false;
                //Channel2_SensorUnit_MvPerMmps.IsEnabled = false;
                //Channel2_OutputUnit_G.IsEnabled = false;
                //Channel2_OutputUnit_Ips.IsEnabled = false;
                //Channel2_OutputUnit_Mmps.IsEnabled = false;
                //Channel2_OutputUnit_Mps2.IsEnabled = false;
                //Channel1_OutputUnit_G.IsEnabled = false;
                //Channel1_OutputUnit_Ips.IsEnabled = false;
                //Channel1_OutputUnit_Mmps.IsEnabled = false;
                //Channel1_OutputUnit_Mps2.IsEnabled = false;
                //Channel2_OutputUnit_G.IsEnabled = false;
                //Channel2_OutputUnit_Ips.IsEnabled = false;
                //Channel2_OutputUnit_Mmps.IsEnabled = false;
                //Channel2_OutputUnit_Mps2.IsEnabled = false;



            }
            //Channel1_SendTypeInformation();
            //Channel2_SendTypeInformation();
            await device.SetDeviceModel(Information.DeviceModel);


        }
        private void Product_DeviceModel_LostFocus(object sender, RoutedEventArgs e)
        {
            Product_DeviceModel_Send();
            Product_Choose_Information();
        }

        private async void SendRS485_NumStopBit()
        {
            try
            {
                if (RS485_NumStopBit.SelectedItem == RS485_NumStopBit_1)
                {
                    ModbusConfig.NumStopBit = DpsDevice.ModbusNumStopBit.One;
                }
                else if (RS485_NumStopBit.SelectedItem == RS485_NumStopBit_2)
                {
                    ModbusConfig.NumStopBit = DpsDevice.ModbusNumStopBit.Two;
                }
                await device.SetModbusConfig(ModbusConfig);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "ERROR");
            }

        }

        private void RS485_NumStopBit_LostFocus(object sender, RoutedEventArgs e)
        {
            SendRS485_NumStopBit();
            RS485_Information();

        }

        private async void Btn_SignalCopy_Click(object sender, RoutedEventArgs e)
        {
            if (ChannelParamenters[0].TConfig == ChannelParamenters[1].TConfig)
            {
                if (Channel1.IsChecked == true)
                {
                    ChannelParamenters[1] = ChannelParamenters[0];
                    ChannelParamenters[1] = await device.SetChannelParameters(1, ChannelParamenters[1]);
                    Interface_Channel2_Information();
                    Interface_Channel1_Information();
                    MessageBox.Show("Channel1复制到Channel2成功");

                }
                else if (Channel2.IsChecked == true)
                {
                    ChannelParamenters[0] = ChannelParamenters[1];
                    ChannelParamenters[0] = await device.SetChannelParameters(0, ChannelParamenters[0]);
                    Interface_Channel2_Information();
                    Interface_Channel1_Information();
                    MessageBox.Show("Channel2复制到Channel1成功");

                }
            }
            else { MessageBox.Show("两个通道不相同，无法复制"); }
        }

        private async void Product_MfgDate_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
            Product_MfgDate.Background = null;
            Information.MfgDate = Convert.ToDateTime(Product_MfgDate.Text);
            await device.SetMfgDate(Information.MfgDate);

            }
            catch(Exception)
            {
                MessageBox.Show("请输入正确日期");
            }

        }

        private void Product_MfgDate_GotFocus(object sender, RoutedEventArgs e)
        {
            Product_MfgDate.Background = Brushes.White;

        }



        private async void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("是否保存", "保存", MessageBoxButton.YesNoCancel, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
            {
                await device.SaveSettingsToFlash();
                Connect_Information();
                //DeviceOnConnected();
                //MessageBox.Show("保存成功");
            }
            else if (result == MessageBoxResult.No)
            {
                await device.DiscardSettings();
                Connect_Information();
            }
            else
            {
                e.Cancel = true;
            }

        }

        private async void Channel2_DampingTimeMs_MouseEnter(object sender, MouseEventArgs e)
        {
            try
            {
                float dampingTimeMs2;

                dampingTimeMs2 = Convert.ToSingle(Channel2_DampingTimeMs.Text) * 1000;
                if ((dampingTimeMs2 - UInt16.MinValue) <= 0)
                {
                    ChannelParamenters[1].DampingTimeMs = 0;
                }
                else if ((dampingTimeMs2 - UInt16.MaxValue) >= 0)
                {
                    ChannelParamenters[1].DampingTimeMs = 60000;
                }
                else
                {
                    ChannelParamenters[1].DampingTimeMs = (ushort)dampingTimeMs2;
                }
                ChannelParamenters[1] = await device.SetChannelParameters(1, ChannelParamenters[1]);
                Interface_Channel2_Information();


            }
            catch (Exception)
            {
                MessageBox.Show("输入错误，请重新输入！", "ERROR");

            }

        }

        private async void Channel1_PositionLowerVoltage_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
            if(e.Key == Key.Enter)
            {
                    ChannelParamenters[0].PositionLowerVoltage = Convert.ToSingle(Channel1_PositionLowerVoltage.Text);
                    ChannelParamenters[0] = await device.SetChannelParameters(0, ChannelParamenters[0]);
                    Interface_Channel1_Information();
                    focus.Focus();
            }

            }

                catch (Exception)
                {
                    MessageBox.Show("输入错误，请重新输入！", "ERROR");
                }

        }


        private void DacTrimsend_Information()
        {
            int Ch1_dactrimarray0, Ch1_dactrimarray1, Ch1_dactrimarray2, Ch1_dactrimarray3, Ch1_dactrimarray4;
            Ch1_dactrimarray0 = Convert.ToInt32(Channel1_DACTrim_4mA.Text);
            if (Ch1_dactrimarray0  <= -1000)
            {
                DacTrimArray1[0] = -1000;
            }
            else if (Ch1_dactrimarray0  >= 1000)
            {
                DacTrimArray1[0] = 1000;
            }
            else
            { DacTrimArray1[0] = Convert.ToInt32(Channel1_DACTrim_4mA.Text); }
            Ch1_dactrimarray1 = Convert.ToInt32(Channel1_DACTrim_8mA.Text);
            if (Ch1_dactrimarray1 <= -1000)
            {
                DacTrimArray1[1] = -1000;
            }
            else if (Ch1_dactrimarray1 >= 1000)
            {
                DacTrimArray1[1] = 1000;
            }
            else
            {
                DacTrimArray1[1] = Convert.ToInt32(Channel1_DACTrim_8mA.Text);
            }
            Ch1_dactrimarray2 = Convert.ToInt32(Channel1_DACTrim_12mA.Text);
            if (Ch1_dactrimarray2 <= -1000)
            {
                DacTrimArray1[2] = -1000;
            }
            else if (Ch1_dactrimarray2 >= 1000)
            {
                DacTrimArray1[2] = 1000;
            }
            else
            {
                DacTrimArray1[2] = Convert.ToInt32(Channel1_DACTrim_12mA.Text);
            }
            Ch1_dactrimarray3 = Convert.ToInt32(Channel1_DACTrim_16mA.Text);
            if (Ch1_dactrimarray3 <= -1000)
            {
                DacTrimArray1[3] = -1000;
            }
            else if (Ch1_dactrimarray3 >= 1000)
            {
                DacTrimArray1[3] = 1000;
            }
            else
            {
                DacTrimArray1[3] = Convert.ToInt32(Channel1_DACTrim_16mA.Text);
            }
            Ch1_dactrimarray4 = Convert.ToInt32(Channel1_DACTrim_20mA.Text);
            if (Ch1_dactrimarray4 <= -1000)
            {
                DacTrimArray1[4] = -1000;
            }
            else if (Ch1_dactrimarray4 >= 1000)
            {
                DacTrimArray1[4] = 1000;
            }
            else
            {
                DacTrimArray1[4] = Convert.ToInt32(Channel1_DACTrim_20mA.Text);
            }
            int Ch2_dactrimarray0, Ch2_dactrimarray1, Ch2_dactrimarray2, Ch2_dactrimarray3, Ch2_dactrimarray4;
            Ch2_dactrimarray0 = Convert.ToInt32(Channel2_DACTrim_4mA.Text);
            if (Ch2_dactrimarray0 <= -1000)
            {
                DacTrimArray2[0] = -1000;
            }
            else if (Ch2_dactrimarray0 >= 1000)
            {
                DacTrimArray2[0] = 1000;
            }
            else
            { DacTrimArray2[0] = Convert.ToInt32(Channel2_DACTrim_4mA.Text); }
            Ch2_dactrimarray1 = Convert.ToInt32(Channel2_DACTrim_8mA.Text);
            if (Ch2_dactrimarray1 <= -1000)
            {
                DacTrimArray2[1] = -1000;
            }
            else if (Ch2_dactrimarray1 >= 1000)
            {
                DacTrimArray2[1] = 1000;
            }
            else
            {
                DacTrimArray2[1] = Convert.ToInt32(Channel2_DACTrim_8mA.Text);
            }
            Ch2_dactrimarray2 = Convert.ToInt32(Channel2_DACTrim_12mA.Text);
            if (Ch2_dactrimarray2 <= -1000)
            {
                DacTrimArray2[2] = -1000;
            }
            else if (Ch1_dactrimarray2 >= 1000)
            {
                DacTrimArray2[2] = 1000;
            }
            else
            {
                DacTrimArray2[2] = Convert.ToInt32(Channel2_DACTrim_12mA.Text);
            }
            Ch2_dactrimarray3 = Convert.ToInt32(Channel2_DACTrim_16mA.Text);
            if (Ch2_dactrimarray3 <= -1000)
            {
                DacTrimArray2[3] = -1000;
            }
            else if (Ch2_dactrimarray3 >= 1000)
            {
                DacTrimArray2[3] = 1000;
            }
            else
            {
                DacTrimArray2[3] = Convert.ToInt32(Channel2_DACTrim_16mA.Text);
            }
            Ch2_dactrimarray4 = Convert.ToInt32(Channel2_DACTrim_20mA.Text);
            if (Ch2_dactrimarray4 <= -1000)
            {
                DacTrimArray2[4] = -1000;
            }
            else if (Ch2_dactrimarray4 >= 1000)
            {
                DacTrimArray2[4] = 1000;
            }
            else
            {
                DacTrimArray2[4] = Convert.ToInt32(Channel2_DACTrim_20mA.Text);
            }

        }
        private async void Channel1_DACTrim_4mA_LostFocus(object sender, RoutedEventArgs e)
        {
            DacTrimsend_Information();
            await device.SetDacTrimArray(0, DacTrimArray1);
            Channel1_DACTrim_4mA.Text = DacTrimArray1[0].ToString();
        }
        private async void Channel1_DACTrim_8mA_LostFocus(object sender, RoutedEventArgs e)
        {
            DacTrimsend_Information();
            await device.SetDacTrimArray(0, DacTrimArray1);
            Channel1_DACTrim_8mA.Text = DacTrimArray1[1].ToString();
        }
        private async void Channel1_DACTrim_12mA_LostFocus(object sender, RoutedEventArgs e)
        {
            DacTrimsend_Information();
            await device.SetDacTrimArray(0, DacTrimArray1);
            Channel1_DACTrim_12mA.Text = DacTrimArray1[2].ToString();

        }
        private async void Channel1_DACTrim_16mA_LostFocus(object sender, RoutedEventArgs e)
        {
            DacTrimsend_Information();
            await device.SetDacTrimArray(0, DacTrimArray1);
            Channel1_DACTrim_16mA.Text = DacTrimArray1[3].ToString();


        }
        private async void Channel1_DACTrim_20mA_LostFocus(object sender, RoutedEventArgs e)
        {
            DacTrimsend_Information();
            await device.SetDacTrimArray(0, DacTrimArray1);
            Channel1_DACTrim_20mA.Text = DacTrimArray1[4].ToString();

        }
        private async void Channel2_DACTrim_4mA_LostFocus(object sender, RoutedEventArgs e)
        {
            DacTrimsend_Information();
            await device.SetDacTrimArray(1, DacTrimArray2);
            Channel2_DACTrim_4mA.Text = DacTrimArray2[0].ToString();

        }
        private async void Channel2_DACTrim_8mA_LostFocus(object sender, RoutedEventArgs e)
        {
            DacTrimsend_Information();
            await device.SetDacTrimArray(1, DacTrimArray2);
            Channel2_DACTrim_8mA.Text = DacTrimArray2[1].ToString();

        }
        private async void Channel2_DACTrim_12mA_LostFocus(object sender, RoutedEventArgs e)
        {
            DacTrimsend_Information();
            await device.SetDacTrimArray(1, DacTrimArray2);
            Channel2_DACTrim_12mA.Text = DacTrimArray2[2].ToString();
        }
        private async void Channel2_DACTrim_16mA_LostFocus(object sender, RoutedEventArgs e)
        {
            DacTrimsend_Information();
            await device.SetDacTrimArray(1, DacTrimArray2);
            Channel2_DACTrim_16mA.Text = DacTrimArray2[3].ToString();
        }
        private async void Channel2_DACTrim_20mA_LostFocus(object sender, RoutedEventArgs e)
        {
            DacTrimsend_Information();
            await device.SetDacTrimArray(1, DacTrimArray2);
            Channel2_DACTrim_20mA.Text = DacTrimArray2[4].ToString();

        }

    }
}

