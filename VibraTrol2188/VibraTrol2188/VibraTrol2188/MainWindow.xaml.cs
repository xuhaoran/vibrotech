﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Vibrotech;
namespace VibraTrol2188
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly DpsDevice _device;
        private DpsDevice.DeviceInfo _deviceInfo;
        public DpsDevice.ChannelParameters[] _channelParameters;
        private DpsDevice.ModbusConfig _modbusConfig;
        private DpsDevice.RelayConfig _relayConfig;
        private bool _deviceInitialized = false;
        private Interface_Show interface_Show;
        //private Parameters_windows parameter;
        private bool _connect = false;
        private int channelNum;
        private List<Interface_Show> user_interface = new List<Interface_Show>();
        private Active active_window = new Active();
        private List<int[]> Dactrims = new List<int[]>();
        public MainWindow()
        {
            InitializeComponent();
            _device = new DpsDevice();
            _device.OnConnected += ThreadDeviceConnected;
            _device.OnDisConnected += DeviceDisConnected;
            _device.OnMeasurementReceived += MeasurementReceived;
        }
        public async void ThreadDeviceConnected()
        {
            try
            {
                _deviceInfo = await _device.GetDeviceInfo();
                _channelParameters = new DpsDevice.ChannelParameters[_deviceInfo.NumChannels];
                for (int i = 0; i < _deviceInfo.NumChannels; i++)
                {
                    _channelParameters[i] = await _device.GetChannelParameters(i);
                    Dactrims[i] = await _device.GetDacTrimArray(i);
                }
                _modbusConfig = await _device.GetModbusConfig();
                Dispatcher.Invoke(new Action(DeviceConnected));

                _deviceInitialized = true;
            }
            catch (Exception e)
            {
                //MessageBox.Show(e.ToString());
            }

        }
        private void DeviceDisConnected()
        {
            _connect = false;

            Dispatcher.Invoke(() =>
            {
                Usb_state.Background = Brushes.Red;
                Usb_state.Text = "USB未连接...";
                user_interface.RemoveAll(it => true);
                DockPanel_allinformation.Children.RemoveRange(5, 8);

            });

        }
        public bool Relay_state_1;
        public bool Relay_state_2;
        private void MeasurementReceived(MeasurementReport report)
        {
            if (!_deviceInitialized)
                return;
            for (int j = 0; j < _deviceInfo.NumChannels; j++)
            {
                Dispatcher.Invoke(() =>
            {
                user_interface[j].Channe1_Output.Text = report.Output[j].ToString("F3");
                user_interface[j].Channel_Current.Text = report.Current[j].ToString("F3");
                user_interface[j].Channel_Voltage.Text = report.Voltage[j].ToString("F3");
                user_interface[j].Output = report.Output[j];
                user_interface[j].Voltage = report.Voltage[j];
            });
                //voltage[j] = report.Voltage[j];
            }

        }

        public async void DeviceConnected()
        {
            _connect = true;
            if (_deviceInfo.IsActivated)
            {
                Interface_Information();
                Usb_state.Background = Brushes.Green;
                Usb_state.Text = "USB连接中...";
            }
            else
            {
                active_window.Activation_HardwareSn.Text = _deviceInfo.HardwareSn;
                active_window.ShowDialog();
                if (active_window.Activation_ActiveCode.Text == null)
                {
                    MessageBox.Show("请输入激活码");
                }
                else
                    await _device.WriteActivationCode(active_window.Activation_ActiveCode.Text);
                Interface_Information();
                Usb_state.Background = Brushes.Green;
            }

        }
        bool Modify = false;

        private void Interface_Information()
        {

            for (int channelnum = 0; channelnum < _deviceInfo.NumChannels; channelnum++)
            {

                open = false;
                interface_Show = new Interface_Show();
                DockPanel_allinformation.Width = Double.NaN;
                DockPanel_allinformation.Height = Double.NaN;
                DockPanel_allinformation.Children.Add(interface_Show);
                interface_Show.SetValue(DockPanel.DockProperty, Dock.Top);
                interface_Show.HorizontalAlignment = HorizontalAlignment.Left;
                user_interface.Add(interface_Show);
                user_interface[channelnum].ChannelNum = channelnum;
                user_interface[channelnum].Channel_Num.Text = "CH" + (channelnum + 1).ToString();
                switch (_channelParameters[channelnum].TConfig)
                {
                    case DpsDevice.TConfig.Position:
                        user_interface[channelnum].Channel_Type.Text = "轴向位移";
                 user_interface[channelnum].Lbl_OutputUnit.Content = _channelParameters[channelnum].OutputUnitStr;
                       break;
                    case DpsDevice.TConfig.Tacho:
                        user_interface[channelnum].Channel_Type.Text = "键相/转速";
                        user_interface[channelnum].Lbl_OutputUnit.Content = _channelParameters[channelnum].OutputUnitStr;

                        break;
                    case DpsDevice.TConfig.Vibration:
                        user_interface[channelnum].Channel_Type.Text = "径向振动";
                        user_interface[channelnum].Lbl_OutputUnit.Content = _channelParameters[channelnum].OutputUnitStr;

                        break;
                }
                user_interface[channelnum].Btn_Openparameter += new EventHandler(btn_Openparameter);
            }
        }
        private List<Parameters_windows> window_parameter = new List<Parameters_windows>();
        private List<string> list_outputUnit = new List<string>();
        private List<string> list_sensorUnit = new List<string>();
        private List<string> list_outputType = new List<string>();
        private bool open = false;
        private Parameters_windows parameter = new Parameters_windows();
        private void btn_Openparameter(object sender, EventArgs e)
        {
            try
            {
                Parameters_windows parameter = new Parameters_windows();
                open = true;
                Interface_Show inter = sender as Interface_Show;
                parameter.ParameterChannelNum = inter.ChannelNum;
                parameter.Channelparameters.Header = "CH" + (inter.ChannelNum + 1).ToString() + "参数设置";
                parameter.Channelcalibration.Header = "CH" + (inter.ChannelNum + 1).ToString() + "校准设置";
                parameter.Title= "CH" + (inter.ChannelNum + 1).ToString() + "参数设置";
                parameter.Btn_Zerovoltage.IsEnabled = true;
                parameter.Btn_Fullscale.IsEnabled = false;
                parameter.Txt_VI1_Voltage.IsEnabled = true;
                parameter.Txt_VI2_Voltage.IsEnabled = false;
                parameter.Cbox_Typechoose.Items.Add("径向振动");
                parameter.Cbox_Typechoose.Items.Add("轴向位移");
                parameter.Cbox_Typechoose.Items.Add("键相/转速");
                foreach (var outputUnit in (DpsDevice.OutputUnit[])Enum.GetValues(typeof(DpsDevice.OutputUnit)))
                    list_outputUnit.Add(outputUnit.GetDescription());
                foreach (var sensorUnit in (DpsDevice.SensorUnit[])Enum.GetValues(typeof(DpsDevice.SensorUnit)))
                    list_sensorUnit.Add(sensorUnit.GetDescription());
                foreach (var outputType in (DpsDevice.OutputType[])Enum.GetValues(typeof(DpsDevice.OutputType)))
                    list_outputType.Add(outputType.GetDescription());
                switch (_channelParameters[inter.ChannelNum].TConfig)
                {
                    case DpsDevice.TConfig.Vibration:
                        parameter.Cbox_Typechoose.SelectedIndex = 0;
                        parameter.Channel_Position.Visibility = Visibility.Hidden;
                        parameter.Channel_Tacho.Visibility = Visibility.Hidden;
                        parameter.Channel_Vibration.Visibility = Visibility.Visible;

                        break;
                    case DpsDevice.TConfig.Position:
                        parameter.Cbox_Typechoose.SelectedIndex = 1;
                        parameter.Channel_Position.Visibility = Visibility.Visible;
                        parameter.Channel_Tacho.Visibility = Visibility.Hidden;
                        parameter.Channel_Vibration.Visibility = Visibility.Hidden;

                        break;
                    case DpsDevice.TConfig.Tacho:
                        parameter.Cbox_Typechoose.SelectedIndex = 2;
                        parameter.Channel_Position.Visibility = Visibility.Hidden;
                        parameter.Channel_Tacho.Visibility = Visibility.Visible;
                        parameter.Channel_Vibration.Visibility = Visibility.Hidden;

                        break;
                }
                for (int j = 0; j < 3; j++)
                {
                    parameter.Cbox_SensorUnit_choose.Items.Add(list_sensorUnit[j]);
                }
                switch (_channelParameters[inter.ChannelNum].SensorUnit)
                {
                    case DpsDevice.SensorUnit.MvPerG:
                        parameter.Cbox_SensorUnit_choose.SelectedIndex = 0;
                        break;
                    case DpsDevice.SensorUnit.MvPerMmps:
                        parameter.Cbox_SensorUnit_choose.SelectedIndex = 1;
                        break;
                    case DpsDevice.SensorUnit.MvPerIps:
                        parameter.Cbox_SensorUnit_choose.SelectedIndex = 2;
                        break;

                }
                for (int i = 0; i < 7; i++)
                {
                    parameter.Cbox_OutputUnit.Items.Add(list_outputUnit[i]);
                }
                switch (_channelParameters[inter.ChannelNum].OutputUnit)
                {
                    case DpsDevice.OutputUnit.G:
                        parameter.Cbox_OutputUnit.SelectedIndex = 0;
                        break;
                    case DpsDevice.OutputUnit.Ips:
                        parameter.Cbox_OutputUnit.SelectedIndex = 3;
                        break;
                    case DpsDevice.OutputUnit.Mil:
                        parameter.Cbox_OutputUnit.SelectedIndex = 6;
                        break;
                    case DpsDevice.OutputUnit.Mm:
                        parameter.Cbox_OutputUnit.SelectedIndex = 4;
                        break;
                    case DpsDevice.OutputUnit.Mmps:
                        parameter.Cbox_OutputUnit.SelectedIndex = 2;
                        break;
                    case DpsDevice.OutputUnit.Mps2:
                        parameter.Cbox_OutputUnit.SelectedIndex = 1;
                        break;
                    case DpsDevice.OutputUnit.Um:
                        parameter.Cbox_OutputUnit.SelectedIndex = 5;
                        break;
                }
                for (int h = 0; h < 3; h++)
                {
                    parameter.Cbox_OutputType.Items.Add(list_outputType[h]);

                }
                switch (_channelParameters[inter.ChannelNum].OutputType)
                {
                    case DpsDevice.OutputType.Peak:
                        parameter.Cbox_OutputType.SelectedIndex = 1;
                        break;
                    case DpsDevice.OutputType.Pkpk:
                        parameter.Cbox_OutputType.SelectedIndex = 2;
                        break;
                    case DpsDevice.OutputType.Rms:
                        parameter.Cbox_OutputType.SelectedIndex = 0;
                        break;
                }
                user_interface[inter.ChannelNum].Lbl_OutputUnit.Content = _channelParameters[inter.ChannelNum].OutputUnitStr;
                parameter.Cbox_PositionUnit.Items.Add(list_sensorUnit[3]);
                parameter.Cbox_PositionUnit.Items.Add(list_sensorUnit[4]);
                parameter.Txt_SensorSensitivity.Text = _channelParameters[inter.ChannelNum].SensorSensitivity.ToString();
                parameter.Txt_OutputFullScale.Text = _channelParameters[inter.ChannelNum].OutputFullScale.ToString();
                parameter.Txt_OutputGain.Text = _channelParameters[inter.ChannelNum].OutputGain.ToString();
                parameter.Txt_NoiseGate.Text = (_channelParameters[inter.ChannelNum].NoiseGate * _channelParameters[inter.ChannelNum].OutputFullScale).ToString();
                parameter.Txt_LfCutoffFreq.Text = _channelParameters[inter.ChannelNum].LfCutoffFreq.ToString();
                parameter.Txt_HfCutoffFreq.Text = _channelParameters[inter.ChannelNum].HfCutoffFreq.ToString();
                parameter.Txt_RslTimeMs.Text = (_channelParameters[inter.ChannelNum].RslTimeMs / 1000).ToString();
                parameter.Txt_dampingTimeMs.Text = (_channelParameters[inter.ChannelNum].DampingTimeMs / 1000).ToString();
                parameter.Txt_PositionDampingTimeMs.Text = (_channelParameters[inter.ChannelNum].DampingTimeMs / 1000).ToString();
                parameter.Txt_PositionLowerVoltage.Text = _channelParameters[inter.ChannelNum].PositionLowerVoltage.ToString();
                parameter.Txt_PositionUpperVoltage.Text = _channelParameters[inter.ChannelNum].PositionUpperVoltage.ToString();
                parameter.Txt_TachoDampingTimeMs.Text = (_channelParameters[inter.ChannelNum].DampingTimeMs / 1000).ToString();
                parameter.Txt_TachoNormalizer.Text = _channelParameters[inter.ChannelNum].TachoNormalizer.ToString();
                parameter.Txt_TachoRange.Text = _channelParameters[inter.ChannelNum].TachoRange.ToString();
                parameter.Txt_TachoTriggerThreshold.Text = _channelParameters[inter.ChannelNum].TachoTriggerThreshold.ToString();
                parameter.Enter += new EventHandler(On_Enter);
                parameter.Send += new EventHandler(On_lostfocusSend);
                parameter.Set_Zero += new EventHandler(On_Set_Zero);
                parameter.Set_Fullscale += new EventHandler(On_Set_Fullscale);
                parameter.Btn_zerovoltage += new EventHandler(On_Btn_zerovoltage);
                parameter.Btn_fullscale += new EventHandler(On_Btn_fullscale);
                parameter.Ckbx_fixoutcurrent += new EventHandler(On_ckbk_fixoutcurrent);
                parameter.Owner = this;
                parameter.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                parameter.ShowDialog();

            }
            catch (Exception a)
            {
                MessageBox.Show(a.ToString());
            }
        }
        private async void On_Enter(object sender, EventArgs e)
        {
            Parameters_windows parametersWindow = sender as Parameters_windows;
            if (_connect)
            {
                _channelParameters[parametersWindow.ParameterChannelNum].TachoTriggerThreshold = Convert.ToSingle(parametersWindow.Txt_TachoTriggerThreshold.Text);
                _channelParameters[parametersWindow.ParameterChannelNum].TachoRange = Convert.ToSingle(parametersWindow.Txt_TachoRange.Text);
                _channelParameters[parametersWindow.ParameterChannelNum].TachoNormalizer = Convert.ToSingle(parametersWindow.Txt_TachoNormalizer.Text);
                _channelParameters[parametersWindow.ParameterChannelNum].PositionUpperVoltage = Convert.ToSingle(parametersWindow.Txt_PositionUpperVoltage.Text);
                _channelParameters[parametersWindow.ParameterChannelNum].PositionLowerVoltage = Convert.ToSingle(parametersWindow.Txt_PositionLowerVoltage.Text);
                _channelParameters[parametersWindow.ParameterChannelNum].RslTimeMs = (ushort)(Convert.ToSingle(parametersWindow.Txt_RslTimeMs.Text) * 1000);
                _channelParameters[parametersWindow.ParameterChannelNum].HfCutoffFreq = Convert.ToSingle(parametersWindow.Txt_HfCutoffFreq.Text);
                _channelParameters[parametersWindow.ParameterChannelNum].LfCutoffFreq = Convert.ToSingle(parametersWindow.Txt_LfCutoffFreq.Text);
                _channelParameters[parametersWindow.ParameterChannelNum].NoiseGate = Convert.ToSingle(parametersWindow.Txt_NoiseGate.Text) / _channelParameters[parametersWindow.ParameterChannelNum].OutputFullScale;
                _channelParameters[parametersWindow.ParameterChannelNum].OutputGain = Convert.ToSingle(parametersWindow.Txt_OutputGain.Text);
                _channelParameters[parametersWindow.ParameterChannelNum].OutputFullScale = Convert.ToSingle(parametersWindow.Txt_OutputFullScale.Text);
                _channelParameters[parametersWindow.ParameterChannelNum].DampingTimeMs = (ushort)(Convert.ToSingle(parametersWindow.Txt_dampingTimeMs.Text) * 1000);
                _channelParameters[parametersWindow.ParameterChannelNum].SensorSensitivity = Convert.ToSingle(parametersWindow.Txt_SensorSensitivity.Text);
                await _device.SetChannelParameters(parametersWindow.ParameterChannelNum, _channelParameters[parametersWindow.ParameterChannelNum]);
            }
            else
            {
                //MessageBox.Show("请连接USB");
                parametersWindow.Close();
            }
        }
        private async void On_lostfocusSend(object sender, EventArgs e)
        {
            Parameters_windows parametersWindow = sender as Parameters_windows;
            if (_connect)
            {

                switch (parametersWindow.Cbox_Typechoose.SelectedIndex)
                {
                    case 0:
                        parametersWindow.Channel_Vibration.Visibility = Visibility.Visible;
                        parametersWindow.Channel_Position.Visibility = Visibility.Hidden;
                        parametersWindow.Channel_Tacho.Visibility = Visibility.Hidden;
                        _channelParameters[parametersWindow.ParameterChannelNum].TConfig = DpsDevice.TConfig.Vibration;
                        user_interface[parametersWindow.ParameterChannelNum].Channel_Type.Text = "径向振动";


                        break;
                    case 1:
                        parametersWindow.Channel_Vibration.Visibility = Visibility.Hidden;
                        parametersWindow.Channel_Position.Visibility = Visibility.Visible;
                        parametersWindow.Channel_Tacho.Visibility = Visibility.Hidden;
                        _channelParameters[parametersWindow.ParameterChannelNum].TConfig = DpsDevice.TConfig.Position;
                        user_interface[parametersWindow.ParameterChannelNum].Channel_Type.Text = "轴向位移";



                        break;
                    case 2:
                        parametersWindow.Channel_Vibration.Visibility = Visibility.Hidden;
                        parametersWindow.Channel_Position.Visibility = Visibility.Hidden;
                        parametersWindow.Channel_Tacho.Visibility = Visibility.Visible;
                        _channelParameters[parametersWindow.ParameterChannelNum].TConfig = DpsDevice.TConfig.Tacho;
                        user_interface[parametersWindow.ParameterChannelNum].Channel_Type.Text = "键相/转速";



                        break;
                }
                switch (parametersWindow.Cbox_SensorUnit_choose.SelectedIndex)
                {
                    case 0:
                        _channelParameters[parametersWindow.ParameterChannelNum].SensorUnit = DpsDevice.SensorUnit.MvPerG;
                        break;
                    case 1:
                        _channelParameters[parametersWindow.ParameterChannelNum].SensorUnit = DpsDevice.SensorUnit.MvPerMmps;
                        break;
                    case 2:
                        _channelParameters[parametersWindow.ParameterChannelNum].SensorUnit = DpsDevice.SensorUnit.MvPerIps;
                        break;
                }
                switch (parametersWindow.Cbox_OutputUnit.SelectedIndex)
                {
                    case 0:
                        _channelParameters[parametersWindow.ParameterChannelNum].OutputUnit = DpsDevice.OutputUnit.G;

                        break;
                    case 1:
                        _channelParameters[parametersWindow.ParameterChannelNum].OutputUnit = DpsDevice.OutputUnit.Mps2;
                        break;
                    case 2:
                        _channelParameters[parametersWindow.ParameterChannelNum].OutputUnit = DpsDevice.OutputUnit.Mmps;
                        break;
                    case 3:
                        _channelParameters[parametersWindow.ParameterChannelNum].OutputUnit = DpsDevice.OutputUnit.Ips;
                        break;
                    case 4:
                        _channelParameters[parametersWindow.ParameterChannelNum].OutputUnit = DpsDevice.OutputUnit.Mm;
                        break;
                    case 5:
                        _channelParameters[parametersWindow.ParameterChannelNum].OutputUnit = DpsDevice.OutputUnit.Um;
                        break;
                    case 6:
                        _channelParameters[parametersWindow.ParameterChannelNum].OutputUnit = DpsDevice.OutputUnit.Mil;
                        break;
                }
                switch (parametersWindow.Cbox_OutputType.SelectedIndex)
                {
                    case 0:
                        _channelParameters[parametersWindow.ParameterChannelNum].OutputType = DpsDevice.OutputType.Rms;
                        break;
                    case 1:
                        _channelParameters[parametersWindow.ParameterChannelNum].OutputType = DpsDevice.OutputType.Peak;
                        break;
                    case 2:
                        _channelParameters[parametersWindow.ParameterChannelNum].OutputType = DpsDevice.OutputType.Pkpk;
                        break;
                }
                _channelParameters[parametersWindow.ParameterChannelNum].TachoTriggerThreshold = Convert.ToSingle(parametersWindow.Txt_TachoTriggerThreshold.Text);
                _channelParameters[parametersWindow.ParameterChannelNum].TachoRange = Convert.ToSingle(parametersWindow.Txt_TachoRange.Text);
                _channelParameters[parametersWindow.ParameterChannelNum].TachoNormalizer = Convert.ToSingle(parametersWindow.Txt_TachoNormalizer.Text);
                _channelParameters[parametersWindow.ParameterChannelNum].PositionUpperVoltage = Convert.ToSingle(parametersWindow.Txt_PositionUpperVoltage.Text);
                _channelParameters[parametersWindow.ParameterChannelNum].PositionLowerVoltage = Convert.ToSingle(parametersWindow.Txt_PositionLowerVoltage.Text);
                _channelParameters[parametersWindow.ParameterChannelNum].RslTimeMs = (ushort)(Convert.ToSingle(parametersWindow.Txt_RslTimeMs.Text) * 1000);
                _channelParameters[parametersWindow.ParameterChannelNum].HfCutoffFreq = Convert.ToSingle(parametersWindow.Txt_HfCutoffFreq.Text);
                _channelParameters[parametersWindow.ParameterChannelNum].LfCutoffFreq = Convert.ToSingle(parametersWindow.Txt_LfCutoffFreq.Text);
                _channelParameters[parametersWindow.ParameterChannelNum].NoiseGate = Convert.ToSingle(parametersWindow.Txt_NoiseGate.Text) / _channelParameters[parametersWindow.ParameterChannelNum].OutputFullScale;
                _channelParameters[parametersWindow.ParameterChannelNum].OutputGain = Convert.ToSingle(parametersWindow.Txt_OutputGain.Text);
                _channelParameters[parametersWindow.ParameterChannelNum].OutputFullScale = Convert.ToSingle(parametersWindow.Txt_OutputFullScale.Text);
                _channelParameters[parametersWindow.ParameterChannelNum].DampingTimeMs = (ushort)(Convert.ToSingle(parametersWindow.Txt_dampingTimeMs.Text) * 1000);
                _channelParameters[parametersWindow.ParameterChannelNum].SensorSensitivity = Convert.ToSingle(parametersWindow.Txt_SensorSensitivity.Text);
                await _device.SetChannelParameters(parametersWindow.ParameterChannelNum, _channelParameters[parametersWindow.ParameterChannelNum]);
                user_interface[parametersWindow.ParameterChannelNum].Lbl_OutputUnit.Content = _channelParameters[parametersWindow.ParameterChannelNum].OutputUnitStr;

            }
            else
            {
                //MessageBox.Show("请连接USB");
                parametersWindow.Close();
            }
        }
        private async void On_Set_Zero(object sender, EventArgs e)
        {
            Parameters_windows parameters_Windows = sender as Parameters_windows;
            _channelParameters[parameters_Windows.ParameterChannelNum].NoiseGate = (user_interface[parameters_Windows.ParameterChannelNum].Output - 0.00f) / _channelParameters[parameters_Windows.ParameterChannelNum].OutputFullScale;
            await _device.SetChannelParameters(parameters_Windows.ParameterChannelNum, _channelParameters[parameters_Windows.ParameterChannelNum]);
            parameters_Windows.Txt_NoiseGate.Text = (_channelParameters[parameters_Windows.ParameterChannelNum].NoiseGate).ToString("F2");
        }
        private async void On_Set_Fullscale(object sender, EventArgs e)
        {
                 Parameters_windows parameters_Windows = sender as Parameters_windows;
           try
            {
                float fullscale;
                if (parameters_Windows.Txt_VibrationValue.Text == "")
                {
                    parameters_Windows.Txt_VibrationValue.Text = _channelParameters[parameters_Windows.ParameterChannelNum].OutputFullScale.ToString();
                }
                fullscale = Convert.ToSingle(parameters_Windows.Txt_VibrationValue.Text);
                if (user_interface[parameters_Windows.ParameterChannelNum].Output > _channelParameters[parameters_Windows.ParameterChannelNum].OutputFullScale*0.1)
                {
                    _channelParameters[parameters_Windows.ParameterChannelNum].OutputGain = fullscale / user_interface[parameters_Windows.ParameterChannelNum].Output;
                    await _device.SetChannelParameters(parameters_Windows.ParameterChannelNum, _channelParameters[parameters_Windows.ParameterChannelNum]);
                    parameters_Windows.Txt_OutputGain.Text = (_channelParameters[parameters_Windows.ParameterChannelNum].OutputGain).ToString("F3");
                }
                else
                {
                    MessageBox.Show("输入信号小于量程的10%");
                }

            }
            catch (Exception x)
            {
                MessageBox.Show("请输入振动台振动值");
                parameters_Windows.Txt_VibrationValue.Text = "";
            }
        }
        private async void Btn_Save_Click(object sender, RoutedEventArgs e)
        {
            await _device.SaveSettingsToFlash();
            for (int i = 0; i < _deviceInfo.NumChannels; i++)
                _channelParameters[i] = await _device.GetChannelParameters(i);

        }
        private void Main_Closed(object sender, EventArgs e)
        {
            Application.Current.Shutdown();
        }
        private async void Btn_default_Click(object sender, RoutedEventArgs e)
        {
            await _device.RestoreDefaultSettings();
            for (int i = 0; i < _deviceInfo.NumChannels; i++)
                _channelParameters[i] = await _device.GetChannelParameters(i);
        }
        private  void On_Btn_zerovoltage(object sender, EventArgs e)
        {
            Parameters_windows parameters_Windows = sender as Parameters_windows;
            user_interface[parameters_Windows.ParameterChannelNum].Voltage_In1 = Convert.ToSingle(parameters_Windows.Txt_VI1_Voltage.Text);
            Thread.Sleep(3000);
            user_interface[parameters_Windows.ParameterChannelNum].Voltage_Out1 = (user_interface[parameters_Windows.ParameterChannelNum].Voltage - _channelParameters[parameters_Windows.ParameterChannelNum].AdcZeroVoltage) / (_channelParameters[parameters_Windows.ParameterChannelNum].AdcFullVoltage - _channelParameters[parameters_Windows.ParameterChannelNum].AdcZeroVoltage);
            MessageBox.Show("Channel1 ADCZero设定成功, 请调整信号源至Channel1上限电压值");
            parameters_Windows.Btn_Zerovoltage.IsEnabled = false;
            parameters_Windows.Btn_Fullscale.IsEnabled = true;
            parameters_Windows.Txt_VI1_Voltage.IsEnabled = false;
            parameters_Windows.Txt_VI2_Voltage.IsEnabled = true;
        }
        private async void On_Btn_fullscale(object sender, EventArgs e)
        {
            Parameters_windows parameters_Windows = sender as Parameters_windows;
            MessageBoxResult result = MessageBox.Show("是否取消", "取消", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
            {

                user_interface[parameters_Windows.ParameterChannelNum].Voltage_In2 = Convert.ToSingle(parameters_Windows.Txt_VI2_Voltage.Text);
                Thread.Sleep(3000);
                user_interface[parameters_Windows.ParameterChannelNum].Voltage_Out2 = (user_interface[parameters_Windows.ParameterChannelNum].Voltage - _channelParameters[parameters_Windows.ParameterChannelNum].AdcZeroVoltage) / (_channelParameters[parameters_Windows.ParameterChannelNum].AdcFullVoltage - _channelParameters[parameters_Windows.ParameterChannelNum].AdcZeroVoltage);
                Thread.Sleep(3000);
                _channelParameters[parameters_Windows.ParameterChannelNum].AdcZeroVoltage = (user_interface[parameters_Windows.ParameterChannelNum].Voltage_Out2 * user_interface[parameters_Windows.ParameterChannelNum].Voltage_In1 - user_interface[parameters_Windows.ParameterChannelNum].Voltage_Out1 * user_interface[parameters_Windows.ParameterChannelNum].Voltage_In2) / (user_interface[parameters_Windows.ParameterChannelNum].Voltage_Out2- user_interface[parameters_Windows.ParameterChannelNum].Voltage_Out1);
                _channelParameters[parameters_Windows.ParameterChannelNum].AdcFullVoltage = (user_interface[parameters_Windows.ParameterChannelNum].Voltage_In1 * (user_interface[parameters_Windows.ParameterChannelNum].Voltage_Out2 - 1) - user_interface[parameters_Windows.ParameterChannelNum].Voltage_In2 * (user_interface[parameters_Windows.ParameterChannelNum].Voltage_Out1 - 1)) / (user_interface[parameters_Windows.ParameterChannelNum].Voltage_Out2 - user_interface[parameters_Windows.ParameterChannelNum].Voltage_Out1);
                await _device.SetChannelParameters(parameters_Windows.ParameterChannelNum, _channelParameters[parameters_Windows.ParameterChannelNum]);
                parameters_Windows.Btn_Zerovoltage.IsEnabled = true;
                parameters_Windows.Btn_Fullscale.IsEnabled = false;
                parameters_Windows.Txt_VI1_Voltage.IsEnabled = true;
                parameters_Windows.Txt_VI2_Voltage.IsEnabled = false;
            }
            else
            {
                parameters_Windows.Btn_Zerovoltage.IsEnabled = true;
                parameters_Windows.Btn_Fullscale.IsEnabled = false;
                parameters_Windows.Txt_VI1_Voltage.IsEnabled = true;
                parameters_Windows.Txt_VI2_Voltage.IsEnabled = false;

            }
        }
        private  void On_ckbk_fixoutcurrent(object sender,EventArgs e)
        {
            Parameters_windows parameters_Windows = sender as Parameters_windows;
            parameters_Windows.Txt_Current_4mA.Text = _channelParameters[parameters_Windows.ParameterChannelNum]..ToString();

        }
        private void Product_information_Click(object sender, RoutedEventArgs e)
        {

        }

        private async void Btn_Restart_Click(object sender, RoutedEventArgs e)
        {
            await _device.RestartDevice();


        }

    }
}

