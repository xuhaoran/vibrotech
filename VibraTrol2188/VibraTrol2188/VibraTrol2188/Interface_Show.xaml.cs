﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Vibrotech;
namespace VibraTrol2188
{
    /// <summary>
    /// Interface_Show.xaml 的交互逻辑
    /// </summary>
    public partial class Interface_Show : UserControl
    {


        public Interface_Show()
        {
            InitializeComponent();

        }
        public int ChannelNum
        {
            get { return (int)this.GetValue(channelnumPropetry); }
            set { SetValue(channelnumPropetry, value); }
        }
        public static readonly DependencyProperty channelnumPropetry = DependencyProperty.Register("ChannelNum", typeof(int), typeof(Interface_Show) ,new PropertyMetadata(0));

        public float Output
        {
            get { return (float)this.GetValue(outputPropetry); }
            set { SetValue(outputPropetry, value); }
        }
        public static readonly DependencyProperty outputPropetry = DependencyProperty.Register("Output", typeof(float), typeof(Interface_Show), new PropertyMetadata(0f));
        public float Voltage
        {
            get { return (float)this.GetValue(voltagePropetry); }
            set { SetValue(voltagePropetry, value); }
        }
        public static readonly DependencyProperty voltagePropetry = DependencyProperty.Register("Voltage", typeof(float), typeof(Interface_Show), new PropertyMetadata(0f));

        public float Voltage_In1
        {
            get { return (float)this.GetValue(voltage_in1Propetry); }
            set { SetValue(voltage_in1Propetry, value); }
        }
        public static readonly DependencyProperty voltage_in1Propetry = DependencyProperty.Register("Voltage_In1", typeof(float), typeof(Interface_Show), new PropertyMetadata(0f));

        public float Voltage_Out1
        {
            get { return (float)this.GetValue(voltage_out1Propetry); }
            set { SetValue(voltage_out1Propetry, value); }
        }
        public static readonly DependencyProperty voltage_out1Propetry = DependencyProperty.Register("Voltage_Out1", typeof(float), typeof(Interface_Show), new PropertyMetadata(0f));
        public float Voltage_In2
        {
            get { return (float)this.GetValue(voltage_in2Propetry); }
            set { SetValue(voltage_in2Propetry, value); }
        }
        public static readonly DependencyProperty voltage_in2Propetry = DependencyProperty.Register("Voltage_In2", typeof(float), typeof(Interface_Show), new PropertyMetadata(0f));

        public float Voltage_Out2
        {
            get { return (float)this.GetValue(voltage_out2Propetry); }
            set { SetValue(voltage_out2Propetry, value); }
        }
        public static readonly DependencyProperty voltage_out2Propetry = DependencyProperty.Register("Voltage_Out2", typeof(float), typeof(Interface_Show), new PropertyMetadata(0f));
        public int[] DacTrimArray
        {
            get { return (int[])this.GetValue(dacTrimArrayPropetry); }
            set { SetValue(dacTrimArrayPropetry, value); }
        }
        public static readonly DependencyProperty dacTrimArrayPropetry = DependencyProperty.Register("DacTrimArray", typeof(int[]), typeof(Interface_Show), new PropertyMetadata(0));

        public event EventHandler Btn_Openparameter;

        private void Btn_OpenParameters_Click(object sender, RoutedEventArgs e)
        {
            if (Btn_Openparameter != null)
            {
                Btn_Openparameter(this, e);
            }
        }


    }

}

