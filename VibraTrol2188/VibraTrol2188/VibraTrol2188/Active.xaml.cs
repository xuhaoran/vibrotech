﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace VibraTrol2188
{
    /// <summary>
    /// Active.xaml 的交互逻辑
    /// </summary>
    public partial class Active : Window
    {
        public Active()
        {
            InitializeComponent();
        }
        private void Grid_KeyDown(object sender, KeyEventArgs e)
        {
            MainWindow main = new MainWindow();
            if (e.Key == Key.Enter)
            {
                this.Close();
            }
        }

        private void Activation_OK_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Close();
                //await device.WriteActivationCode(Activation_ActiveCode.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "ERROR");
            }

        }

        private void Activation_Cencel_Click(object sender, RoutedEventArgs e)
        {
            Activation_ActiveCode.Text = "";
            this.Close();
        }
    }
}
