﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace VibraTrol2188
{
    /// <summary>
    /// Parameters_windows.xaml 的交互逻辑
    /// </summary>
    public partial class Parameters_windows : Window
    {

        public Parameters_windows()
        {
            InitializeComponent();
        }
        public event EventHandler Send;

        public float damping;
        public event EventHandler Esc;
        public event EventHandler Enter;
        public event EventHandler Set_Zero;
        public event EventHandler Set_Fullscale;
        private void Grid_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Enter(this,e);
                Cbox_Typechoose.Focus();


                //damping = Convert.ToSingle(main._channelParameters[0].DampingTimeMs);
            }

        }
        public int ParameterChannelNum
        {
            get { return (int)this.GetValue(channelnumPropetry); }
            set { SetValue(channelnumPropetry, value); }
        }
        public static readonly DependencyProperty channelnumPropetry = DependencyProperty.Register("ParameterChannelNum", typeof(int), typeof(Parameters_windows), new PropertyMetadata(0));

        private void Grid_LostFocus(object sender, RoutedEventArgs e)
        {
            if (Send != null)
            {
                Send(this, new EventArgs());
            }
        }
        private void Btn_CalibrationZero_Click(object sender,RoutedEventArgs e)
        {
            if (Set_Zero != null)
            {
                Set_Zero(this, new EventArgs());
            }
        }
        private void Btn_CalibrationFull_Click(object sender, RoutedEventArgs e)
        {
            if (Set_Fullscale != null)
            {
                Set_Fullscale(this, new EventArgs());
            }
        }


        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key==Key.Escape)
            {
                //Esc(this,e);
                this.Close();
            }
        }
        public event EventHandler Btn_zerovoltage;
        public event EventHandler Btn_fullscale;

        private void Btn_Zerovoltage_Click(object sender, RoutedEventArgs e)
        {
            if (Btn_zerovoltage != null)
            {
                Btn_zerovoltage(this, new EventArgs());
            }
        }

        private void Btn_Fullscale_Click(object sender, RoutedEventArgs e)
        {
            if (Btn_fullscale != null)
            {
                Btn_fullscale(this, new EventArgs());
            }

        }
        public event EventHandler Ckbx_fixoutcurrent;
        private void Ckbx_FixOutCurrent_Click(object sender, RoutedEventArgs e)
        {
            if (Ckbx_fixoutcurrent != null)
            {
                Ckbx_fixoutcurrent(this, new EventArgs());
            }
        }
    }
}
